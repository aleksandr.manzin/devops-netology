# Домашнее задание к занятию "5.3. Введение. Экосистема. Архитектура. Жизненный цикл Docker контейнера"
___

## Задача №1

> Сценарий выполения задачи:
> * создайте свой репозиторий на https://hub.docker.com;
> * выберете любой образ, который содержит веб-сервер Nginx;
> * создайте свой fork образа;
> * реализуйте функциональность: запуск веб-сервера в фоне с индекс-страницей, содержащей HTML-код ниже:

```
<html>
<head>
Hey, Netology
</head>
<body>
<h1>I’m DevOps Engineer!</h1>
</body>
</html>
```

> Опубликуйте созданный форк в своем репозитории и предоставьте ответ в виде ссылки на https://hub.docker.com/username_repo.


* создадим файл `index.html` с содержимым, указанным в задании;
* создадим `Dockerfile` и запишем в него следующие директивы:
	```
	FROM nginx					# стягиваем образ nginx из репозитория docker
	
	RUN rm -f /usr/share/nginx/html/index.html	# удаляем файл index.html

	COPY /$PWD/index.html /usr/share/nginx/html/	# копируем, созданный на первом шаге, файл index.html
	```
		
* получаем Docker-образ с помощью `Dockerfile`:
	```
	$ docker build -t aleksandrmanzin/nginx:1.21.6 .
	...
	Successfully built 06bc07559386
	Successfully tagged aleksandrmanzin/nginx:1.21.6
	```
* логинимся на hub.docker.com:
	```bash
	$ docker login -u aleksandrmanzin
	Login Succeeded
	```
* выгружаем образ на `hub.docker.com`:
	```bash
	$ docker push aleksandrmanzin/nginx:1.21.6
	The push refers to repository [docker.io/aleksandrmanzin/nginx]
	```
* проверим, что у нас получилось. Для этого сначала полностью очистим все образы:
	```bash
	$ docker images
	REPOSITORY				TAG			IMAGE ID		CREATED			SIZE
	aleksandrmanzin/nginx	1.21.6		06bc07559386	12 seconds ago	142MB
	nginx					latest		0e901e68141f	4 days ago		142MB
	$ docker system prune -a -f
	$ docker run -d -p 80:80  aleksandrmanzin/nginx:1.21.6
	Unable to find image 'aleksandrmanzin/nginx:1.21.6' locally
	1.21.6: Pulling from aleksandrmanzin/nginx
	```
* перейдем на страничку сайта с другого компьютера:
	
	![nginx](screenshots/nginx.png "nginx modified index.html")
	
* и посмотрим в наш репозиторий:
	![Docker hub](screenshots/hub.png "https://hub.docker.com/repository/docker/aleksandrmanzin/nginx")
* ссылка на образ: ***https://hub.docker.com/repository/docker/aleksandrmanzin/nginx***
	
## Задача №2

> Посмотрите на сценарий ниже и ответьте на вопрос: "Подходит ли в этом сценарии использование Docker контейнеров или лучше подойдет виртуальная машина, физическая машина? Может быть возможны разные варианты?"
>
> Детально опишите и обоснуйте свой выбор.
>
> Сценарий:
> * Высоконагруженное монолитное java веб-приложение;

Виртуальная машина тут не подойдет, т.к. приложение высоконагруженное и для него требуется максимальное использование аппаратных ресурсов сервера.
На мой взгляд, для реализации данного сценария подойдет и `docker`, и физический сервер. Однако, т.к. приложение монолитное, то его можно развернуть и на других серверах, в случае такой необходимости. Поэтому, пожалуй, тут больше подойдет физический сервер
> * Nodejs веб-приложение;

Для данного случая `Docker` отлично подойдет. В контейнер можно будет упаковать веб-приложение со всеми его зависимостями. 
> * Мобильное приложение c версиями для Android и iOS;

Поизучав немного вопрос, наткнулся на несколько статей, в которых описывается процесс запуска Android-приложений с помощью `Dockerfile`. Однако, под iOS ничего подобного найти на смог. Но, думаю, этому есть логическое объяснение. iOS используют ядро, которое основано на базе OSX, а Docker использует ядро Linux. Следовательно, запуск iOS-приложений в `Docker` невозможен. Это лишь мои догадки, надеюсь, Вы меня поправите, если ошибаюсь.
Если бы в задаче стояло только Android-приложение, то `Docker` полностью бы подошёл под эту задачу.
В данном сценарии подойдут виртуальные машины.
> * Шина данных на базе Apache Kafka;

`Docker` подойдет для этого сценария. На DockerHub достаточно большое количество образов с Apache Kafka. В случае обнаружения багов в продакшене, легко откатиться на работающую версию.
> * Elasticsearch кластер для реализации логирования продуктивного веб-приложения - три ноды elasticsearch, два logstash и две ноды kibana;

Можно реализовать как на виртуальных машинах, так и с помощью `Docker`. `Docker` позволит быстро развернуть необходимые контейнеры и создать кластер. Также контейнеры более легковесные
> * Мониторинг-стек на базе Prometheus и Grafana;

`Docker` отлично подходит для разворачивания контейнеров с Prometheus и Grafana. Также плюсом является быстрое разворачивание контейнеров.
> * MongoDB, как основное хранилище данных для java-приложения;

`Docker` подойдет для этого сценария. Вообще, на DockerHub очень много образов БД, в том числе и официальный образ `mongodb`. Для теста запускал с помощью `Docker compose` mongodb в паре с mongo-express и mariadb в паре с adminer.
> * Gitlab сервер для реализации CI/CD процессов и приватный (закрытый) Docker Registry.

Думаю, целесообразно использовать либо виртуальные машины, либо `docker`. 

## Задача №3

> * Запустите первый контейнер из образа centos c любым тэгом в фоновом режиме, подключив папку /data из текущей рабочей директории на хостовой машине в /data контейнера;

```bash
aleksandr@manzin:~/05-virt-03-docker$ docker run -v /$PWD/data:/data --name centos -it --detach centos:centos7.9.2009 bash
aleksandr@manzin:~/05-virt-03-docker$ docker ps
CONTAINER ID   IMAGE                   COMMAND       CREATED              STATUS              PORTS     NAMES
413fecdc3297   centos:centos7.9.2009   "bash"        About a minute ago   Up About a minute             centos
```
> * Запустите второй контейнер из образа debian в фоновом режиме, подключив папку /data из текущей рабочей директории на хостовой машине в /data контейнера;
	
```bash
aleksandr@manzin:~/05-virt-03-docker$ docker run -v /$PWD/data:/data --name debian -it --detach debian:stable bash
aleksandr@manzin:~/05-virt-03-docker$ docker ps	
CONTAINER ID   IMAGE                   COMMAND       CREATED              STATUS              PORTS     NAMES
090432e96392   debian:stable           "bash"        About a minute ago   Up About a minute             debian
```
> * Подключитесь к первому контейнеру с помощью docker exec и создайте текстовый файл любого содержания в /data;

```bash
aleksandr@manzin:~/05-virt-03-docker$ docker exec centos_share echo 'Hello, Netology!' > data/netology.txt
```
> * Добавьте еще один файл в папку /data на хостовой машине;

```bash
aleksandr@manzin:~/05-virt-03-docker$ echo "I'm DevOps Engineer!" > data/devops.txt
```
> * Подключитесь во второй контейнер и отобразите листинг и содержание файлов в /data контейнера.

```bash
aleksandr@manzin:~/05-virt-03-docker$ docker exec debian ls -l data
total 8
-rw-rw-r-- 1 1000 1000 21 Jun  4 21:03 devops.txt
-rw-rw-r-- 1 1000 1000 17 Jun  4 21:02 netology.txt
aleksandr@manzin:~/05-virt-03-docker$ docker exec debian cat data/*
I'm DevOps Engineer!
Hello, Netology!
```

## Задача №4 (*)

> Воспроизвести практическую часть лекции самостоятельно.
> Соберите Docker образ с Ansible, загрузите на Docker Hub и пришлите ссылку вместе с остальными ответами к задачам.

* создадим `Dockerfile` (будем использовать версию ansible 2.10.0):
	```
	FROM alpine:3.14

	RUN CARGO_NET_GIT_FETCH_WITH_CLI=1 && \
		apk --no-cache add \
			sudo \
			python3 \
			py3-pip \
			openssl \
			ca-certificates \
			sshpass \
			openssh-client \
			rsync \
			git && \
		apk --no-cache add --virtual build-dependencies \
			python3-dev \
			libffi-dev \
			musl-dev \
			gcc \
			cargo \
			openssl-dev \
			libressl-dev \
			build-base && \
		pip install --upgrade pip wheel && \
		pip install --upgrade cryptography cffi && \
		pip install ansible-core && \
		pip install ansible==2.10.0 && \
		pip install mitogen ansible-lint jmespath && \
		pip install --upgrade pywinrm && \
		apk del build-dependencies && \
		rm -rf /var/cache/apk/* && \
		rm -rf /root/.cache/pip && \
		rm -rf /root/.cargo

	RUN mkdir /ansible && \
		mkdir -p /etc/ansible && \
		echo 'localhost' > /etc/ansible/hosts

	WORKDIR /ansible

	CMD [ "ansible-playbook", "--version" ]
	```

* соберем образ:
	```bash
	aleksandr@manzin:~/05-virt-03-docker/src/build/ansible$ docker built -t aleksandrmanzin/ansible:2.10.0 .
	```
* отправим в репозиторий:
	```bash
	aleksandr@manzin:~/05-virt-03-docker/src/build/ansible$ docker push aleksandrmanzin/ansible:2.10.0
	```
* ссылка на образ: ***https://hub.docker.com/repository/docker/aleksandrmanzin/ansible***