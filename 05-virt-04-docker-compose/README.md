# Домашнее задание к занятию "5.4. Оркестрация группой Docker контейнеров на примере Docker Compose"
___
## Задача №1
> Создать собственный образ операционной системы с помощью Packer.

* установим `yc`:
  ```bash
  $ curl -sSL https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash
  ```
* произведем настройку профиля CLI:
  ```bash
  $ yc init
  Welcome! This command will take you through the configuration process.
  Pick desired action:
   [1] Re-initialize this profile 'cloud-aleksandrmanzin' with new settings
   [2] Create a new profile
   [3] Switch to and re-initialize existing profile: 'default'
  Please enter your numeric choice: 1
  Please go to https://oauth.yandex.ru/authorize?response_type=token&client_id=1a6990aa636648e9b2ef855fa7bec2fb in order to obtain OAuth token.

  Please enter OAuth token: [AQAAAAAIB*********************XCBDPjiMI]
  You have one cloud available: 'aleksandrmanzin' (id = b1gjp5l6o4ehob5b3eak). It is going to be used by default.
  Please choose folder to use:
   [1] default (id = b1gbc25c6g5ug4m1574b)
   [2] Create a new folder
  Please enter your numeric choice: 2
  Please enter a folder name: netology
  Your current folder has been set to 'netology' (id = b1gl0fkdpeiilq9hi2b3).
  Do you want to configure a default Compute zone? [Y/n] y
  Which zone do you want to use as a profile default?
   [1] ru-central1-a
   [2] ru-central1-b
   [3] ru-central1-c
   [4] Do not set default zone
  Please enter your numeric choice: 1
  Your profile default Compute zone has been set to 'ru-central1-a'.
  ```
* проверяем настройки нашего профиля `CLI`:
  ```bash
  $ yc config list
  token: AQAAAAAIBNTpAATuwYa7EKKcv0wYl_XCBDPjiMI
  cloud-id: b1gjp5l6o4ehob5b3eak
  folder-id: b1gl0fkdpeiilq9hi2b3
  compute-default-zone: ru-central1-a
  ```
* создаем облачную сеть:
  ```bash
  $ yc vpc network create --name net
  id: enp4g90fn9ae6sacdrnl
  folder_id: b1gl0fkdpeiilq9hi2b3
  created_at: "2022-06-13T12:38:57Z"
  name: net
  ```
* создаем подсеть в облачной сети `net`:
  ```bash
  $ yc vpc subnet create --name my-subnet-a --zone ru-central1-a --range 10.1.2.0/24 --network-name net --description "my first subnet via yc"
  id: e9bab2tptvoj9m0sd810
  folder_id: b1gl0fkdpeiilq9hi2b3
  created_at: "2022-06-13T12:40:56Z"
  name: my-subnet-a
  description: my first subnet via yc
  network_id: enp4g90fn9ae6sacdrnl
  zone_id: ru-central1-a
  v4_cidr_blocks:
  - 10.1.2.0/24
  ```
* получаем список всех подсетей:
  ```bash
  $ yc vpc subnet list
  +----------------------+-------------+----------------------+----------------+---------------+---------------+
  |          ID          |    NAME     |      NETWORK ID      | ROUTE TABLE ID |     ZONE      |     RANGE     |
  +----------------------+-------------+----------------------+----------------+---------------+---------------+
  | e9bab2tptvoj9m0sd810 | my-subnet-a | enp4g90fn9ae6sacdrnl |                | ru-central1-a | [10.1.2.0/24] |
  +----------------------+-------------+----------------------+----------------+---------------+---------------+
  ```
* подставляем все необходимые данные (`OATH`, `folder_id`, `subnet_id`, `zone`) в файл конфигурации `packer` - `centos-7-base.json`:
  ```
  {
    "builders": [
      {
        "disk_type": "network-nvme",
        "folder_id": "b1gl0fkdpeiilq9hi2b3",
        "image_description": "by packer",
        "image_family": "centos",
        "image_name": "centos-7-base",
        "source_image_family": "centos-7",
        "ssh_username": "centos",
        "subnet_id": "e9bab2tptvoj9m0sd810",
        "token": "AQAAAAAIBNTpAATuwYa7EKKcv0wYl_XCBDPjiMI",
        "type": "yandex",
        "use_ipv4_nat": true,
        "zone": "ru-central1-a"
      }
    ],
    "provisioners": [
      {
        "inline": [
          "sudo yum -y update",
          "sudo yum -y install bridge-utils bind-utils iptables curl net-tools tcpdump rsync telnet openssh-server"
        ],
        "type": "shell"
      }
    ]
  }
  ```
* проверяем нашу конфигурацию `packer`:
  ```bash
  $ packer validate centos-7-base.json
  The configuration is valid.
  ```
* запускаем сборку образа:
  ```bash
  $ packer build centos-7-base.json
  .....
  ==> Wait completed after 1 minute 57 seconds

  ==> Builds finished. The artifacts of successful builds are:
  --> yandex: A disk image was created: centos-7-base (id: fd8s09jem4neck5929kq) with family name centos
  ```
* проверяем, что образ создался:
  ```bash
  $ yc compute image list
  +----------------------+---------------+--------+----------------------+--------+
  |          ID          |     NAME      | FAMILY |     PRODUCT IDS      | STATUS |
  +----------------------+---------------+--------+----------------------+--------+
  | fd8fjsl19terp4096ddh | centos-7-base | centos | f2ed6g56rfbepn59jt09 | READY  |
  +----------------------+---------------+--------+----------------------+--------+
  ```
* образ:
![Packer](screenshots/packer_image.png "Packer Image")

## Задача №2
> Создать вашу первую виртуальную машину в Яндекс.Облаке.

* копируем ID-образа и прописываем его в файле `terraform/variables.tf`. Вместе с этим, надо еще добавить `folder_id` и `cloud_id`:
  ```
  # Заменить на ID своего облака
  # https://console.cloud.yandex.ru/cloud?section=overview
  variable "yandex_cloud_id" {
    default = "b1gjp5l6o4ehob5b3eak"
  }

  # Заменить на Folder своего облака
  # https://console.cloud.yandex.ru/cloud?section=overview
  variable "yandex_folder_id" {
    default = "b1gl0fkdpeiilq9hi2b3"
  }

  # Заменить на ID своего образа
  # ID можно узнать с помощью команды yc compute image list
  variable "centos-7-base" {
    default = "fd8fjsl19terp4096ddh"
  }
  ```
* cоздаём сервисный аккаунт:
  ```bash
  $ yc iam service-account create --name service-bot
  ```
* присваиваем роль `editor` для сервисного аккаунта, чтоб он мог создавать и удалять сети:
  ```bash
  yc resource-manager folder add-access-binding netology --role editor --subject serviceAccount:ajeh5jne6ak4rd4k50d3
  ```
* создаем авторизованный ключ для сервисного аккаунта и записываем его в файл `key.json`:
  ```bash
  $ yc iam key create --service-account-name service-bot --output key.json
  ```
* удаляем наши сеть и подсеть, которые были созданы для создания образа `Packer-ом`
  ```bash
  $ yc vpc subnet delete --name my-subnet-a && yc vpc network delete --name net
  ```
* выполняем инициализацию конфигурации `terraform`:
  ```bash
  $ terraform init
  Initializing the backend...

  Initializing provider plugins...
  - Finding latest version of yandex-cloud/yandex...
  - Installing yandex-cloud/yandex v0.75.0...
  - Installed yandex-cloud/yandex v0.75.0 (self-signed, key ID E40F590B50BB8E40)

  Partner and community providers are signed by their developers.
  If youd like to know more about provider signing, you can read about it here:
  https://www.terraform.io/docs/cli/plugins/signing.html

  Terraform has created a lock file .terraform.lock.hcl to record the provider
  selections it made above. Include this file in your version control repository
  so that Terraform can guarantee to make the same selections by default when
  you run "terraform init" in the future.

  Terraform has been successfully initialized!

  You may now begin working with Terraform. Try running "terraform plan" to see
  any changes that are required for your infrastructure. All Terraform commands
  should now work.

  If you ever set or change modules or backend configuration for Terraform,
  rerun this command to reinitialize your working directory. If you forget, other
  commands will detect it and remind you to do so if necessary.
  ```
* выполняем проверку конфигурации:
  ```bash
  $ terraform validate
  Success! The configuration is valid
  ```
* запускаем проверку плана:
  ```bash
  $ terraform plan
  ```
* применяем Terraform план с автоматическим подтверждением на выполнение:
  ```bash
  $ terraform apply -auto-approve
  ```
* свойства виртуальной машины:
![Virtual Machine](screenshots/properties.png "Yandex Cloud VM")

## Задача №3
> Создать ваш первый готовый к боевой эксплуатации компонент мониторинга, состоящий из стека микросервисов.

* запускаем `ansible-playbook`:
  ```bash
  $ ansible-playbook provision.yml
  ```
* веб-морда:
![Grafana](screenshots/grafana.png "Grafana")

## Задача №4 (*)
> Создать вторую ВМ и подключить её к мониторингу развёрнутому на первом сервере.

Для удобства скачал дашборд, чтоб удобно было переключаться между серверами:
  * мониторинг метрик первого сервера:
  ![First Server](screenshots/1st.png "First Server Metrics - node01")
  * мониторинг метрик второго сервера:
  ![Second Server](screenshots/2nd.png "Second Server Metrics - node02")
