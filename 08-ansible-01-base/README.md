# Домашнее задание к занятию "08.01 Введение в Ansible"

## Подготовка к выполнению
> 1. Установите ansible версии 2.10 или выше.
> 2. Создайте свой собственный публичный репозиторий на github с произвольным именем.
> 3. Скачайте [playbook](./playbook/) из репозитория с домашним заданием и перенесите его в свой репозиторий.

  ![Ansible](screenshots/ansible.png "Ansible version 2.10.8")

## Основная часть
> 1. Попробуйте запустить playbook на окружении из `test.yml`, зафиксируйте какое значение имеет факт `some_fact` для указанного хоста при выполнении playbook'a.

  - запустим `ansible-playbook` из `test.yml`:
    ```bash
    $ ansible-playbook -i inventory/test.yml
    ```
    ![facts](screenshots/facts.png)

  - значение `fact` - `12`

> 2. Найдите файл с переменными (group_vars) в котором задаётся найденное в первом пункте значение и поменяйте его на 'all default fact'.

  - производим изменения в файле `/group_vars/all/examp.yml`:
    ![Default Facts](screenshots/default_facts.png)

> 3. Воспользуйтесь подготовленным (используется `docker`) или создайте собственное окружение для проведения дальнейших испытаний.

  - создал своё окружение:
    - `docker-compose.yml`:
      ```yml
      version: '2.9'

      services:
        centos7:
          image: centos:7
          container_name: centos7
          tty: true

        ubuntu:
          build:
            dockerfile: Dockerfile
          container_name: ubuntu
          tty: true
      ```
    - `Dockerfile`:
      ```yml
      FROM ubuntu:22.04

      RUN apt update -y && \
          apt install -y python3
      ```
  - запустим наши контейнеры:
    ```bash
    $ docker compose up -d
    [+] Running 2/2
     ⠿ Container ubuntu   Started   11.0s
     ⠿ Container centos7  Started   10.9s  
    ```
> 4. Проведите запуск playbook на окружении из `prod.yml`. Зафиксируйте полученные значения `some_fact` для каждого из `managed host`.

  ```bash
  $ ansible-playbook -i inventory/prod.yml site.yml
  ```
  ![prod](screenshots/prod.png)

> 5. Добавьте факты в `group_vars` каждой из групп хостов так, чтобы для `some_fact` получились следующие значения: для `deb` - 'deb default fact', для `el` - 'el default fact'.

  - `deb`:

    ![](screenshots/deb_fact.png)

  - `el`:

    ![](screenshots/el_fact.png)


> 6.  Повторите запуск playbook на окружении `prod.yml`. Убедитесь, что выдаются корректные значения для всех хостов.

  ```bash
  $ ansible-playbook -i inventory/prod.yml site.yml
  ```
  ![prod](screenshots/new_facts.png)

> 7. При помощи `ansible-vault` зашифруйте факты в `group_vars/deb` и `group_vars/el` с паролем `netology`.

  - шифруем файл `group_vars/deb/examp.yml`:
    ```bash
    $ ansible-vault encrypt group_vars/deb/examp.yml
    New Vault password:
    Confirm New Vault password:
    Encryption successful
    ```
  - шифруем файл `group_vars/el/examp.yml`:
    ```bash
    $ ansible-vault encrypt group_vars/el/examp.yml
    New Vault password:
    Confirm New Vault password:
    Encryption successful
    ```

> 8. Запустите playbook на окружении `prod.yml`. При запуске `ansible` должен запросить у вас пароль. Убедитесь в работоспособности.

  - для выполнения `playbook` необходимо передать параметр `--ask-vault-pass`:
    ```bash
    $ ansible-playbook --ask-vault-pass -i inventory/prod.yml site.yml
    Vault password:
    ```
    ![Encrypted](screenshots/encrypted.png "Encrypted")

  - либо можно передать параметр `--vault-id @prompt`:
    ```bash
    $ ansible-playbook -i inventory/prod.yml site.yml --vault-id @prompt
    Vault password (default):
    ```
    ![Encrypted](screenshots/encrypted2.png "Encrypted")

> 9. Посмотрите при помощи `ansible-doc` список плагинов для подключения. Выберите подходящий для работы на `control node`.

  - для этой задачи воспользуемся ключом `-t` с указанием `connection`:
    ```bash
    $ ansible-doc -l -t connection | tee
    ```

    ![Connection](screenshots/connection.png "Connection")

    Нам нужен, соответственно, `local`

> 10. В `prod.yml` добавьте новую группу хостов с именем  `local`, в ней разместите localhost с необходимым типом подключения.

  - добавим новую группу:
      ```yml
      ---
        el:
          hosts:
            centos7:
              ansible_connection: docker
        deb:
          hosts:
            ubuntu:
              ansible_connection: docker

        local:
          hosts:
            localhost:
              ansible_connection: local
      ```

> 11. Запустите playbook на окружении `prod.yml`. При запуске `ansible` должен запросить у вас пароль. Убедитесь что факты `some_fact` для каждого из хостов определены из верных `group_vars`.

  - запустим наш `playbook`:
    ```bash
    $ ansible-playbook --ask-vault-pass -i inventory/prod.yml site.yml
    Vault password:
    ```

    ![All Facts](screenshots/all_facts.png "All facts.png")

> 12. Заполните `README.md` ответами на вопросы. Сделайте `git push` в ветку `master`. В ответе отправьте ссылку на ваш открытый репозиторий с изменённым `playbook` и заполненным `README.md`.

  [README.md](https://gitlab.com/aleksandr.manzin/devops-netology/-/tree/main/08-ansible-01-base/playbook/README.md)


## Необязательная часть

> 1. При помощи `ansible-vault` расшифруйте все зашифрованные файлы с переменными.

  ![Decrypt](screenshots/decrypt.png "Decryption")

> 2. Зашифруйте отдельное значение `PaSSw0rd` для переменной `some_fact` паролем `netology`. Добавьте полученное значение в `group_vars/all/examp.yml`.

  - зашифруем значение `PaSSw0rd` и сразу добавим в нужный нам файл `group_vars/all/examp.yml`:
    ```bash
    ansible-vault encrypt_string "PaSSw0rd" --vault-pass-file .ansible_vault_pass | grep -v "Encryption successful" | tee -a group_vars/all/examp.yml
    !vault |
              $ANSIBLE_VAULT;1.1;AES256
              64323939346432336566633038303566623134653131353432666534396431646530363130326531
              3636333438626465376234613064373664376232373966650a303137623538326436326635383764
              37346362393838323037303335353061626333663861336530303662353236656239366166383137
              6331306162326331620a353265323136396537636330313230663931346662383665363061626564
              3366
    ```
  - отредактируем немного файл `group_vars/all/examp.yml`, приведя его к виду:
    ```bash
    $ cat group_vars/all/examp.yml
    ---
      some_fact: !vault |
              $ANSIBLE_VAULT;1.1;AES256
              64323939346432336566633038303566623134653131353432666534396431646530363130326531
              3636333438626465376234613064373664376232373966650a303137623538326436326635383764
              37346362393838323037303335353061626333663861336530303662353236656239366166383137
              6331306162326331620a353265323136396537636330313230663931346662383665363061626564
              3366
    ```

> 3. Запустите `playbook`, убедитесь, что для нужных хостов применился новый `fact`.

  ```bash
  $ ansible-playbook -i inventory/prod.yml site.yml --vault-pass-file .ansible_vault_pass
  ```
  ![Encrypt String](screenshots/encrypt_string.png "Encrypt String")
> 4. Добавьте новую группу хостов `fedora`, самостоятельно придумайте для неё переменную. В качестве образа можно использовать [этот](https://hub.docker.com/r/pycontribs/fedora).


  - `docker-compose.yml`:
    ```yml
    ............................
    fedora:
      image: pycontribs/fedora
      container_name: fedora
      tty: true      
    ```
  - `prod.yml`:
    ```yml
    fed:
      hosts:
        fedora:
          ansible_connection: docker
    ```
  - `group_vars/fed/examp.yml`:
    ```yml
    ---
      some_fact: "fedora facts"
    ```

  ![Fedora Facts](screenshots/fedora_add.png "Fedora Facts")

> 5. Напишите скрипт на bash: автоматизируйте поднятие необходимых контейнеров, запуск ansible-playbook и остановку контейнеров.

  - создадим файл, который будет выполнять скрипт:
    ```bash
    $ touch facts.sh; chmod +x facts.sh
    ```
  - скрипт на `bash`:
    ```bash
    $ cat facts.sh
    #!/usr/bin/env bash
    docker compose up -d
    ansible-playbook -i inventory/prod.yml site.yml --vault-pass-file .ansible_vault_pass
    docker compose stop
    ```
    ![Script](screenshots/script.png "Script on bash")

> 6. Все изменения должны быть зафиксированы и отправлены в ваш личный репозиторий.

  [README.md](https://gitlab.com/aleksandr.manzin/devops-netology/-/tree/main/08-ansible-01-base/)
