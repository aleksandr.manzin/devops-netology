#!/usr/bin/env bash
docker compose up -d
ansible-playbook -i inventory/prod.yml site.yml --vault-pass-file .ansible_vault_pass
docker compose stop
