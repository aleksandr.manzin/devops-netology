# Самоконтроль выполненения задания

> 1. Где расположен файл с `some_fact` из второго пункта задания?

  `group_vars/all/exmap.yml`

> 2. Какая команда нужна для запуска вашего `playbook` на окружении `test.yml`?

  `ansible-playbook -i inventory/test.yml site.yml`

> 3. Какой командой можно зашифровать файл?

  `ansible-vault encrypt group_vars/deb/examp.yml`

> 4. Какой командой можно расшифровать файл?

  `ansible-vault decrypt group_vars/deb/examp.yml`

> 5. Можно ли посмотреть содержимое зашифрованного файла без команды расшифровки файла? Если можно, то как?

  `ansible-vault view group_vars/deb/examp.yml`

> 6. Как выглядит команда запуска `playbook`, если переменные зашифрованы?

  Два варианта, которые я использовал:
  - `ansible-playbook --ask-vault-pass -i inventory/prod.yml site.yml`
  - `ansible-playbook -i inventory/prod.yml site.yml --vault-id @prompt`
  - также имеется вариант, в котором пароль будет браться из отдельного файла. Для этого нужно создать этот файл, указать в нем пароль и при запуске `playbook` просто подсовывать этот файл:
    `ansible-playbook -i inventory/prod.yml site.yml --vault-password-file .ansible_vault_pass`

    Однако, в данном случае, необходимо не забыть добавить этот файл в `.gitignore`, чтобы он не попал в систему контроля версий.
    Также можно исключить запуск с флагом `--vault-password-file` с помощью переменной среды `export ANSIBLE_VAULT_PASSWORD_FILE=./.ansible_vault_pass` и созданием конфига:
    ```bash
    $ cat ansible.cfg
    .......
    vault_password_file = ./.ansible_vault_pass
    ```

> 7. Как называется модуль подключения к host на windows?

  `winrm` - `windows remote management`

> 8. Приведите полный текст команды для поиска информации в документации ansible для модуля подключений ssh

  `ansible-doc -t connection ssh`

> 9. Какой параметр из модуля подключения `ssh` необходим для того, чтобы определить пользователя, под которым необходимо совершать подключение?

  `remote_user`
