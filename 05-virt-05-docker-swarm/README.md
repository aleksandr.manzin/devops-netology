# Домашнее задание к занятию "5.5. Оркестрация кластером Docker контейнеров на примере Docker Swarm"

## Задача 1

> Дайте письменые ответы на следующие вопросы:
> - В чём отличие режимов работы сервисов в Docker Swarm кластере: replication и global?

- `replication-режим` - режим, в котором указанное количество реплик одного сервиса распределяется по узлам, в соответствии со стратегией планирования. При этом есть вариант, что несколько экземпляров будет запущено на одной и той же ноде, поэтому если мы хотим запускать один экземпляр сервиса на одной ноде, то можно использовать параметр `replicas-max-per-node` со значением 1.
- `global-режим` - режим, в котором сервис разворачивается на каждой доступной ноде в кластере

> - Какой алгоритм выбора лидера используется в Docker Swarm кластере?

`Raft Consensus Algorithm` - алгоритм голосования, который позволяет выбрать нового лидера. Стоит заметить, что решение о выборе нового лидера определяется большинством управляющих нод, что означает, что рекомендуется делать нечетное количество управляющих нод.

> - Что такое Overlay Network?

`Overlay Network` - этот тип сети позволяет объединить между собой несколько хостов, на которых запущен `Docker`, что позволяет службам `Docker Swarm` взаимодействовать друг с другом в режиме кластера.

## Задача 2

> Создать ваш первый Docker Swarm кластер в Яндекс.Облаке
>
> Для получения зачета, вам необходимо предоставить скриншот из терминала (консоли), с выводом команды:
```
docker node ls
```

Для начала подготовим всё, что нам необходимо:
- создадим сеть и подсеть;
- проверим нашу подсеть:
  ```bash
  $ yc vpc subnet list
    +----------------------+-------------+----------------------+----------------+---------------+---------------+
  |          ID          |    NAME     |      NETWORK ID      | ROUTE TABLE ID |     ZONE      |     RANGE     |
  +----------------------+-------------+----------------------+----------------+---------------+---------------+
  | e9bhdq2juijafn1tagpg | my-subnet-a | enphh1e191c3s1smrg84 |                | ru-central1-a | [10.1.2.0/24] |
  +----------------------+-------------+----------------------+----------------+---------------+---------------+
  ```
- сделаем образ с помощью `Packer`, предварительно подготовив `centos-7-base.json`;
- проверяем создался ли образ:
  ```bash
  $ yc compute image list
  +----------------------+---------------+--------+----------------------+--------+
  |          ID          |     NAME      | FAMILY |     PRODUCT IDS      | STATUS |
  +----------------------+---------------+--------+----------------------+--------+
  | fd882fp86cvufcsuhr8u | centos-7-base | centos | f2euv1kekdgvc0jrpaet | READY  |
  +----------------------+---------------+--------+----------------------+--------+
  ```
  ![Packer](screenshots/centos_image.png "Centos image")
- удалим подсеть и сеть;
- создаём авторизованный ключ для сервисного аккаунта и выгружаем в `key.json`;
- переходим в директорию `terraform` и делаем инициализацию конфигурации, проверку конфигурации, запускаем план и применяем план;
- по завершении получаем список внутренних и внешних IP-адресов:
  ```bash
  Outputs:

  external_ip_address_node01 = "51.250.72.190"
  external_ip_address_node02 = "51.250.88.213"
  external_ip_address_node03 = "51.250.79.135"
  external_ip_address_node04 = "51.250.89.17"
  external_ip_address_node05 = "51.250.74.62"
  external_ip_address_node06 = "51.250.75.93"
  internal_ip_address_node01 = "192.168.101.11"
  internal_ip_address_node02 = "192.168.101.12"
  internal_ip_address_node03 = "192.168.101.13"
  internal_ip_address_node04 = "192.168.101.14"
  internal_ip_address_node05 = "192.168.101.15"
  internal_ip_address_node06 = "192.168.101.16"
  ```
- заходим на любой из серверов:
  ```bash
  $ ssh centos@51.250.72.190
  $ sudo -i
  $ docker node list
  ID                            HOSTNAME             STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
  9xg0hl35bpxayxlzndzq3raee *   node01.netology.yc   Ready     Active         Leader           20.10.17
  8pjc1wtne7c3u26mredyxobir     node02.netology.yc   Ready     Active         Reachable        20.10.17
  p1m4842ilexfvs1f29eyvd8w2     node03.netology.yc   Ready     Active         Reachable        20.10.17
  wlmjrh9cqit3nlmp7iiy29chn     node04.netology.yc   Ready     Active                          20.10.17
  nhdmfzfvd7kpngfzbs6vjaer7     node05.netology.yc   Ready     Active                          20.10.17
  umwen9myx6vl529m9sud80vqt     node06.netology.yc   Ready     Active                          20.10.17
  ```
  ![Docker node ls](screenshots/nodes.png "Docker node ls")

## Задача 3

> Создать ваш первый, готовый к боевой эксплуатации кластер мониторинга, состоящий из стека микросервисов.
>
> Для получения зачета, вам необходимо предоставить скриншот из терминала (консоли), с выводом команды:
  ```
  docker service ls
  ```

- с той же ноды посмотрим список задеплоенных микросервисов:
  ```bash
  $ docker service ls
  [root@node01 ~]# docker service ls
  ID             NAME                                MODE         REPLICAS   IMAGE                                          PORTS
  kzm76o0fjszr   swarm_monitoring_alertmanager       replicated   1/1        stefanprodan/swarmprom-alertmanager:v0.14.0    
  eb4jt8dmpagp   swarm_monitoring_caddy              replicated   1/1        stefanprodan/caddy:latest                      *:3000->3000/tcp, *:9090->9090/tcp, *:9093-9094->9093-9094/tcp
  oeg0y31jgo69   swarm_monitoring_cadvisor           global       6/6        google/cadvisor:latest                         
  110oyx27gco5   swarm_monitoring_dockerd-exporter   global       6/6        stefanprodan/caddy:latest                      
  bb4ictqvw1l0   swarm_monitoring_grafana            replicated   1/1        stefanprodan/swarmprom-grafana:5.3.4           
  1iwlz0kofmmp   swarm_monitoring_node-exporter      global       6/6        stefanprodan/swarmprom-node-exporter:v0.16.0   
  lkmfot3zcbm2   swarm_monitoring_prometheus         replicated   1/1        stefanprodan/swarmprom-prometheus:v2.5.0       
  e4ko4kuix9x7   swarm_monitoring_unsee              replicated   1/1        cloudflare/unsee:v0.8.0         
  ```
  ![Service list](screenshots/services.png "Service list")

## Задача 4 (*)

>Выполнить на лидере Docker Swarm кластера команду (указанную ниже) и дать письменное описание её функционала, что она делает и зачем она нужна:
```
> # см.документацию: https://docs.docker.com/engine/swarm/swarm_manager_locking/
docker swarm update --autolock=true
```

- выполним команду на управляющей ноде:
  ```bash
  [root@node01 ~]# docker swarm update --autolock=true
  Swarm updated.
  To unlock a swarm manager after it restarts, run the `docker swarm unlock`
  command and provide the following key:

      SWMKEY-1-2gcu9LXG37aNOtwCaVugFjbqHMl5/qpZhtk4dGlx5NY

  Please remember to store this key in a password manager, since without it you
  will not be able to restart the manager.
  ```

- в `Docker Swarm` используются ключи TLS для шифрования связи между нодами, а также для шифрования и дешифрования журналов Raft на диске. Так вот, если прописать параметр `--autolock`, то при перезагрузке менеджера, нужно будет в ручном режиме разблокировать этого менеджера, используя приведенный выше ключ шифрования ключа. Также следует заметить, что если ключ шифрования будет утерян или изменен (т.е. использована команда `docker swarm unlock-key --rotate`) в момент, когда менеджер был недоступен по какой-либо причине, то необходимо будет удалить менеджера из `Docker Swarm` и заново добавить его в качестве менеджера.

- попробуем немного поэкспериментировать. Откроем ещё одно терминала и подключимся ко второй ноде. Выведем список нод, а также проверим ключ шифрования для разблокировки менеджеров:
  ```bash
  $ ssh centos@51.250.88.213
  $ sudo -i
  $ docker node ls
  ID                            HOSTNAME             STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
  9xg0hl35bpxayxlzndzq3raee     node01.netology.yc   Ready     Active         Leader           20.10.17
  8pjc1wtne7c3u26mredyxobir *   node02.netology.yc   Ready     Active         Reachable        20.10.17
  p1m4842ilexfvs1f29eyvd8w2     node03.netology.yc   Ready     Active         Reachable        20.10.17
  wlmjrh9cqit3nlmp7iiy29chn     node04.netology.yc   Ready     Active                          20.10.17
  nhdmfzfvd7kpngfzbs6vjaer7     node05.netology.yc   Ready     Active                          20.10.17
  umwen9myx6vl529m9sud80vqt     node06.netology.yc   Ready     Active                          20.10.17
  $ docker swarm unlock-key
  To unlock a swarm manager after it restarts, run the `docker swarm unlock`
  command and provide the following key:

      SWMKEY-1-2gcu9LXG37aNOtwCaVugFjbqHMl5/qpZhtk4dGlx5NY

  Please remember to store this key in a password manager, since without it you
  will not be able to restart the manager.
  ```
- а теперь попробуем перезапустить `сервис Docker` на этой ноде и посмотреть, что из этого выйдет:
  ```bash
  $ systemctl restart docker
  ```
- на первой ноде(Лидере) запустим `watch docker node ls`, чтоб отобразить, что статус второй ноды изменился на `unreachable`:
  ```bash
  $ watch docker node ls
  ID                            HOSTNAME             STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
  9xg0hl35bpxayxlzndzq3raee *   node01.netology.yc   Ready     Active         Leader           20.10.17
  8pjc1wtne7c3u26mredyxobir     node02.netology.yc   Down      Active         Unreachable      20.10.17
  p1m4842ilexfvs1f29eyvd8w2     node03.netology.yc   Ready     Active         Reachable        20.10.17
  wlmjrh9cqit3nlmp7iiy29chn     node04.netology.yc   Ready     Active                          20.10.17
  nhdmfzfvd7kpngfzbs6vjaer7     node05.netology.yc   Ready     Active                          20.10.17
  umwen9myx6vl529m9sud80vqt     node06.netology.yc   Ready     Active                          20.10.17
  ```
- итак, мы видим, что вторая нода недоступна, но при этом статус `Docker` говорит о том, что сервис работает:
  ```bash
  $ systemctl status docker
  ● docker.service - Docker Application Container Engine
     Loaded: loaded (/usr/lib/systemd/system/docker.service; enabled; vendor preset: disabled)
     Active: active (running) since Вт 2022-06-28 19:21:25 UTC; 3min 26s ago
       Docs: https://docs.docker.com
   Main PID: 19190 (dockerd)
      Tasks: 10
     Memory: 35.2M
     CGroup: /system.slice/docker.service
             └─19190 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock

  июн 28 19:21:24 node02.netology.yc dockerd[19190]: time="2022-06-28T19:21:24.792479287Z" level=info msg="[graphdriver] using prior storage driver: overlay2"
  июн 28 19:21:24 node02.netology.yc dockerd[19190]: time="2022-06-28T19:21:24.800555864Z" level=info msg="Loading containers: start."
  июн 28 19:21:24 node02.netology.yc dockerd[19190]: time="2022-06-28T19:21:24.953035774Z" level=info msg="Default bridge (docker0) is assigned with an IP address 172.17.0.0/16. Daemon option --bi...d IP address"
  июн 28 19:21:25 node02.netology.yc dockerd[19190]: time="2022-06-28T19:21:25.007100013Z" level=info msg="Loading containers: done."
  июн 28 19:21:25 node02.netology.yc dockerd[19190]: time="2022-06-28T19:21:25.029468899Z" level=info msg="Docker daemon" commit=a89b842 graphdriver(s)=overlay2 version=20.10.17
  июн 28 19:21:25 node02.netology.yc dockerd[19190]: time="2022-06-28T19:21:25.035113828Z" level=error msg="cluster exited with error: Swarm is encrypted and needs to be unlocked before it can be ...o unlock it."
  июн 28 19:21:25 node02.netology.yc dockerd[19190]: time="2022-06-28T19:21:25.035178452Z" level=error msg="swarm component could not be started" error="Swarm is encrypted and needs to be unlocked...o unlock it."
  июн 28 19:21:25 node02.netology.yc dockerd[19190]: time="2022-06-28T19:21:25.035210044Z" level=info msg="Daemon has completed initialization"
  июн 28 19:21:25 node02.netology.yc systemd[1]: Started Docker Application Container Engine.
  июн 28 19:21:25 node02.netology.yc dockerd[19190]: time="2022-06-28T19:21:25.060319938Z" level=info msg="API listen on /var/run/docker.sock"
  Hint: Some lines were ellipsized, use -l to show in full.
  ```
- давайте разблокируем нашу вторую ноду, вернув её в `Docker Swarm`:
  ```bash
  $ docker swarm unlock
  Please enter unlock key: SWMKEY-1-H5YKxDj5XPd3ltihZmPO/qHlDu2K4ikon8kWE6avB5k
  $ docker node ls
  ID                            HOSTNAME             STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
  9xg0hl35bpxayxlzndzq3raee     node01.netology.yc   Ready     Active         Leader           20.10.17
  8pjc1wtne7c3u26mredyxobir *   node02.netology.yc   Ready     Active         Reachable        20.10.17
  p1m4842ilexfvs1f29eyvd8w2     node03.netology.yc   Ready     Active         Reachable        20.10.17
  wlmjrh9cqit3nlmp7iiy29chn     node04.netology.yc   Ready     Active                          20.10.17
  nhdmfzfvd7kpngfzbs6vjaer7     node05.netology.yc   Ready     Active                          20.10.17
  umwen9myx6vl529m9sud80vqt     node06.netology.yc   Ready     Active                          20.10.17
  ```
- и на `node01`, которая является лидером, увидим, что статус `node02` изменился на `Reachable`:
  ```bash
  ID                            HOSTNAME             STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
  9xg0hl35bpxayxlzndzq3raee *   node01.netology.yc   Ready     Active         Leader           20.10.17
  8pjc1wtne7c3u26mredyxobir     node02.netology.yc   Ready     Active         Reachable        20.10.17
  p1m4842ilexfvs1f29eyvd8w2     node03.netology.yc   Ready     Active         Reachable        20.10.17
  wlmjrh9cqit3nlmp7iiy29chn     node04.netology.yc   Ready     Active                          20.10.17
  nhdmfzfvd7kpngfzbs6vjaer7     node05.netology.yc   Ready     Active                          20.10.17
  umwen9myx6vl529m9sud80vqt     node06.netology.yc   Ready     Active                          20.10.17
  ```
