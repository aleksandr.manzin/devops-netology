# Домашнее задание к занятию "3.8. Компьютерные сети, лекция 3"

 1. > Подключитесь к публичному маршрутизатору в интернет. Найдите маршрут к вашему публичному IP:
 
	```bash
	vagrant@vagrant:~$ telnet route-server.opentransit.net
	Trying 204.59.3.38...
	Connected to route-server.opentransit.net.

    *** Log in with username 'rviews', password 'Rviews' ***

	User Access Verification

	Username: rviews
	Password:
	OAKRS1#show ip route 188.243.161.167
	Routing entry for 188.242.0.0/15, supernet
	  Known via "bgp 5511", distance 200, metric 100
	  Tag 9002, type internal
	  Last update from 193.251.250.190 7w0d ago
	  Routing Descriptor Blocks:
	  * 193.251.250.190, from 193.251.140.1, 7w0d ago
		  Route metric is 100, traffic share count is 1
		  AS Hops 2
		  Route tag 9002
		  MPLS label: none
		  MPLS Flags: NSF
    ```
 	
	Видим, что до AS, принадлежащей провайдеру SkyNet, 2 прыжка(hop'a).
	
    ```bash
	OAKRS1#show bgp 188.243.161.167
    % Command accepted but obsolete, unreleased or unsupported; see documentation.
    BGP routing table entry for 188.242.0.0/15, version 284956638
    Paths: (3 available, best #3, table default)
    Multipath: eBGP
      Not advertised to any peer
      Refresh Epoch 1
      9002 35807
        193.251.250.190 from 193.251.245.87 (172.25.4.22)
          Origin IGP, metric 100, localpref 85, valid, internal, atomic-aggregate
          Community: 5511:507 5511:666 5511:710
          Originator: 172.25.4.85, Cluster list: 0.0.0.100
          rx pathid: 0, tx pathid: 0
          Updated on Feb 21 2022 19:09:31 EST
      Refresh Epoch 1
      9002 35807
        193.251.250.190 from 193.251.245.38 (172.25.4.12)
          Origin IGP, metric 100, localpref 85, valid, internal, atomic-aggregate
          Community: 5511:507 5511:666 5511:710
          Originator: 172.25.4.85, Cluster list: 0.0.0.100
          rx pathid: 0, tx pathid: 0
          Updated on Feb 8 2022 18:22:20 EST
      Refresh Epoch 1
      9002 35807
        193.251.250.190 from 193.251.140.1 (172.25.4.136)
          Origin IGP, metric 100, localpref 85, valid, internal, atomic-aggregate, best
          Community: 5511:507 5511:666 5511:710
          Originator: 172.25.4.85, Cluster list: 0.0.0.100
          rx pathid: 0, tx pathid: 0x0
          Updated on Dec 9 2021 17:37:16 EST
	```
	
	Используется eBGP. Видим, что указано две AS - AS с номер 9002 и AS с номером 35807 (AS провайдера SkyNet).
	Выбран наилучший маршрут, который определен с помощью параметра AS-Path (чем меньше список номеров AS, тем предпочтительнее маршрут). Найдем запись в таблице исключительно для нашей сети:
	```bash
	OAKRS1#show bgp | include 188.242.0.0/15
	* i  188.242.0.0/15   193.251.250.190        100     85      0 9002 35807 i
	```
	Как видим, в AS-path указано две AS (9002 35807)

2. > Создайте dummy0 интерфейс в Ubuntu. Добавьте несколько статических маршрутов. Проверьте таблицу маршрутизации.

	Первым делом нам необходимо загрузить модуль `dummy`:
	```bash
	vagrant@vagrant:~$ sudo modprobe dummy
	vagrant@vagrant:~$ lsmod | grep dummy
	dummy                  16384  0
	```
	Теперь можем перейти к настройкам интерфейса. Т.к. мы хотим, чтоб у нас был персистентный интерфейс, мы воспользуемся `netplan`:
	```bash
	vagrant@vagrant:~$ sudo touch /etc/netplan/02-dummy.yaml
	vagrant@vagrant:~$ sudo nano /etc/netplan/02-dummy.yaml
	```
	Вносим в файл следующие данные:
    ```
    network:
      version: 2
      bridges:
        dummy0:
          dhcp4: no
          addresses: [10.0.2.20/24]
    ```
	Проверим созданный нами файл:
	```bash
	vagrant@vagrant:~$ sudo netplan try
	```
	Видим, что ошибок нет и можно применить внесенные изменения и проверять наш интерфейс:
	```bash
	vagrant@vagrant:~$ sudo netplan apply
	vagrant@vagrant:/etc/netplan$ ip -c -br -4 a ls up
	lo               UNKNOWN        127.0.0.1/8
	eth0             UP             10.0.2.15/24
	dummy0           UNKNOWN        10.0.2.20/32
	```
	Интерфейс поднялся и функционирует. Теперь перейдём к статическим маршрутам. Для начала включим маршрутизацию:
    ```bash
    vagrant@vagrant:~$ sudo sysctl -w net.ipv4.ip_forward=1
    vagrant@vagrant:~$ sudo sysctl -a | grep ipv4.ip_forward
    net.ipv4.ip_forward = 1
    net.ipv4.ip_forward_update_priority = 1
    net.ipv4.ip_forward_use_pmtu = 0
    ```
	Каково же было моё разочарование, что `netplan` не поддерживает `dummy-интерфейсы`. Что очень странно, ведь по умолчанию в Ubuntu нет установленного пакета `net-tools` и ставить его специально для создания `dummy-интерфейса` как минимум странно. А жаль, только проникся `netplan`, очень прост и удобен в использовании. И непонятно тогда зачем использовать его, если можно по-старинке использовать `/etc/network/interfaces`. По сути, можно сделать `bridge` без указания физического интерфейса:
	Изменим наш файл `/etc/netplan/02-dummy.yaml`:
    ```
    network:
      version: 2
      bridges:
        dummy0:
          dhcp4: no
          addresses: [10.0.2.20/32]
          routes:
            - to: 10.0.2.20/32
              via: 10.0.2.2
              metric: 100
              on-link: yes
            - to: 10.0.2.0/24
              via: 10.0.2.2
              metric: 100
              on-link: yes
    ```
	Подтвердим нашу конфигурацию и проверим маршруты:
    ```bash
    vagrant@vagrant:~$ sudo netplan apply
    vagrant@vagrant:~$ ip -c route
    default via 10.0.2.2 dev eth0 proto dhcp src 10.0.2.15 metric 100
    10.0.2.0/24 dev eth0 proto kernel scope link src 10.0.2.15
    10.0.2.0/24 via 10.0.2.2 dev dummy0 proto static metric 100 onlink
    10.0.2.2 dev eth0 proto dhcp scope link src 10.0.2.15 metric 100
    10.0.2.20 via 10.0.2.2 dev dummy0 proto static metric 100 onlink
    ```
	P.s. если моё решение не имеет места быть, разумеется, готов переделать и использовать пакет `net-tools` для полноценной реализации `dummy`. 

3. > Проверьте открытые TCP порты в Ubuntu
    
    Посмотреть LISTEN-порты можно с помощью утилиты: 
	
	* `ss`:
    ```bash
    vagrant@vagrant:~$ sudo ss -lntp
    State    Recv-Q   Send-Q      Local Address:Port       Peer Address:Port   Process
    LISTEN   0        4096        127.0.0.53%lo:53              0.0.0.0:*       users:(("systemd-resolve",pid=632,fd=13))
    LISTEN   0        128               0.0.0.0:22              0.0.0.0:*       users:(("sshd",pid=711,fd=3))
    LISTEN   0        128                  [::]:22                 [::]:*       users:(("sshd",pid=711,fd=4))
	```
	
	* `lsof`:
    ```bash
    vagrant@vagrant:~$ sudo lsof -nPi | grep -i listen
    systemd-r  632 systemd-resolve   13u  IPv4  19933      0t0  TCP 127.0.0.53:53 (LISTEN)
    sshd       711            root    3u  IPv4  24215      0t0  TCP *:22 (LISTEN)
    sshd       711            root    4u  IPv6  24217      0t0  TCP *:22 (LISTEN)
	```
	
	> Какие протоколы и приложения используют эти порты? Приведите несколько примеров.
	
	* Local Address:Port:
	    * `127.0.0.53:53` - прослушивается 53 порт для адреса 127.0.0.53. Локальный кэширующий dns-сервер (`systemd-resolved - dns`)
	    * 0.0.0.0:22 - прослушивается 22 порт для подключений с любого адреса в локальной сети (`ssh`);
	    * [::]:22 - то же самое, только для IPv6 (`ssh`).
	* Peer Address:Port
	    * 0.0.0.0:* - принимаются соединения с любого IP-адреса, отправленные с любого порта
		* [::]:* - то же самое, только для IPv6
    
4. > Проверьте используемые UDP сокеты в Ubuntu
    С помощью утилиты:
    * `ss`:
    ```bash
    vagrant@vagrant:~$ sudo ss -anup
    State    Recv-Q   Send-Q      Local Address:Port       Peer Address:Port   Process
    UNCONN   0        0           127.0.0.53%lo:53              0.0.0.0:*       users:(("systemd-resolve",pid=632,fd=12))
    UNCONN   0        0          10.0.2.15%eth0:68              0.0.0.0:*       users:(("systemd-network",pid=627,fd=19))
	```
    
	* `lsof`:
    ```bash
    vagrant@vagrant:~$ 
    systemd-n 627 systemd-network   19u  IPv4  21474      0t0  UDP 10.0.2.15:68
    systemd-r 632 systemd-resolve   12u  IPv4  19932      0t0  UDP 127.0.0.53:53
    ```
	
    > Какие протоколы и приложения используют эти порты?
	* `systemd-resolved` - 127.0.0.53:53 - `локальный dns-резолвер`;
	* `systemd-networkd` - 10.0.2.15:68 - `DHCP - протокол динамической конфигурации хостов`.
	
5. > Используя diagrams.net, создайте L3 диаграмму вашей домашней сети или любой другой сети, с которой вы работали.

	![HomeNetwork](screenshots/homenetwork.png "HomeNetwork")