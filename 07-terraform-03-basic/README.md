# Домашнее задание к занятию "7.3. Основы и принцип работы Терраформ"

## Задача 1. Создадим бэкэнд в S3 (необязательно, но крайне желательно).

> Если в рамках предыдущего задания у вас уже есть аккаунт AWS, то давайте продолжим знакомство со взаимодействием
терраформа и aws.
>
> 1. Создайте s3 бакет, iam роль и пользователя от которого будет работать терраформ. Можно создать отдельного пользователя, а можно использовать созданного в рамках предыдущего задания, просто добавьте ему необходимы права, как описано [здесь](https://www.terraform.io/docs/backends/types/s3.html).
> 1. Зарегистрируйте бэкэнд в терраформ проекте как описано по ссылке выше.

- первоначально был создан сервисный аккаунт:
  ```tf
  // создание сервисного аккаунта "bucket_sa"
  resource "yandex_iam_service_account" "sa" {
    name = "sa"
  }

  // Назначение роли сервисному аккаунту
  resource "yandex_resourcemanager_folder_iam_member" "sa-editor" {
    folder_id = var.yc_folder_id
    role      = "storage.editor"
    member    = "serviceAccount:${yandex_iam_service_account.sa.id}"
  }

  // Создание статического ключа доступа
  resource "yandex_iam_service_account_static_access_key" "sa-static-key" {
    service_account_id = yandex_iam_service_account.sa.id
    description        = "static access key for object storage"
  }
  ```
- далее, в файле `instances.tf` добавили ресурс, создающий бакет:
  ```tf
  // Создание бакета с использованием ключа
  resource "yandex_storage_bucket" "aleksandrmanzin-s3-storage" {
    access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
    secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
    bucket     = var.bucket
  }
  ```
- подправлены файлы, созданы новые переменные;
- произведена инициализация терраформа, просмотрен план:
  ```bash
  $ terraform init
  Terraform has been successfully initialized!
  $ terraform plan
  Terraform will perform the following actions:

  # yandex_compute_instance.instance[0] will be created
  + resource "yandex_compute_instance" "instance" {
      + allow_stopping_for_update = true
      + hostname                  = "node01.netology.cloud"
      + name                      = "node01"
  ---------------------
  Plan: 7 to add, 0 to change, 0 to destroy.
  ```

## Задача 2. Инициализируем проект и создаем воркспейсы.

> 1. Выполните `terraform init`:
>    * если был создан бэкэнд в S3, то терраформ создат файл стейтов в S3 и запись в таблице dynamodb.
>    * иначе будет создан локальный файл со стейтами.  
> 1. Создайте два воркспейса `stage` и `prod`.
> 1. В уже созданный `aws_instance` добавьте зависимость типа инстанса от вокспейса, что бы в разных ворскспейсах использовались разные `instance_type`.
> 1. Добавим `count`. Для `stage` должен создаться один экземпляр `ec2`, а для `prod` два.
> 1. Создайте рядом еще один `aws_instance`, но теперь определите их количество при помощи `for_each`, а не `count`.
> 1. Что бы при изменении типа инстанса не возникло ситуации, когда не будет ни одного инстанса добавьте параметр жизненного цикла `create_before_destroy = true` в один из рессурсов `aws_instance`.
> 1. При желании поэкспериментируйте с другими параметрами и рессурсами.
>
> В виде результата работы пришлите:
> * Вывод команды `terraform workspace list`.
> * Вывод команды `terraform plan` для воркспейса `prod`.  

- создадим 2 воркспейса:
  ```bash
  $ terraform workspace new prod
  Created and switched to workspace "prod"!
  $ terraform workspace new stage
  Created and switched to workspace "stage"!
  ```

- создадим файл `locals.tf`, в котором создадим переменные, с помощью которых  будут подстраиваться настройки под наши воркспейсы:
  ```tf
  locals {
    replications = {
      default = 1
      stage   = 1
      prod    = 2
    }

    cores = {
      default = 2
      stage   = 2
      prod    = 2
    }

    memory = {
      default = 2
      stage   = 2
      prod    = 4
    }

    boot_disk_type = {
      default = "network-hdd"
      stage   = "network-hdd"
      prod    = "network-nvme"
    }

    boot_disk_size = {
      default = 20
      stage   = 20
      prod    = 40
    }

    vpc_subnet = {
      default = ["10.10.1.0/24"]
      stage   = ["10.10.1.0/24"]
      prod    = ["10.100.1.0/24"]
    }
  }
  ```
- теперь подправим немного наши файлы `instances.tf` и `network.tf`:
  - `network.tf`
    ```tf
    resource "yandex_vpc_network" "default" {
      name = "net-${terraform.workspace}"
    }

    resource "yandex_vpc_subnet" "default" {
      name           = "subnet-${terraform.workspace}"
      zone           = var.yc_zone
      network_id     = yandex_vpc_network.default.id
      v4_cidr_blocks = local.vpc_subnet[terraform.workspace]
    }
    ```
  - `instances.tf`
    ```tf
    // получение последнего образа centos 8
    data "yandex_compute_image" "image" {
      family = var.yc_image_family
    }

    // создание инстанса с использованием count
    resource "yandex_compute_instance" "instance" {
      count                     = local.replications[terraform.workspace]
      name                      = "${terraform.workspace}-${var.name}${format(var.count_format, count.index + 1)}"
      zone                      = var.yc_zone
      hostname                  = "${terraform.workspace}-${var.name}${format(var.count_format, count.index + 1)}.${var.hostname}"
      folder_id                 = var.yc_folder_id
      allow_stopping_for_update = true

      resources {
        cores  = local.cores[terraform.workspace]
        memory = local.memory[terraform.workspace]
      }

      boot_disk {
        initialize_params {
          name     = terraform.workspace
          image_id = data.yandex_compute_image.image.id
          type     = local.boot_disk_type[terraform.workspace]
          size     = local.boot_disk_size[terraform.workspace]
        }
      }

      network_interface {
        subnet_id = yandex_vpc_subnet.default.id
        nat       = true
      }

      metadata = {
        ssh-keys = "var.username:${file("~/.ssh/id_rsa.pub")}"
      }

      lifecycle {
        create_before_destroy = true
      }
    }

    // Создание бакета с использованием ключа
    resource "yandex_storage_bucket" "aleksandrmanzin-s3-storage" {
      access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
      secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
      bucket     = var.bucket
    }
    ```

- список воркспейсов:
  ```bash
  $ terraform workspace list
  default
  * prod
  stage
  ```

- самое время проверить план:
  ```bash
  $ terraform plan
  data.yandex_compute_image.image: Reading...
  data.yandex_compute_image.image: Read complete after 4s [id=fd86tafe9jg6c4hd2aqp]

  Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
    + create

  Terraform will perform the following actions:

    # yandex_compute_instance.instance[0] will be created
    + resource "yandex_compute_instance" "instance" {
        + allow_stopping_for_update = true
        + created_at                = (known after apply)
        + folder_id                 = "b1gl0fkdpeiilq9hi2b3"
        + fqdn                      = (known after apply)
        + hostname                  = "prod-node01.netology.cloud"
        + id                        = (known after apply)
        + metadata                  = {
            + "ssh-keys" = <<-EOT
                  var.username:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDRr9DnS4fKgblXq2L7rsKxuZyhy5ANn9AJLZ+EiQ/HuYNq3s2h9HJw1mIbIt4+28FKbl0/jR1VdNIgU7PuSzuDccq7xP9g12v+Wpv9c+GAm2GeU3HqewHN/DdQLRWEmZFta26GCUBo4tbESSX7HCITQxUmFpUVOyFWFB09orA+dtb23Yr7Cg83jczaTO8Jt7/Qy53NaK1VcyjKZjIzAT0Yew8DrW9VRyvquHtJJYdgh8S1SHTQVOp7JK00z52hJbFwXXKZ6V5oIUoeAXxpr0CYoSYIkE8Avk/uH0KIQdBj97HF1KlA3dRU4ECKHKJh2NWy3cCKHjmfLxGko/Ccx2Dew8LQBUWqWoeL9PE152iYDqzQXST7ih3C3bPhy4OZxHjWNXK0gsbPFyWirFjEZ9sOJtMK/4WEE8Z3BdyMkF3WGAlAdom2PtlTCRqn4rDogWcNO/TxBxXMv1tUrMPZGTCRU0XMZ+DiTGQKkL452Ofm6Z0IP+rRjcFDf1vzBB4lurs= aleksandr@manzin
              EOT
          }
        + name                      = "prod-node01"
        + network_acceleration_type = "standard"
        + platform_id               = "standard-v1"
        + service_account_id        = (known after apply)
        + status                    = (known after apply)
        + zone                      = "ru-central1-a"

        + boot_disk {
            + auto_delete = true
            + device_name = (known after apply)
            + disk_id     = (known after apply)
            + mode        = (known after apply)

            + initialize_params {
                + block_size  = (known after apply)
                + description = (known after apply)
                + image_id    = "fd86tafe9jg6c4hd2aqp"
                + name        = "prod-01"
                + size        = 40
                + snapshot_id = (known after apply)
                + type        = "network-nvme"
              }
          }

        + network_interface {
            + index              = (known after apply)
            + ip_address         = (known after apply)
            + ipv4               = true
            + ipv6               = (known after apply)
            + ipv6_address       = (known after apply)
            + mac_address        = (known after apply)
            + nat                = true
            + nat_ip_address     = (known after apply)
            + nat_ip_version     = (known after apply)
            + security_group_ids = (known after apply)
            + subnet_id          = (known after apply)
          }

        + placement_policy {
            + host_affinity_rules = (known after apply)
            + placement_group_id  = (known after apply)
          }

        + resources {
            + core_fraction = 100
            + cores         = 2
            + memory        = 4
          }

        + scheduling_policy {
            + preemptible = (known after apply)
          }
      }

    # yandex_compute_instance.instance[1] will be created
    + resource "yandex_compute_instance" "instance" {
        + allow_stopping_for_update = true
        + created_at                = (known after apply)
        + folder_id                 = "b1gl0fkdpeiilq9hi2b3"
        + fqdn                      = (known after apply)
        + hostname                  = "prod-node02.netology.cloud"
        + id                        = (known after apply)
        + metadata                  = {
            + "ssh-keys" = <<-EOT
                  var.username:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDRr9DnS4fKgblXq2L7rsKxuZyhy5ANn9AJLZ+EiQ/HuYNq3s2h9HJw1mIbIt4+28FKbl0/jR1VdNIgU7PuSzuDccq7xP9g12v+Wpv9c+GAm2GeU3HqewHN/DdQLRWEmZFta26GCUBo4tbESSX7HCITQxUmFpUVOyFWFB09orA+dtb23Yr7Cg83jczaTO8Jt7/Qy53NaK1VcyjKZjIzAT0Yew8DrW9VRyvquHtJJYdgh8S1SHTQVOp7JK00z52hJbFwXXKZ6V5oIUoeAXxpr0CYoSYIkE8Avk/uH0KIQdBj97HF1KlA3dRU4ECKHKJh2NWy3cCKHjmfLxGko/Ccx2Dew8LQBUWqWoeL9PE152iYDqzQXST7ih3C3bPhy4OZxHjWNXK0gsbPFyWirFjEZ9sOJtMK/4WEE8Z3BdyMkF3WGAlAdom2PtlTCRqn4rDogWcNO/TxBxXMv1tUrMPZGTCRU0XMZ+DiTGQKkL452Ofm6Z0IP+rRjcFDf1vzBB4lurs= aleksandr@manzin
              EOT
          }
        + name                      = "prod-node02"
        + network_acceleration_type = "standard"
        + platform_id               = "standard-v1"
        + service_account_id        = (known after apply)
        + status                    = (known after apply)
        + zone                      = "ru-central1-a"

        + boot_disk {
            + auto_delete = true
            + device_name = (known after apply)
            + disk_id     = (known after apply)
            + mode        = (known after apply)

            + initialize_params {
                + block_size  = (known after apply)
                + description = (known after apply)
                + image_id    = "fd86tafe9jg6c4hd2aqp"
                + name        = "prod-02"
                + size        = 40
                + snapshot_id = (known after apply)
                + type        = "network-nvme"
              }
          }

        + network_interface {
            + index              = (known after apply)
            + ip_address         = (known after apply)
            + ipv4               = true
            + ipv6               = (known after apply)
            + ipv6_address       = (known after apply)
            + mac_address        = (known after apply)
            + nat                = true
            + nat_ip_address     = (known after apply)
            + nat_ip_version     = (known after apply)
            + security_group_ids = (known after apply)
            + subnet_id          = (known after apply)
          }

        + placement_policy {
            + host_affinity_rules = (known after apply)
            + placement_group_id  = (known after apply)
          }

        + resources {
            + core_fraction = 100
            + cores         = 2
            + memory        = 4
          }

        + scheduling_policy {
            + preemptible = (known after apply)
          }
      }

    # yandex_iam_service_account.sa will be created
    + resource "yandex_iam_service_account" "sa" {
        + created_at = (known after apply)
        + folder_id  = (known after apply)
        + id         = (known after apply)
        + name       = "sa"
      }

    # yandex_iam_service_account_static_access_key.sa-static-key will be created
    + resource "yandex_iam_service_account_static_access_key" "sa-static-key" {
        + access_key           = (known after apply)
        + created_at           = (known after apply)
        + description          = "static access key for object storage"
        + encrypted_secret_key = (known after apply)
        + id                   = (known after apply)
        + key_fingerprint      = (known after apply)
        + secret_key           = (sensitive value)
        + service_account_id   = (known after apply)
      }

    # yandex_resourcemanager_folder_iam_member.sa-editor will be created
    + resource "yandex_resourcemanager_folder_iam_member" "sa-editor" {
        + folder_id = "b1gl0fkdpeiilq9hi2b3"
        + id        = (known after apply)
        + member    = (known after apply)
        + role      = "storage.editor"
      }

    # yandex_storage_bucket.aleksandrmanzin-s3-storage will be created
    + resource "yandex_storage_bucket" "aleksandrmanzin-s3-storage" {
        + access_key            = (known after apply)
        + acl                   = "private"
        + bucket                = "tf-state-bucket-aleksandrmanzin"
        + bucket_domain_name    = (known after apply)
        + default_storage_class = (known after apply)
        + folder_id             = (known after apply)
        + force_destroy         = false
        + id                    = (known after apply)
        + secret_key            = (sensitive value)
        + website_domain        = (known after apply)
        + website_endpoint      = (known after apply)

        + anonymous_access_flags {
            + list = (known after apply)
            + read = (known after apply)
          }

        + versioning {
            + enabled = (known after apply)
          }
      }

    # yandex_vpc_network.default will be created
    + resource "yandex_vpc_network" "default" {
        + created_at                = (known after apply)
        + default_security_group_id = (known after apply)
        + folder_id                 = (known after apply)
        + id                        = (known after apply)
        + labels                    = (known after apply)
        + name                      = "net-prod"
        + subnet_ids                = (known after apply)
      }

    # yandex_vpc_subnet.default will be created
    + resource "yandex_vpc_subnet" "default" {
        + created_at     = (known after apply)
        + folder_id      = (known after apply)
        + id             = (known after apply)
        + labels         = (known after apply)
        + name           = "subnet-prod"
        + network_id     = (known after apply)
        + v4_cidr_blocks = [
            + "10.100.1.0/24",
          ]
        + v6_cidr_blocks = (known after apply)
        + zone           = "ru-central1-a"
      }

  Plan: 8 to add, 0 to change, 0 to destroy.

  Changes to Outputs:
    + access_key           = (sensitive value)
    + hostnames            = [
        + "prod-node01.netology.cloud",
        + "prod-node02.netology.cloud",
      ]
    + instance_external_ip = [
        + (known after apply),
        + (known after apply),
      ]
    + instance_internal_ip = [
        + (known after apply),
        + (known after apply),
      ]
    + static_key           = (sensitive value)

  ```
  Как видим, у нас будут созданы две ВМ с настройками, которые мы задавали с помощью локальных переменных.

- и раз, у нас в задании имеется ещё `for_each`, который нельзя использовать одновременно с `count`, то создадим новый файлик `server.tf`, в котором воспользуемся циклом (файл `instances.tf` закомментируем, чтоб не увеличивать и без того большой вывод плана):
  ```tf
  // получение последнего образа centos 8
  data "yandex_compute_image" "image" {
    family = var.yc_image_family
  }

  // создание инстанса с использованием for_each
  resource "yandex_compute_instance" "server" {
    for_each = {
      web = { "type" : "m2.micro", "public_ip" : true, "cores" : 2, "memory" : 4, "size" : 20 },
      db  = { "type" : "b2.medium", "public_ip" : false, "cores" : 4, "memory" : 16, "size" : 100 }
    }

    name                      = "${each.key}-${terraform.workspace}-${var.name}"
    zone                      = var.yc_zone
    hostname                  = "${each.key}-${terraform.workspace}-${var.name}.${var.hostname}"
    folder_id                 = var.yc_folder_id
    allow_stopping_for_update = true

    resources {
      cores  = each.value["cores"]
      memory = each.value["memory"]
    }

    boot_disk {
      initialize_params {
        name     = "${terraform.workspace}-environment"
        image_id = data.yandex_compute_image.image.id
        type     = each.value["type"]
        size     = each.value["size"]
      }
    }

    network_interface {
      subnet_id = yandex_vpc_subnet.default.id
      nat       = true
    }

    metadata = {
      ssh-keys = "var.username:${file("~/.ssh/id_rsa.pub")}"
    }

    lifecycle {
      create_before_destroy = true
    }
  }

  // Создание бакета с использованием ключа
  resource "yandex_storage_bucket" "aleksandrmanzin-s3-storage" {
    access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
    secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
    bucket     = var.bucket
  }
  ```
- выполним `terraform plan`:
  ```bash
  $ terraform plan
  data.yandex_compute_image.image: Reading...
  data.yandex_compute_image.image: Read complete after 4s [id=fd86tafe9jg6c4hd2aqp]

  Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
    + create

  Terraform will perform the following actions:

    # yandex_compute_instance.server["db"] will be created
    + resource "yandex_compute_instance" "server" {
        + allow_stopping_for_update = true
        + created_at                = (known after apply)
        + folder_id                 = "b1gl0fkdpeiilq9hi2b3"
        + fqdn                      = (known after apply)
        + hostname                  = "db-prod-node.netology.cloud"
        + id                        = (known after apply)
        + metadata                  = {
            + "ssh-keys" = <<-EOT
                  var.username:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDRr9DnS4fKgblXq2L7rsKxuZyhy5ANn9AJLZ+EiQ/HuYNq3s2h9HJw1mIbIt4+28FKbl0/jR1VdNIgU7PuSzuDccq7xP9g12v+Wpv9c+GAm2GeU3HqewHN/DdQLRWEmZFta26GCUBo4tbESSX7HCITQxUmFpUVOyFWFB09orA+dtb23Yr7Cg83jczaTO8Jt7/Qy53NaK1VcyjKZjIzAT0Yew8DrW9VRyvquHtJJYdgh8S1SHTQVOp7JK00z52hJbFwXXKZ6V5oIUoeAXxpr0CYoSYIkE8Avk/uH0KIQdBj97HF1KlA3dRU4ECKHKJh2NWy3cCKHjmfLxGko/Ccx2Dew8LQBUWqWoeL9PE152iYDqzQXST7ih3C3bPhy4OZxHjWNXK0gsbPFyWirFjEZ9sOJtMK/4WEE8Z3BdyMkF3WGAlAdom2PtlTCRqn4rDogWcNO/TxBxXMv1tUrMPZGTCRU0XMZ+DiTGQKkL452Ofm6Z0IP+rRjcFDf1vzBB4lurs= aleksandr@manzin
              EOT
          }
        + name                      = "db-prod-node"
        + network_acceleration_type = "standard"
        + platform_id               = "standard-v1"
        + service_account_id        = (known after apply)
        + status                    = (known after apply)
        + zone                      = "ru-central1-a"

        + boot_disk {
            + auto_delete = true
            + device_name = (known after apply)
            + disk_id     = (known after apply)
            + mode        = (known after apply)

            + initialize_params {
                + block_size  = (known after apply)
                + description = (known after apply)
                + image_id    = "fd86tafe9jg6c4hd2aqp"
                + name        = "prod-db"
                + size        = 100
                + snapshot_id = (known after apply)
                + type        = "network-nvme"
              }
          }

        + network_interface {
            + index              = (known after apply)
            + ip_address         = (known after apply)
            + ipv4               = true
            + ipv6               = (known after apply)
            + ipv6_address       = (known after apply)
            + mac_address        = (known after apply)
            + nat                = true
            + nat_ip_address     = (known after apply)
            + nat_ip_version     = (known after apply)
            + security_group_ids = (known after apply)
            + subnet_id          = (known after apply)
          }

        + placement_policy {
            + host_affinity_rules = (known after apply)
            + placement_group_id  = (known after apply)
          }

        + resources {
            + core_fraction = 100
            + cores         = 4
            + memory        = 16
          }

        + scheduling_policy {
            + preemptible = (known after apply)
          }
      }

    # yandex_compute_instance.server["web"] will be created
    + resource "yandex_compute_instance" "server" {
        + allow_stopping_for_update = true
        + created_at                = (known after apply)
        + folder_id                 = "b1gl0fkdpeiilq9hi2b3"
        + fqdn                      = (known after apply)
        + hostname                  = "web-prod-node.netology.cloud"
        + id                        = (known after apply)
        + metadata                  = {
            + "ssh-keys" = <<-EOT
                  var.username:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDRr9DnS4fKgblXq2L7rsKxuZyhy5ANn9AJLZ+EiQ/HuYNq3s2h9HJw1mIbIt4+28FKbl0/jR1VdNIgU7PuSzuDccq7xP9g12v+Wpv9c+GAm2GeU3HqewHN/DdQLRWEmZFta26GCUBo4tbESSX7HCITQxUmFpUVOyFWFB09orA+dtb23Yr7Cg83jczaTO8Jt7/Qy53NaK1VcyjKZjIzAT0Yew8DrW9VRyvquHtJJYdgh8S1SHTQVOp7JK00z52hJbFwXXKZ6V5oIUoeAXxpr0CYoSYIkE8Avk/uH0KIQdBj97HF1KlA3dRU4ECKHKJh2NWy3cCKHjmfLxGko/Ccx2Dew8LQBUWqWoeL9PE152iYDqzQXST7ih3C3bPhy4OZxHjWNXK0gsbPFyWirFjEZ9sOJtMK/4WEE8Z3BdyMkF3WGAlAdom2PtlTCRqn4rDogWcNO/TxBxXMv1tUrMPZGTCRU0XMZ+DiTGQKkL452Ofm6Z0IP+rRjcFDf1vzBB4lurs= aleksandr@manzin
              EOT
          }
        + name                      = "web-prod-node"
        + network_acceleration_type = "standard"
        + platform_id               = "standard-v1"
        + service_account_id        = (known after apply)
        + status                    = (known after apply)
        + zone                      = "ru-central1-a"

        + boot_disk {
            + auto_delete = true
            + device_name = (known after apply)
            + disk_id     = (known after apply)
            + mode        = (known after apply)

            + initialize_params {
                + block_size  = (known after apply)
                + description = (known after apply)
                + image_id    = "fd86tafe9jg6c4hd2aqp"
                + name        = "prod-web"
                + size        = 20
                + snapshot_id = (known after apply)
                + type        = "network-hdd"
              }
          }

        + network_interface {
            + index              = (known after apply)
            + ip_address         = (known after apply)
            + ipv4               = true
            + ipv6               = (known after apply)
            + ipv6_address       = (known after apply)
            + mac_address        = (known after apply)
            + nat                = true
            + nat_ip_address     = (known after apply)
            + nat_ip_version     = (known after apply)
            + security_group_ids = (known after apply)
            + subnet_id          = (known after apply)
          }

        + placement_policy {
            + host_affinity_rules = (known after apply)
            + placement_group_id  = (known after apply)
          }

        + resources {
            + core_fraction = 100
            + cores         = 2
            + memory        = 4
          }

        + scheduling_policy {
            + preemptible = (known after apply)
          }
      }

    # yandex_iam_service_account.sa will be created
    + resource "yandex_iam_service_account" "sa" {
        + created_at = (known after apply)
        + folder_id  = (known after apply)
        + id         = (known after apply)
        + name       = "sa"
      }

    # yandex_iam_service_account_static_access_key.sa-static-key will be created
    + resource "yandex_iam_service_account_static_access_key" "sa-static-key" {
        + access_key           = (known after apply)
        + created_at           = (known after apply)
        + description          = "static access key for object storage"
        + encrypted_secret_key = (known after apply)
        + id                   = (known after apply)
        + key_fingerprint      = (known after apply)
        + secret_key           = (sensitive value)
        + service_account_id   = (known after apply)
      }

    # yandex_resourcemanager_folder_iam_member.sa-editor will be created
    + resource "yandex_resourcemanager_folder_iam_member" "sa-editor" {
        + folder_id = "b1gl0fkdpeiilq9hi2b3"
        + id        = (known after apply)
        + member    = (known after apply)
        + role      = "storage.editor"
      }

    # yandex_storage_bucket.aleksandrmanzin-s3-storage will be created
    + resource "yandex_storage_bucket" "aleksandrmanzin-s3-storage" {
        + access_key            = (known after apply)
        + acl                   = "private"
        + bucket                = "tf-state-bucket-aleksandrmanzin"
        + bucket_domain_name    = (known after apply)
        + default_storage_class = (known after apply)
        + folder_id             = (known after apply)
        + force_destroy         = false
        + id                    = (known after apply)
        + secret_key            = (sensitive value)
        + website_domain        = (known after apply)
        + website_endpoint      = (known after apply)

        + anonymous_access_flags {
            + list = (known after apply)
            + read = (known after apply)
          }

        + versioning {
            + enabled = (known after apply)
          }
      }

    # yandex_vpc_network.default will be created
    + resource "yandex_vpc_network" "default" {
        + created_at                = (known after apply)
        + default_security_group_id = (known after apply)
        + folder_id                 = (known after apply)
        + id                        = (known after apply)
        + labels                    = (known after apply)
        + name                      = "net-prod"
        + subnet_ids                = (known after apply)
      }

    # yandex_vpc_subnet.default will be created
    + resource "yandex_vpc_subnet" "default" {
        + created_at     = (known after apply)
        + folder_id      = (known after apply)
        + id             = (known after apply)
        + labels         = (known after apply)
        + name           = "subnet-prod"
        + network_id     = (known after apply)
        + v4_cidr_blocks = [
            + "10.100.1.0/24",
          ]
        + v6_cidr_blocks = (known after apply)
        + zone           = "ru-central1-a"
      }

  Plan: 8 to add, 0 to change, 0 to destroy.
  ```
______

- выполнил `terraform apply`, используя файл `instances.tf`, в котором использовался `count`:
  ```bash
  $ terraform apply
  Apply complete! Resources: 8 added, 0 changed, 0 destroyed.

  Outputs:

  access_key = <sensitive>
  hostnames = [
    "prod-node01.netology.cloud",
    "prod-node02.netology.cloud",
  ]
  instance_external_ip = [
    "51.250.74.109",
    "51.250.66.198",
  ]
  instance_internal_ip = [
    "10.100.1.17",
    "10.100.1.32",
  ]
  static_key = <sensitive>
  ```

- добавим блок `backend "s3" {}` в файл `provider.tf`:
  ```tf
  terraform {
    backend "s3" {
      endpoint = "storage.yandexcloud.net"
      bucket   = "tf-state-bucket-aleksandrmanzin"
      region   = "ru-central1"
      key      = ""IaC/terraform/terraform.tfstate""

      skip_region_validation      = true
      skip_credentials_validation = true
    }

    required_providers {
      yandex = {
        source = "yandex-cloud/yandex"
      }
    }
    required_version = ">= 0.13"
  }

  provider "yandex" {
    token     = var.yc_token
    cloud_id  = var.yc_cloud_id
    folder_id = var.yc_folder_id
    zone      = var.yc_zone
  }
  ```

- создадим файл `backend.tfvars`, в который запишем две переменные `access_key` и `secret_key`, чтоб не использовать каждый раз `export`;

- выполним `terraform init` для добавления файла `terraform.tfstate` в наше облако:
  ```bash
  $ terraform init -backend-config backend.tfvars
  Terraform has been successfully initialized!
  ```
  ![Infrastructure](screenshots/infrastructure.png)
  ![VM](screenshots/virtualmachines.png)
  ![bucket](screenshots/tf-state-bucket.png)
  ![tfstate](screenshots/tfstate.png)
