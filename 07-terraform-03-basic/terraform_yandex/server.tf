# // получение последнего образа centos 8
# data "yandex_compute_image" "image" {
#   family = var.yc_image_family
# }
#
# // создание инстанса с использованием for_each
# resource "yandex_compute_instance" "server" {
#   for_each = {
#     web = { "type" : "network-hdd", "public_ip" : true, "cores" : 2, "memory" : 4, "size" : 20 },
#     db  = { "type" : "network-nvme", "public_ip" : false, "cores" : 4, "memory" : 16, "size" : 100 }
#   }
#
#   name                      = "${each.key}-${terraform.workspace}-${var.name}"
#   zone                      = var.yc_zone
#   hostname                  = "${each.key}-${terraform.workspace}-${var.name}.${var.hostname}"
#   folder_id                 = var.yc_folder_id
#   allow_stopping_for_update = true
#
#   resources {
#     cores  = each.value["cores"]
#     memory = each.value["memory"]
#   }
#
#   boot_disk {
#     initialize_params {
#       name     = "${terraform.workspace}-${each.key}"
#       image_id = data.yandex_compute_image.image.id
#       type     = each.value["type"]
#       size     = each.value["size"]
#     }
#   }
#
#   network_interface {
#     subnet_id = yandex_vpc_subnet.default.id
#     nat       = true
#   }
#
#   metadata = {
#     ssh-keys = "var.username:${file("~/.ssh/id_rsa.pub")}"
#   }
#
#   lifecycle {
#     create_before_destroy = true
#   }
# }
#
# // Создание бакета с использованием ключа
# resource "yandex_storage_bucket" "aleksandrmanzin-s3-storage" {
#   access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
#   secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
#   bucket     = var.bucket
# }
