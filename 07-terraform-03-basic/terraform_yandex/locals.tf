locals {
  replications = {
    default = 1
    stage   = 1
    prod    = 2
  }

  cores = {
    default = 2
    stage   = 2
    prod    = 2
  }

  memory = {
    default = 2
    stage   = 2
    prod    = 4
  }

  boot_disk_type = {
    default = "network-hdd"
    stage   = "network-hdd"
    prod    = "network-nvme"
  }

  boot_disk_size = {
    default = 20
    stage   = 20
    prod    = 40
  }

  vpc_subnet = {
    default = ["10.10.1.0/24"]
    stage   = ["10.10.1.0/24"]
    prod    = ["10.100.1.0/24"]
  }
}
