# Hostnames
output "hostnames" {
  value = yandex_compute_instance.instance[*].hostname
}
#External IP
output "instance_external_ip" {
  value = yandex_compute_instance.instance[*].network_interface.0.nat_ip_address
}

#Internal IP
output "instance_internal_ip" {
  value = yandex_compute_instance.instance[*].network_interface.0.ip_address
}

output "access_key" {
  value     = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  sensitive = true
}

output "static_key" {
  value     = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  sensitive = true
}
