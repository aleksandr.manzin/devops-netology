terraform {
  backend "s3" {
    endpoint = "storage.yandexcloud.net"
    bucket   = "tf-state-bucket-aleksandrmanzin"
    region   = "ru-central1"
    key      = "IaC/terraform/terraform.tfstate"

    skip_region_validation      = true
    skip_credentials_validation = true
  }

  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = var.yc_token
  cloud_id  = var.yc_cloud_id
  folder_id = var.yc_folder_id
  zone      = var.yc_zone
}
