variable "yc_token" {
  type        = string
  description = "Yandex Cloud API key"
}

variable "yc_cloud_id" {
  type        = string
  description = "Yandex Cloud ID"
  default     = "b1gjp5l6o4ehob5b3eak"
}

variable "yc_folder_id" {
  type        = string
  description = "Yandex Cloud Folder ID"
  default     = "b1gl0fkdpeiilq9hi2b3"
}

variable "yc_zone" {
  type        = string
  description = "Yandex Cloud Zone (Region)"
  default     = "ru-central1-a"
}

variable "yc_image_family" {
  description = "OS Family"
  default     = "centos-8"
}

variable "username" {
  default = "centos"
}

variable "count_format" {
  default = "%02d"
}

variable "name" {
  default = "node"
}

variable "hostname" {
  default = "netology.cloud"
}

variable "bucket" {
  default = "tf-state-bucket-aleksandrmanzin"
}
