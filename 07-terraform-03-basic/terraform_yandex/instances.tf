// получение последнего образа centos 8
data "yandex_compute_image" "image" {
  family = var.yc_image_family
}

// создание инстанса с использованием count
resource "yandex_compute_instance" "instance" {
  count                     = local.replications[terraform.workspace]
  name                      = "${terraform.workspace}-${var.name}${format(var.count_format, count.index + 1)}"
  zone                      = var.yc_zone
  hostname                  = "${terraform.workspace}-${var.name}${format(var.count_format, count.index + 1)}.${var.hostname}"
  folder_id                 = var.yc_folder_id
  allow_stopping_for_update = true

  resources {
    cores  = local.cores[terraform.workspace]
    memory = local.memory[terraform.workspace]
  }

  boot_disk {
    initialize_params {
      name     = "${terraform.workspace}-${format(var.count_format, count.index + 1)}"
      image_id = data.yandex_compute_image.image.id
      type     = local.boot_disk_type[terraform.workspace]
      size     = local.boot_disk_size[terraform.workspace]
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.default.id
    nat       = true
  }

  metadata = {
    ssh-keys = "var.username:${file("~/.ssh/id_rsa.pub")}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

// Создание бакета с использованием ключа
resource "yandex_storage_bucket" "aleksandrmanzin-s3-storage" {
  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  bucket     = var.bucket
}
