# Домашнее задание к занятию "7.2. Облачные провайдеры и синтаксис Terraform."

## Задача 1 Регистрация в ЯО и знакомство с основами

> 1. Подробная инструкция на русском языке содержится здесь.
> 1. Обратите внимание на период бесплатного использования после регистрации аккаунта.
> 1. Используйте раздел "Подготовьте облако к работе" для регистрации аккаунта. Далее раздел "Настройте провайдер" для подготовки базового терраформ конфига.
> 1. Воспользуйтесь инструкцией на сайте терраформа, что бы не указывать авторизационный токен в коде, а терраформ провайдер брал его из переменных окружений.

- подготовили облако к работе:
  - получили токен авторизации;
  - создали облако;
  - создали каталог;
  - выбрали зону доступности.
- создадим файл `.terraformrc` в домашней директории, который будет указывать на источник, из которого будет устанавливаться провайдер:
  ```bash
  provider_installation {
    network_mirror {
      url = "https://terraform-mirror.yandexcloud.net/"
      include = ["registry.terraform.io/*/*"]
    }
    direct {
      exclude = ["registry.terraform.io/*/*"]
    }
  }
  ```
- сделаем отдельную директорию `terraform_yandex`, в которой у нас будет наша инфраструктура `terraform`;
- сделаем вывод команды `yc config list` в файл `private.auto.tfvars`:
    ```bash
    $ yc config list > private.auto.tfvars
    ```
- теперь в нашем файле имеются все необходимые данные. Откроем файлик и отредактируем его, как нам нужно:
    ```tf
    YC_TOKEN     = "AQ**AAAIBNT**TuwYa**KKcv***X*CBD*jiMI"
    YC_CLOUD_ID  = "b1gjp5l6o4ehob5b3eak"
    YC_FOLDER_ID = "b1gl0fkdpeiilq9hi2b3"
    YC_ZONE      = "ru-central1-a"
    ```
- создадим файл `variables.tf`:
    ```tf
    variable "YC_TOKEN" {
      type        = string
      description = "Yandex Cloud API key"
    }

    variable "YC_CLOUD_ID" {
      type        = string
      description = "Yandex Cloud ID"
    }

    variable "YC_FOLDER_ID" {
      type        = string
      description = "Yandex Cloud Folder ID"
    }

    variable "YC_ZONE" {
      type        = string
      description = "Yandex Cloud Zone (Region)"
    }

    variable "yandex_cloud_image_family" {
      default = "centos-8"
    }
    ```
- создадим файл `provider.tf`:
  ```tf
  terraform {
    required_providers {
      yandex = {
        source = "yandex-cloud/yandex"
      }
    }
    required_version = ">= 0.13"
  }

  provider "yandex" {
    token     = var.YC_TOKEN
    cloud_id  = var.YC_CLOUD_ID
    folder_id = var.YC_FOLDER_ID
    zone      = var.YC_ZONE
  }
  ```
## Задача 2. Создание `yandex_compute_instance` через терраформ

> 1. В каталоге terraform вашего основного репозитория, который был создан в начале курсе, создайте файл main.tf и versions.tf.
> 1. Зарегистрируйте провайдер
  - для aws. В файл main.tf добавьте блок provider, а в versions.tf блок terraform с вложенным блоком required_providers. Укажите любой выбранный вами регион внутри блока provider.
  - либо для yandex.cloud. Подробную инструкцию можно найти здесь.
> 1. Внимание! В гит репозиторий нельзя пушить ваши личные ключи доступа к аккаунту. Поэтому в предыдущем задании мы указывали их в виде переменных окружения.
> 1. В файле main.tf воспользуйтесь блоком data "aws_ami для поиска ami образа последнего Ubuntu.
> 1. В файле main.tf создайте рессурс
    - либо ec2 instance. Постарайтесь указать как можно больше параметров для его определения. Минимальный набор параметров указан в первом блоке Example Usage, но желательно, указать большее количество параметров.
    - либо yandex_compute_image.
> 1. Также в случае использования aws:
    - Добавьте data-блоки aws_caller_identity и aws_region.
    - В файл outputs.tf поместить блоки output с данными об используемых в данный момент:
      - AWS account ID,
      - AWS user ID,
      - AWS регион, который используется в данный момент,
      - Приватный IP ec2 инстансы,
      - Идентификатор подсети в которой создан инстанс.
> 1. Если вы выполнили первый пункт, то добейтесь того, что бы команда terraform plan выполнялась без ошибок.

> В качестве результата задания предоставьте:
> 1. Ответ на вопрос: при помощи какого инструмента (из разобранных на прошлом занятии) можно создать свой образ ami?
> 1. Ссылку на репозиторий с исходной конфигурацией терраформа.

- файлы `versions.tf` и `main.tf` я объединил в предыдущей задаче в один, и назвал `provider.tf`;
- также были созданы файлы:
  - `network.tf`:
    ```tf
    resource "yandex_vpc_network" "default" {
      name = "net"
    }

    resource "yandex_vpc_subnet" "default" {
      name           = "subnet"
      zone           = var.YC_ZONE
      network_id     = yandex_vpc_network.default.id
      v4_cidr_blocks = ["10.100.1.0/24"]
    }
    ```
  - `node01.tf`:
    ```tf
    data "yandex_compute_image" "image" {
      family = var.yandex_cloud_image_family
    }

    resource "yandex_compute_instance" "node01" {
      name                      = "node01"
      zone                      = var.YC_ZONE
      hostname                  = "node01.netology.cloud"
      allow_stopping_for_update = true

      resources {
        cores  = 2
        memory = 4
      }

      boot_disk {
        initialize_params {
          image_id = data.yandex_compute_image.image.id
          type     = "network-nvme"
          size     = "30"
        }
      }

      network_interface {
        subnet_id = yandex_vpc_subnet.default.id
        nat       = true
      }

      metadata = {
        ssh-keys = "centos:${file("~/.ssh/id_rsa.pub")}"
      }
    }

    ```

  - `output.tf`:
    ```tf
    #External IP
    output "instance_external_ip" {
      value = yandex_compute_instance.node01.network_interface.0.nat_ip_address
    }

    #Internal IP
    output "instance_internal_ip" {
      value = yandex_compute_instance.node01.network_interface.0.ip_address
    }
    ```
- сделаем `terraform init`:
  ```bash

  Initializing the backend...

  Initializing provider plugins...
  - Reusing previous version of yandex-cloud/yandex from the dependency lock file
  - Using previously-installed yandex-cloud/yandex v0.77.0

  Terraform has been successfully initialized!

  You may now begin working with Terraform. Try running 'terraform plan' to see
  any changes that are required for your infrastructure. All Terraform commands
  should now work.

  If you ever set or change modules or backend configuration for Terraform,
  rerun this command to reinitialize your working directory. If you forget, other
  commands will detect it and remind you to do so if necessary.
  ```

- проверим правильность нашей конфигурации:
  ```bash
  $ terraform validate
  Success! The configuration is valid.
  ```
- смотрим план поднимаемой инфраструктуры:
  ```bash
  $ terraform plan
  data.yandex_compute_image.image: Reading...
  data.yandex_compute_image.image: Read complete after 4s [id=fd86tafe9jg6c4hd2aqp]

  Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
    + create

  Terraform will perform the following actions:

    # yandex_compute_instance.node01 will be created
    + resource "yandex_compute_instance" "node01" {
        + allow_stopping_for_update = true
        + created_at                = (known after apply)
        + folder_id                 = (known after apply)
        + fqdn                      = (known after apply)
        + hostname                  = "node01.netology.cloud"
        + id                        = (known after apply)
        + metadata                  = {
            + "ssh-keys" = <<-EOT
                  centos:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDRr9DnS4fKgblXq2L7rsKxuZyhy5ANn9AJLZ+EiQ/HuYNq3s2h9HJw1mIbIt4+28FKbl0/jR1VdNIgU7PuSzuDccq7xP9g12v+Wpv9c+GAm2GeU3HqewHN/DdQLRWEmZFta26GCUBo4tbESSX7HCITQxUmFpUVOyFWFB09orA+dtb23Yr7Cg83jczaTO8Jt7/Qy53NaK1VcyjKZjIzAT0Yew8DrW9VRyvquHtJJYdgh8S1SHTQVOp7JK00z52hJbFwXXKZ6V5oIUoeAXxpr0CYoSYIkE8Avk/uH0KIQdBj97HF1KlA3dRU4ECKHKJh2NWy3cCKHjmfLxGko/Ccx2Dew8LQBUWqWoeL9PE152iYDqzQXST7ih3C3bPhy4OZxHjWNXK0gsbPFyWirFjEZ9sOJtMK/4WEE8Z3BdyMkF3WGAlAdom2PtlTCRqn4rDogWcNO/TxBxXMv1tUrMPZGTCRU0XMZ+DiTGQKkL452Ofm6Z0IP+rRjcFDf1vzBB4lurs= aleksandr@manzin
              EOT
          }
        + name                      = "node01"
        + network_acceleration_type = "standard"
        + platform_id               = "standard-v1"
        + service_account_id        = (known after apply)
        + status                    = (known after apply)
        + zone                      = "ru-central1-a"

        + boot_disk {
            + auto_delete = true
            + device_name = (known after apply)
            + disk_id     = (known after apply)
            + mode        = (known after apply)

            + initialize_params {
                + block_size  = (known after apply)
                + description = (known after apply)
                + image_id    = "fd86tafe9jg6c4hd2aqp"
                + name        = (known after apply)
                + size        = 30
                + snapshot_id = (known after apply)
                + type        = "network-nvme"
              }
          }

        + network_interface {
            + index              = (known after apply)
            + ip_address         = (known after apply)
            + ipv4               = true
            + ipv6               = (known after apply)
            + ipv6_address       = (known after apply)
            + mac_address        = (known after apply)
            + nat                = true
            + nat_ip_address     = (known after apply)
            + nat_ip_version     = (known after apply)
            + security_group_ids = (known after apply)
            + subnet_id          = (known after apply)
          }

        + placement_policy {
            + host_affinity_rules = (known after apply)
            + placement_group_id  = (known after apply)
          }

        + resources {
            + core_fraction = 100
            + cores         = 2
            + memory        = 4
          }

        + scheduling_policy {
            + preemptible = (known after apply)
          }
      }

    # yandex_vpc_network.default will be created
    + resource "yandex_vpc_network" "default" {
        + created_at                = (known after apply)
        + default_security_group_id = (known after apply)
        + folder_id                 = (known after apply)
        + id                        = (known after apply)
        + labels                    = (known after apply)
        + name                      = "net"
        + subnet_ids                = (known after apply)
      }

    # yandex_vpc_subnet.default will be created
    + resource "yandex_vpc_subnet" "default" {
        + created_at     = (known after apply)
        + folder_id      = (known after apply)
        + id             = (known after apply)
        + labels         = (known after apply)
        + name           = "subnet"
        + network_id     = (known after apply)
        + v4_cidr_blocks = [
            + "10.100.1.0/24",
          ]
        + v6_cidr_blocks = (known after apply)
        + zone           = "ru-central1-a"
      }

  Plan: 3 to add, 0 to change, 0 to destroy.

  Changes to Outputs:
    + instance_external_ip = (known after apply)
    + instance_internal_ip = (known after apply)

  ```

- Результаты задания:
  1. можно собирать образы при помощи `Packer`;
  2. [ссылка на конфигурацию](https://gitlab.com/aleksandr.manzin/devops-netology/-/tree/main/07-terraform-02-syntax/terraform_yandex)
