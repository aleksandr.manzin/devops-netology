variable "YC_TOKEN" {
  type        = string
  description = "Yandex Cloud API key"
}

variable "YC_CLOUD_ID" {
  type        = string
  description = "Yandex Cloud ID"
}

variable "YC_FOLDER_ID" {
  type        = string
  description = "Yandex Cloud Folder ID"
}

variable "YC_ZONE" {
  type        = string
  description = "Yandex Cloud Zone (Region)"
}

variable "yandex_cloud_image_family" {
  default = "centos-8"
}
