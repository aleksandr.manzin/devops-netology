resource "yandex_vpc_network" "default" {
  name = "net"
}

resource "yandex_vpc_subnet" "default" {
  name           = "subnet"
  zone           = var.YC_ZONE
  network_id     = yandex_vpc_network.default.id
  v4_cidr_blocks = ["10.100.1.0/24"]
}
