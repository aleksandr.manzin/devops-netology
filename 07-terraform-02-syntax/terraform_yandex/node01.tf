data "yandex_compute_image" "image" {
  family = var.yandex_cloud_image_family
}

resource "yandex_compute_instance" "node01" {
  name                      = "node01"
  zone                      = var.YC_ZONE
  hostname                  = "node01.netology.cloud"
  allow_stopping_for_update = true

  resources {
    cores  = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.image.id
      type     = "network-nvme"
      size     = "30"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.default.id
    nat       = true
  }

  metadata = {
    ssh-keys = "centos:${file("~/.ssh/id_rsa.pub")}"
  }
}
