#External IP
output "instance_external_ip" {
  value = yandex_compute_instance.node01.network_interface.0.nat_ip_address
}

#Internal IP
output "instance_internal_ip" {
  value = yandex_compute_instance.node01.network_interface.0.ip_address
}
