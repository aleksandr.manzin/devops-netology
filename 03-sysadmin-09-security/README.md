# Домашнее задание к занятию "3.9. Элементы безопасности информационных систем"

1. > Установите Bitwarden плагин для браузера. Зарегестрируйтесь и сохраните несколько паролей.

    Установил `Bitwarden` и сохранил несколько паролей. С этой утилитой знаком достаточно давно:

    ![Bitwarden](screenshots/Bitwarden.PNG "Bitwarden")

    Помимо этого протестировал `gopass`:
    * для начала скачаем и установим пакет `gopass`:
        ```bash
        $ wget -O gopass.deb https://github.com/gopasspw/gopass/releases/download/v1.13.0/gopass_1.13.0_linux_amd64.deb
        $ sudo dpkg -i gopass.deb
        dpkg: dependency problems prevent configuration of gopass:
         gopass depends on gnupg2; however:																																				
          Package gnupg2 is not installed.                 			# менеджер пакетов сообщает нам, что для gopass требуется GnuPG
        $ sudo apt install -y gnupg2                         		# устанавливаем GnuPG
        $ sudo dpkg -i gopass.deb                           		# возвращаемся к установке пакета gopass.deb
        ```
    * теперь перейдем к настройке:
        ```bash
        $ echo "source <(gopass completion bash)" >>.bashrc  		# добавляем возможность автодополнения команд gopass в shell
        $ gopass setup                                     		    # производим настройку gopass, инициализацию, вводим имя, почту, генерируем парольную фразу, подключаем удаленный репозиторий
        ```
    * Создаём несколько директорий и «примаунчиваем» их. Записываем пароли и смотрим, что у нас получилось:
        ```bash
        $ gopass list
        gopass
        ├── personal (/home/vagrant/gopass_store2)
        │   └── websites/
        │       └── mail.google.com/
        │           └── gopass_testing
        └── work (/home/vagrant/gopass_store)
            └── websites/
                └── mail.work.ru/
                    └── support
        ```

    Очень удобная утилита, которая показывает в древовидном, удобном формате всю структуру паролей. Обязательно возьму на вооружение и буду использовать!

2. > Установите Google authenticator на мобильный телефон. Настройте вход в Bitwarden акаунт через Google authenticator OTP.
   
	* Добавил Bitwarden в свой `GoogleAuthenticator`:
  
        ![GoogleAuthenticator](screenshots/GoogleAuthenticatorEnable.PNG "GoogleAuthenticator")

3. > Установите apache2, сгенерируйте самоподписанный сертификат, настройте тестовый сайт для работы по HTTPS.

    * установим `apache2` и проверяем, запустилась ли служба:
        ```bash
        $ sudo apt install apache2
        $ systemctl status apache2
        ```
    * настроим IP через `netplan`:
		```bash
		$ sudo touch /etc/netplan/02-eth1.yaml
		$ sudo nano /etc/netplan/02-eth1.yaml
		  network:
			version: 2
			ethernets:
			  eth1:
				dhcp4: no
				addresses: [192.168.100.222/24]
				routes:
				  - to: default
					via: 192.168.100.1
					on-link: yes
		$ sudo netplan apply
		$ ip -4 -c -br a
		lo               UNKNOWN        127.0.0.1/8
		eth0             UP             10.0.2.15/24
		eth1             UP             192.168.100.222/24
		```
	 
    * разрешим трафик на 80 и 443 портах, для этого разрешим `Apache Full` на фаерволе `ufw`:
        ```bash
        $ sudo ufw allow 'Apache Full'
        $ sudo ufw status
        Status: active

        To                         Action      From
        --                         ------      ----
        OpenSSH                    ALLOW       Anywhere
        Apache Full                ALLOW       Anywhere
        OpenSSH (v6)               ALLOW       Anywhere (v6)
        Apache Full (v6)           ALLOW       Anywhere (v6)
        ```

    * открываем браузер, вбиваем адрес `192.168.100.222`, и видим, что открывается стандартная страница:
    
	    ![Apache](screenshots/Apache.PNG "Apache")

    * активируем модуль `mod_ssl`, который предоставляет поддержку SSL и после этого перезапустим служду `Apache`:
        ```bash
        $ sudo a2enmod ssl
        $ sudo systemctl restart apache2
        ```

    * создадим сертификат с помощью `openssl`:
        ```bash
        $ sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt
        ```
        Описание параметров:
        * `req -x509` - указывает, что мы хотим использовать управление запросами на подписание сертификатов `X.509` (инфраструктура открытых ключей, используемая стандартами SSL и TLS для управления ключами и сертификатами);
        * `-nodes` - предписывает openssl пропустить опцию защиты сертификата кодовой фразой;
        * `-days 365` - период действия сертификата;
        * `-newkey rsa:2048` - указывает, что хотим сгенерировать новый сертификат и новый ключ (ключ длиной 2048 бит) одновременно;
        * `-keyout` - путь до генерируемого файла закрытого ключа;
        * `-out` - путь до создаваемоего сертификата.
		
        Заполняем все необходимые данные, о которых нас спрашивают.	
		
    * создаём файл `mysite.conf` по пути `/etc/apache2/sites-available/` и вносим туда следующие данные:
        ```
        <VirtualHost *:443>
           ServerName 192.168.100.222
           DocumentRoot /var/www/mysite

           SSLEngine on
           SSLCertificateFile /etc/ssl/certs/apache-selfsigned.crt
           SSLCertificateKeyFile /etc/ssl/private/apache-selfsigned.key
        </VirtualHost>
		
        <VirtualHost *:80>
           ServerName 192.168.100.222
           Redirect / https://192.168.100.222/
        </VirtualHost>
        ```
        Последний блок для перенаправления с `http` на `https`. Т.о. при обращении к `http://192.168.100.222` будет открываться `https://192.168.100.222`.
	
    * также создаём отдельную директорию для сайта и создаём страничку `index.html` с простым заголовком:
        ```bash
        $ sudo mkdir /var/www/mysite
        $ sudo nano /var/www/mysite/index.html
        <!DOCTYPE HTML>
        <meta charset="utf-8">
        <title> MySite </title>
        <h1> It works! </h1>
        ```
	
    * активируем новый файл конфигурации `mysite.conf` и деактивируем старый файл кофигурации `000-default.conf`:
        ```bash
        $ sudo a2ensite mysite.conf
        $ sudo a2dissite 000-default.conf
        ```
	
    * проверим конфигурацию на ошибки и, если увидим сообщение `Syntax OK`, то перезагрузим службу `Apache`:
        ```bash
        $ sudo apache2ctl configtest
        Syntax OK
        $ sudo systemctl reload apache2
        ```
	
    * теперь можно заходить на созданную нами страничку `https://192.168.100.222`:
	  
        ![HTTPS](screenshots/cert.PNG "443")
	
4. > Проверьте на TLS уязвимости произвольный сайт в интернете
	
	Устанавливаем и запускаем `testssl`:
    
    ```bash
    $ git clone --depth 1 https://github.com/drwetter/testssl.sh.git
    $ cd testssl.sh
    $ ./testssl.sh -U --sneaky https://www.vk.com/
    Further IP addresses:   87.240.190.78 87.240.139.194 87.240.190.67 87.240.190.72 87.240.137.158
    rDNS (93.186.225.208):  --
    Service detected:       HTTP


    Testing vulnerabilities

    Heartbleed (CVE-2014-0160)                not vulnerable (OK), no heartbeat extension
    CCS (CVE-2014-0224)                       not vulnerable (OK)
    Ticketbleed (CVE-2016-9244), experiment.  not vulnerable (OK), no session ticket extension
    ROBOT                                     not vulnerable (OK)
    Secure Renegotiation (RFC 5746)           supported (OK)
    Secure Client-Initiated Renegotiation     not vulnerable (OK)
    CRIME, TLS (CVE-2012-4929)                not vulnerable (OK)
    BREACH (CVE-2013-3587)                    no gzip/deflate/compress/br HTTP compression (OK)  - only supplied "/" tested
    POODLE, SSL (CVE-2014-3566)               not vulnerable (OK)
    TLS_FALLBACK_SCSV (RFC 7507)              Downgrade attack prevention supported (OK)
    SWEET32 (CVE-2016-2183, CVE-2016-6329)    not vulnerable (OK)
    FREAK (CVE-2015-0204)                     not vulnerable (OK)
    DROWN (CVE-2016-0800, CVE-2016-0703)      not vulnerable on this host and port (OK)
											   make sure you don't use this certificate elsewhere with SSLv2 enabled services
											   https://censys.io/ipv4?q=6006EAF70015B37CADF5A47EDF169BDC19BEBF078886CDE35BA87D9B89E08894 could help you to find out
    LOGJAM (CVE-2015-4000), experimental      not vulnerable (OK): no DH EXPORT ciphers, no common prime detected
    BEAST (CVE-2011-3389)                     TLS1: ECDHE-ECDSA-AES128-SHA ECDHE-RSA-AES128-SHA DHE-RSA-AES128-SHA
													 AES128-SHA
		  									   VULNERABLE -- but also supports higher protocols  TLSv1.1 TLSv1.2 (likely mitigated)
    LUCKY13 (CVE-2013-0169), experimental     potentially VULNERABLE, uses cipher block chaining (CBC) ciphers with TLS. Check patches
    Winshock (CVE-2014-6321), experimental    not vulnerable (OK)
    RC4 (CVE-2013-2566, CVE-2015-2808)        no RC4 ciphers detected (OK)
    ```
	
5. > Установите на Ubuntu ssh сервер, сгенерируйте новый приватный ключ. Скопируйте свой публичный ключ на другой сервер. Подключитесь к серверу по SSH-ключу
	
    Для теста я создал ещё одного пользователя на виртуальной машине, назвал его `user`, как у меня на Windows.
    
	* Устанавливаем ssh сервер (в нашем случае уже установлен):
	    ```bash
	    $ sudo apt install openssh-server -y 
	    ```
	* генерируем ключи, вводим парольную фразу:
	    ```bash
	    $ ssh-keygen
	    ```
	* копируем публичный ключ другому пользователю (vagrant) для теста внутри виртуальной машины:
	    ```bash
	    $ ssh-copy-id vagrant@vagrant
	    ```
	* пытаемся подключиться, используя закрытый ключ:
	    ```bash
	    $ ssh vagrant@vagrant
	    Enter passphrase for key '/home/user/.ssh/id_rsa':
	    Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.4.0-91-generic x86_64)
	    ```
	    Видим строку приветствия пользователя vagrant.
	
	    Решил сгенерировать ключи в Windows10 и скопировать полученный открытый ключ на виртуальную машину. Пробуем подключиться:
	    ```
	    PS E:\> ssh 192.168.1.55
	    Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.4.0-91-generic x86_64)
	    ```
	    Попадаем на нашу виртуальную машину. Все отлично. Можно открытый ключ скопировать другому пользователю, например, vagrant и подключиться из windows, используя `vagrant@192.168.1.55`

6. > Переименуйте файлы ключей из задания 5. Настройте файл конфигурации SSH клиента, так чтобы вход на удаленный сервер осуществлялся по имени сервера.
	
    Для начала отключим возможность подключения через пароль, для этого в файле `/etc/ssh/sshd-config` поставим значение `NO` для `PasswordAuthentication`. Теперь перейдем к тестам:
    
	* Тестируем на Windows10:
        * переименуем оба ключа в Windows10:
		    * id_rsa в private_key
			* id_rsa.pub в public_key
		
		* попробуем подключиться, не указывая нужный ключ:
		```
		PS E:\> ssh 192.168.1.55
		user@vagrant: Permission denied (publickey).
		```
		Видим, что подключиться невозможно. Пробуем подключиться, указав ключ `private_key`:
		```
		PS E:\> ssh 192.168.1.55 -i "C:\Users\user\.ssh\private_key"
		Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.4.0-91-generic x86_64)
		```
		Как видим, все получилось. Теперь создадим `config`, в котором укажем наш ключ и hostname виртуальной машины:
		```
		Host vagrant
			HostName 192.168.1.55
			IdentityFile "C:\Users\user\.ssh\private_key"
		```
		Проверяем:
		```
		PS E:\> ssh vagrant
		Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.4.0-91-generic x86_64)
		```
		Теперь можно подключаться без проблем к виртуальной машине, используя имя и без указания ключа.
	
	* Тестируем на Linux:
		* переименуем наши ключи:
		```bash
		$ mv id_rsa private_key && mv id_rsa.pub public_key
		```
		
		* попробуем подключиться:
		```bash
		$ ssh vagrant@vagrant
		vagrant@vagrant: Permission denied (publickey).
		```
		
		Неудачно! Пробуем подключиться с указанием ключа:
		```bash
		$ ssh vagrant@vagrant -i ~/.ssh/private_key
		Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.4.0-91-generic x86_64)
		```
		Видим приветственную строчку.  Теперь создадим `config` в директории `~/.ssh/`, в котором укажем наш ключ, hostname виртуальной машины и определим пользователя для соединения ssh. Для удобства поменяем `hostname`:
		```bash
		$ sudo hostname ubuntu
		$ nano ~/.ssh/config 
		Host ubuntu
			HostName 192.168.1.55
			User vagrant
			IdentityFile ~/.ssh/private_key
		```
		Проверяем:		
		```bash
		$ ssh vagrant@vagrant
		Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.4.0-91-generic x86_64)
		```
		Отлично, теперь можно подключаться, не указывая закрытый ключ и по хоста.

7. > Соберите дамп трафика утилитой tcpdump в формате pcap, 100 пакетов. Откройте файл pcap в Wireshark

	* Запустим утилиту `tcpdump` на сбор 100 пакетов. Для теста воспользуемся пингом какого-либо ресурса, например, `netology.ru`. В одном терминале запускаем `ping netology.ru`, а в другом собираем трафик:
	    ```bash
	    $ sudo tcpdump -v -tttt -i eth0 -c 100 -n -w icmp.pcap 
	    tcpdump: listening on eth0, link-type EN10MB (Ethernet), capture size 262144 bytes
	    100 packets captured
	    100 packets received by filter
	    0 packets dropped by kernel
	    ```
	* трафик собрали. Скачаем файл `icmp.pcap` на windows и откроем через wireshark. Можем наблюдать мак-адрес источника и получателя, IP-адреса источника и получателя, что используется протокол ICMP:
		* ICMP Request
        
	        ![wireshark](screenshots/wireshark_icmprequest.png "ICMP Request")
		
		* ICMP Reply
        
	        ![wireshark](screenshots/wireshark_icmpreply.png "ICMP Reply")
	
8. > Просканируйте хост scanme.nmap.org. Какие сервисы запущены?

	* Установим `nmap` и  соберем подробную информацию о хосте `scanme.nmap.org`. Для этого используем ключ `-A` для определения версии ОС, сканирования с использованием скриптом и трассировки, ключ `-T4` для более быстрого выполнения:
	    ```bash
	    $ sudo apt install -y nmap
	    $ sudo nmap -A -T4 scanme.nmap.org
	    PORT      STATE    SERVICE
	    22/tcp    open     ssh
	    80/tcp    open     http
	    135/tcp   filtered msrpc
	    139/tcp   filtered netbios-ssn
	    445/tcp   filtered microsoft-ds
	    593/tcp   filtered http-rpc-epmap
	    4444/tcp  filtered krb524
	    9929/tcp  open     nping-echo
	    31337/tcp open     Elite
	    ```
        
        Открытые порты:
        
        * `22/tcp` - `ssh`;
	    * `80/tcp` - `http`;
	    * `9929/tcp` - `nping-echo` - инструмент генерации сетевых пакетов / ping утилита;
	    * `31337/tcp` - `Elite`- TCP-порт, использовавшийся известной троянской программой `Back Orifice`.
	

9. > Установите и настройте фаервол ufw на web-сервер из задания 3. Откройте доступ снаружи только к портам 22,80,443

	* Данные действия я проделывал в 3 задании. Поэтому приведу список открытых портов:
	    ```bash
	    $ sudo ufw status
	    Status: active
    
	    To                         Action      From
	    --                         ------      ----
	    OpenSSH                    ALLOW       Anywhere
	    Apache Full                ALLOW       Anywhere
	    OpenSSH (v6)               ALLOW       Anywhere (v6)
	    Apache Full (v6)           ALLOW       Anywhere (v6)
	    ```
	    Из вывода видим, что разрешен доступ по ssh (22 порт) и Apache Full (80 и 443 порты):
	    ```bash
	    $ sudo ufw app info "Apache Full"
	    Profile: Apache Full
	    Title: Web Server (HTTP,HTTPS)
	    Description: Apache v2 is the next generation of the omnipresent Apache web
	    server.
    
	    Ports:
	      80,443/tcp
    	  
    	$ sudo ufw app info OpenSSH
    	Profile: OpenSSH
	    Title: Secure shell server, an rshd replacement
	    Description: OpenSSH is a free implementation of the Secure Shell protocol.
    
	    Port:
	      22/tcp
	    ```  
	
	    Для открытия данных портов можно использовать несколько вариантов, например `sudo ufw allow OpenSSH` или `sudo ufw allow 22`, что соответствует разрешению входящих и исходящих соединений к 22 порту.