# Домашнее задание к занятию "08.03 Работа с Roles"

## Подготовка к выполнению
> 1. Создайте два пустых публичных репозитория в любом своём проекте: elastic-role и kibana-role.

  ![](screenshots/roles.png "Elastic & Kibana roles")
> 2. Скачайте [role](./roles/) из репозитория с домашним заданием и перенесите его в свой репозиторий elastic-role.

  ![](screenshots/elastic-role.png "Elastic role")
> 3. Скачайте дистрибутив [java](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html) и положите его в директорию `playbook/files/`.

  - скачан архив `jdk-11.0.16_linux-x64_bin.tar.gz`

> 4. Установите molecule: `pip3 install molecule`

  ```bash
  $ molecule --version
  molecule 4.0.1 using python 3.10
      ansible:2.13.4
      delegated:4.0.1 from molecule
      docker:2.0.0 from molecule_docker requiring collections: community.docker>=3.0.0-a2

  ```
> 5. Добавьте публичную часть своего ключа к своему профилю в github.

  - публичная часть ключа добавлена

## Основная часть

> Наша основная цель - разбить наш playbook на отдельные roles. Задача: сделать roles для elastic, kibana и написать playbook для использования этих ролей. Ожидаемый результат: существуют два ваших репозитория с roles и один репозиторий с playbook.


> 1. Создать в старой версии playbook файл `requirements.yml` и заполнить его следующим содержимым:
>    ```yaml
>    ---
>      - src: git@github.com:netology-code/mnt-homeworks-ansible.git
>        scm: git
>        version: "1.0.1"
>        name: java
>    ```
> 2. При помощи `ansible-galaxy` скачать себе эту роль. Запустите  `molecule test`, посмотрите на вывод команды.

  - скачиваем роль:
    ```bash
    $ ansible-galaxy install --role-file requirements.yml --roles-path roles
    ```

  - запускаем `molecule test`
    ```bash
    molecule test
    INFO     default scenario test matrix: dependency, lint, cleanup, destroy, syntax, create, prepare, converge, idempotence, side_effect, verify, cleanup, destroy
    INFO     Performing prerun with role_name_check=0...
    INFO     Set ANSIBLE_LIBRARY=/home/aleksandr/.cache/ansible-compat/38a096/modules:/home/aleksandr/.ansible/plugins/modules:/usr/share/ansible/plugins/modules
    INFO     Set ANSIBLE_COLLECTIONS_PATH=/home/aleksandr/.cache/ansible-compat/38a096/collections:/home/aleksandr/.ansible/collections:/usr/share/ansible/collections
    INFO     Set ANSIBLE_ROLES_PATH=/home/aleksandr/.cache/ansible-compat/38a096/roles:/home/aleksandr/.ansible/roles:/usr/share/ansible/roles:/etc/ansible/roles
    INFO     Using /home/aleksandr/.cache/ansible-compat/38a096/roles/docker.java symlink to current repository in order to enable Ansible to find the role using its expected full name.
    INFO     Running default > dependency
    WARNING  Skipping, missing the requirements file.
    WARNING  Skipping, missing the requirements file.
    INFO     Running default > lint
    INFO     Lint is disabled.
    INFO     Running default > cleanup
    WARNING  Skipping, cleanup playbook not configured.
    INFO     Running default > destroy
    INFO     Sanity checks: 'docker'

    PLAY [Destroy] *****************************************************************

    TASK [Destroy molecule instance(s)] ********************************************
    changed: [localhost] => (item=centos8)
    changed: [localhost] => (item=centos7)
    changed: [localhost] => (item=ubuntu)

    TASK [Wait for instance(s) deletion to complete] *******************************
    ok: [localhost] => (item=centos8)
    ok: [localhost] => (item=centos7)
    ok: [localhost] => (item=ubuntu)

    TASK [Delete docker networks(s)] ***********************************************

    PLAY RECAP *********************************************************************
    localhost                  : ok=2    changed=1    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0

    INFO     Running default > syntax

    playbook: /home/aleksandr/git/devops-netology/08-ansible-03-role/playbook/roles/java/molecule/default/converge.yml
    INFO     Running default > create

    PLAY [Create] ******************************************************************

    TASK [Log into a Docker registry] **********************************************
    skipping: [localhost] => (item=None)
    skipping: [localhost] => (item=None)
    skipping: [localhost] => (item=None)
    skipping: [localhost]

    TASK [Check presence of custom Dockerfiles] ************************************
    ok: [localhost] => (item={'image': 'docker.io/pycontribs/centos:8', 'name': 'centos8', 'pre_build_image': True})
    ok: [localhost] => (item={'image': 'docker.io/pycontribs/centos:7', 'name': 'centos7', 'pre_build_image': True})
    ok: [localhost] => (item={'image': 'docker.io/pycontribs/ubuntu:latest', 'name': 'ubuntu', 'pre_build_image': True})

    TASK [Create Dockerfiles from image names] *************************************
    skipping: [localhost] => (item={'image': 'docker.io/pycontribs/centos:8', 'name': 'centos8', 'pre_build_image': True})
    skipping: [localhost] => (item={'image': 'docker.io/pycontribs/centos:7', 'name': 'centos7', 'pre_build_image': True})
    skipping: [localhost] => (item={'image': 'docker.io/pycontribs/ubuntu:latest', 'name': 'ubuntu', 'pre_build_image': True})

    TASK [Discover local Docker images] ********************************************
    ok: [localhost] => (item={'changed': False, 'skipped': True, 'skip_reason': 'Conditional result was False', 'item': {'image': 'docker.io/pycontribs/centos:8', 'name': 'centos8', 'pre_build_image': True}, 'ansible_loop_var': 'item', 'i': 0, 'ansible_index_var': 'i'})
    ok: [localhost] => (item={'changed': False, 'skipped': True, 'skip_reason': 'Conditional result was False', 'item': {'image': 'docker.io/pycontribs/centos:7', 'name': 'centos7', 'pre_build_image': True}, 'ansible_loop_var': 'item', 'i': 1, 'ansible_index_var': 'i'})
    ok: [localhost] => (item={'changed': False, 'skipped': True, 'skip_reason': 'Conditional result was False', 'item': {'image': 'docker.io/pycontribs/ubuntu:latest', 'name': 'ubuntu', 'pre_build_image': True}, 'ansible_loop_var': 'item', 'i': 2, 'ansible_index_var': 'i'})

    TASK [Build an Ansible compatible image (new)] *********************************
    skipping: [localhost] => (item=molecule_local/docker.io/pycontribs/centos:8)
    skipping: [localhost] => (item=molecule_local/docker.io/pycontribs/centos:7)
    skipping: [localhost] => (item=molecule_local/docker.io/pycontribs/ubuntu:latest)

    TASK [Create docker network(s)] ************************************************

    TASK [Determine the CMD directives] ********************************************
    ok: [localhost] => (item={'image': 'docker.io/pycontribs/centos:8', 'name': 'centos8', 'pre_build_image': True})
    ok: [localhost] => (item={'image': 'docker.io/pycontribs/centos:7', 'name': 'centos7', 'pre_build_image': True})
    ok: [localhost] => (item={'image': 'docker.io/pycontribs/ubuntu:latest', 'name': 'ubuntu', 'pre_build_image': True})

    TASK [Create molecule instance(s)] *********************************************
    changed: [localhost] => (item=centos8)
    changed: [localhost] => (item=centos7)
    changed: [localhost] => (item=ubuntu)

    TASK [Wait for instance(s) creation to complete] *******************************
    changed: [localhost] => (item={'failed': 0, 'started': 1, 'finished': 0, 'ansible_job_id': '948294089011.691058', 'results_file': '/home/aleksandr/.ansible_async/948294089011.691058', 'changed': True, 'item': {'image': 'docker.io/pycontribs/centos:8', 'name': 'centos8', 'pre_build_image': True}, 'ansible_loop_var': 'item'})
    changed: [localhost] => (item={'failed': 0, 'started': 1, 'finished': 0, 'ansible_job_id': '403676420568.691086', 'results_file': '/home/aleksandr/.ansible_async/403676420568.691086', 'changed': True, 'item': {'image': 'docker.io/pycontribs/centos:7', 'name': 'centos7', 'pre_build_image': True}, 'ansible_loop_var': 'item'})
    changed: [localhost] => (item={'failed': 0, 'started': 1, 'finished': 0, 'ansible_job_id': '32376963868.691111', 'results_file': '/home/aleksandr/.ansible_async/32376963868.691111', 'changed': True, 'item': {'image': 'docker.io/pycontribs/ubuntu:latest', 'name': 'ubuntu', 'pre_build_image': True}, 'ansible_loop_var': 'item'})

    PLAY RECAP *********************************************************************
    localhost                  : ok=5    changed=2    unreachable=0    failed=0    skipped=4    rescued=0    ignored=0

    INFO     Running default > prepare
    WARNING  Skipping, prepare playbook not configured.
    INFO     Running default > converge

    PLAY [Converge] ****************************************************************

    TASK [Gathering Facts] *********************************************************
    ok: [centos8]
    ok: [ubuntu]
    ok: [centos7]

    TASK [Include roles] ***********************************************************

    TASK [java : Upload .tar.gz file containing binaries from local storage] *******
    skipping: [centos7]
    skipping: [centos8]
    skipping: [ubuntu]

    TASK [java : Upload .tar.gz file conaining binaries from remote storage] *******
    changed: [centos7]
    changed: [centos8]
    changed: [ubuntu]

    TASK [java : Ensure installation dir exists] ***********************************
    changed: [centos7]
    changed: [ubuntu]
    changed: [centos8]

    TASK [java : Extract java in the installation directory] ***********************
    changed: [centos8]
    changed: [ubuntu]
    changed: [centos7]

    TASK [java : Export environment variables] *************************************
    changed: [centos7]
    changed: [ubuntu]
    changed: [centos8]

    PLAY RECAP *********************************************************************
    centos7                    : ok=5    changed=4    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0
    centos8                    : ok=5    changed=4    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0
    ubuntu                     : ok=5    changed=4    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0

    INFO     Running default > idempotence

    PLAY [Converge] ****************************************************************

    TASK [Gathering Facts] *********************************************************
    ok: [centos8]
    ok: [ubuntu]
    ok: [centos7]

    TASK [Include roles] ***********************************************************

    TASK [java : Upload .tar.gz file containing binaries from local storage] *******
    skipping: [centos7]
    skipping: [centos8]
    skipping: [ubuntu]

    TASK [java : Upload .tar.gz file conaining binaries from remote storage] *******
    ok: [centos7]
    ok: [ubuntu]
    ok: [centos8]

    TASK [java : Ensure installation dir exists] ***********************************
    ok: [centos7]
    ok: [ubuntu]
    ok: [centos8]

    TASK [java : Extract java in the installation directory] ***********************
    skipping: [centos7]
    skipping: [centos8]
    skipping: [ubuntu]

    TASK [java : Export environment variables] *************************************
    ok: [centos7]
    ok: [ubuntu]
    ok: [centos8]

    PLAY RECAP *********************************************************************
    centos7                    : ok=4    changed=0    unreachable=0    failed=0    skipped=2    rescued=0    ignored=0
    centos8                    : ok=4    changed=0    unreachable=0    failed=0    skipped=2    rescued=0    ignored=0
    ubuntu                     : ok=4    changed=0    unreachable=0    failed=0    skipped=2    rescued=0    ignored=0

    INFO     Idempotence completed successfully.
    INFO     Running default > side_effect
    WARNING  Skipping, side effect playbook not configured.
    INFO     Running default > verify
    INFO     Running Ansible Verifier

    PLAY [Verify] ******************************************************************

    TASK [Check Java can running] **************************************************
    ok: [ubuntu]
    ok: [centos8]
    ok: [centos7]

    PLAY RECAP *********************************************************************
    centos7                    : ok=1    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
    centos8                    : ok=1    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
    ubuntu                     : ok=1    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

    INFO     Verifier completed successfully.
    INFO     Running default > cleanup
    WARNING  Skipping, cleanup playbook not configured.
    INFO     Running default > destroy

    PLAY [Destroy] *****************************************************************

    TASK [Destroy molecule instance(s)] ********************************************
    changed: [localhost] => (item=centos8)
    changed: [localhost] => (item=centos7)
    changed: [localhost] => (item=ubuntu)

    TASK [Wait for instance(s) deletion to complete] *******************************
    changed: [localhost] => (item=centos8)
    changed: [localhost] => (item=centos7)
    changed: [localhost] => (item=ubuntu)

    TASK [Delete docker networks(s)] ***********************************************

    PLAY RECAP *********************************************************************
    localhost                  : ok=2    changed=2    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0

    INFO     Pruning extra files from scenario ephemeral directory
    ```
> 3. Перейдите в каталог с ролью elastic-role и создайте сценарий тестирования по умолчаню при помощи `molecule init scenario --driver-name docker`.

  - создаем сценарий тестирования:
    ```bash
    $ molecule init scenario --driver-name docker
    INFO     Initializing new scenario default...
    INFO     Initialized scenario in /home/aleksandr/git/devops-netology/08-ansible-03-role/playbook/roles/elastic-role/molecule/default successfully.
    ```

> 4. Добавьте несколько разных дистрибутивов (centos:8, ubuntu:latest) для инстансов и протестируйте роль, исправьте найденные ошибки, если они есть.

  - добавляем дистрибутивы:
    ```yml
    ---
    dependency:
      name: galaxy
    driver:
      name: docker
    platforms:
      - name: centos8
        image: docker.io/pycontribs/centos:8
        pre_build_image: true
      - name: centos7
        image: docker.io/pycontribs/centos:7
        pre_build_image: true
      - name: ubuntu
        image: docker.io/pycontribs/ubuntu:latest
        pre_build_image: true
    provisioner:
      name: ansible
    verifier:
      name: ansible
    ```

  - вносим изменения в `meta/main.yml`:
    ```yml
    galaxy_info:
      author: Aleksandr Manzin
      role_name: elastic
      namespace: docker
      description: Install elasticsearch from remote server
      company: Netology
      license: BSD-3-Clause
      min_ansible_version: 2.8
      platforms:
        - name: CentOS
          versions:
            - 7
            - 8
        - name: ubuntu
          versions: all
      galaxy_tags: [elasticsearch]
    dependencies: []
    ```

  - также подправляем в некоторых файлах детали/неточности

  - тестирование роли:
    ```bash
    molecule test
    INFO     default scenario test matrix: dependency, lint, cleanup, destroy, syntax, create, prepare, converge, idempotence, side_effect, verify, cleanup, destroy
    INFO     Performing prerun with role_name_check=0...
    INFO     Set ANSIBLE_LIBRARY=/home/aleksandr/.cache/ansible-compat/986051/modules:/home/aleksandr/.ansible/plugins/modules:/usr/share/ansible/plugins/modules
    INFO     Set ANSIBLE_COLLECTIONS_PATH=/home/aleksandr/.cache/ansible-compat/986051/collections:/home/aleksandr/.ansible/collections:/usr/share/ansible/collections
    INFO     Set ANSIBLE_ROLES_PATH=/home/aleksandr/.cache/ansible-compat/986051/roles:/home/aleksandr/.ansible/roles:/usr/share/ansible/roles:/etc/ansible/roles
    INFO     Using /home/aleksandr/.cache/ansible-compat/986051/roles/docker.elastic symlink to current repository in order to enable Ansible to find the role using its expected full name.
    INFO     Running default > dependency
    INFO     Running ansible-galaxy collection install -v --pre community.docker:>=3.0.0-a2
    WARNING  Skipping, missing the requirements file.
    WARNING  Skipping, missing the requirements file.
    INFO     Running default > lint
    INFO     Lint is disabled.
    INFO     Running default > cleanup
    WARNING  Skipping, cleanup playbook not configured.
    INFO     Running default > destroy
    INFO     Sanity checks: 'docker'

    PLAY [Destroy] *****************************************************************

    TASK [Destroy molecule instance(s)] ********************************************
    changed: [localhost] => (item=centos8)
    changed: [localhost] => (item=centos7)
    changed: [localhost] => (item=ubuntu)

    TASK [Wait for instance(s) deletion to complete] *******************************
    ok: [localhost] => (item=centos8)
    ok: [localhost] => (item=centos7)
    ok: [localhost] => (item=ubuntu)

    TASK [Delete docker networks(s)] ***********************************************

    PLAY RECAP *********************************************************************
    localhost                  : ok=2    changed=1    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0

    INFO     Running default > syntax

    playbook: /home/aleksandr/git/devops-netology/08-ansible-03-role/playbook/roles/elastic/molecule/default/converge.yml
    INFO     Running default > create

    PLAY [Create] ******************************************************************

    TASK [Log into a Docker registry] **********************************************
    skipping: [localhost] => (item=None)
    skipping: [localhost] => (item=None)
    skipping: [localhost] => (item=None)
    skipping: [localhost]

    TASK [Check presence of custom Dockerfiles] ************************************
    ok: [localhost] => (item={'image': 'docker.io/pycontribs/centos:8', 'name': 'centos8', 'pre_build_image': True})
    ok: [localhost] => (item={'image': 'docker.io/pycontribs/centos:7', 'name': 'centos7', 'pre_build_image': True})
    ok: [localhost] => (item={'image': 'docker.io/pycontribs/ubuntu:latest', 'name': 'ubuntu', 'pre_build_image': True})

    TASK [Create Dockerfiles from image names] *************************************
    skipping: [localhost] => (item={'image': 'docker.io/pycontribs/centos:8', 'name': 'centos8', 'pre_build_image': True})
    skipping: [localhost] => (item={'image': 'docker.io/pycontribs/centos:7', 'name': 'centos7', 'pre_build_image': True})
    skipping: [localhost] => (item={'image': 'docker.io/pycontribs/ubuntu:latest', 'name': 'ubuntu', 'pre_build_image': True})

    TASK [Discover local Docker images] ********************************************
    ok: [localhost] => (item={'changed': False, 'skipped': True, 'skip_reason': 'Conditional result was False', 'item': {'image': 'docker.io/pycontribs/centos:8', 'name': 'centos8', 'pre_build_image': True}, 'ansible_loop_var': 'item', 'i': 0, 'ansible_index_var': 'i'})
    ok: [localhost] => (item={'changed': False, 'skipped': True, 'skip_reason': 'Conditional result was False', 'item': {'image': 'docker.io/pycontribs/centos:7', 'name': 'centos7', 'pre_build_image': True}, 'ansible_loop_var': 'item', 'i': 1, 'ansible_index_var': 'i'})
    ok: [localhost] => (item={'changed': False, 'skipped': True, 'skip_reason': 'Conditional result was False', 'item': {'image': 'docker.io/pycontribs/ubuntu:latest', 'name': 'ubuntu', 'pre_build_image': True}, 'ansible_loop_var': 'item', 'i': 2, 'ansible_index_var': 'i'})

    TASK [Build an Ansible compatible image (new)] *********************************
    skipping: [localhost] => (item=molecule_local/docker.io/pycontribs/centos:8)
    skipping: [localhost] => (item=molecule_local/docker.io/pycontribs/centos:7)
    skipping: [localhost] => (item=molecule_local/docker.io/pycontribs/ubuntu:latest)

    TASK [Create docker network(s)] ************************************************

    TASK [Determine the CMD directives] ********************************************
    ok: [localhost] => (item={'image': 'docker.io/pycontribs/centos:8', 'name': 'centos8', 'pre_build_image': True})
    ok: [localhost] => (item={'image': 'docker.io/pycontribs/centos:7', 'name': 'centos7', 'pre_build_image': True})
    ok: [localhost] => (item={'image': 'docker.io/pycontribs/ubuntu:latest', 'name': 'ubuntu', 'pre_build_image': True})

    TASK [Create molecule instance(s)] *********************************************
    changed: [localhost] => (item=centos8)
    changed: [localhost] => (item=centos7)
    changed: [localhost] => (item=ubuntu)

    TASK [Wait for instance(s) creation to complete] *******************************
    changed: [localhost] => (item={'failed': 0, 'started': 1, 'finished': 0, 'ansible_job_id': '100668839962.718712', 'results_file': '/home/aleksandr/.ansible_async/100668839962.718712', 'changed': True, 'item': {'image': 'docker.io/pycontribs/centos:8', 'name': 'centos8', 'pre_build_image': True}, 'ansible_loop_var': 'item'})
    changed: [localhost] => (item={'failed': 0, 'started': 1, 'finished': 0, 'ansible_job_id': '621439033844.718739', 'results_file': '/home/aleksandr/.ansible_async/621439033844.718739', 'changed': True, 'item': {'image': 'docker.io/pycontribs/centos:7', 'name': 'centos7', 'pre_build_image': True}, 'ansible_loop_var': 'item'})
    changed: [localhost] => (item={'failed': 0, 'started': 1, 'finished': 0, 'ansible_job_id': '251976391018.718764', 'results_file': '/home/aleksandr/.ansible_async/251976391018.718764', 'changed': True, 'item': {'image': 'docker.io/pycontribs/ubuntu:latest', 'name': 'ubuntu', 'pre_build_image': True}, 'ansible_loop_var': 'item'})

    PLAY RECAP *********************************************************************
    localhost                  : ok=5    changed=2    unreachable=0    failed=0    skipped=4    rescued=0    ignored=0

    INFO     Running default > prepare
    WARNING  Skipping, prepare playbook not configured.
    INFO     Running default > converge

    PLAY [Converge] ****************************************************************

    TASK [Gathering Facts] *********************************************************
    ok: [centos8]
    ok: [ubuntu]
    ok: [centos7]

    TASK [Include elastic-role] ****************************************************

    TASK [elastic-role : Upload tar.gz Elasticsearch from remote URL] *******************
    changed: [centos7]
    changed: [ubuntu]
    changed: [centos8]

    TASK [elastic-role : Create directrory for Elasticsearch] ***************************
    changed: [centos7]
    changed: [ubuntu]
    changed: [centos8]

    TASK [elastic-role : Extract Elasticsearch in the installation directory] ***********
    changed: [ubuntu]
    changed: [centos8]
    changed: [centos7]

    TASK [elastic-role : Set environment Elastic] ***************************************
    changed: [centos7]
    changed: [ubuntu]
    changed: [centos8]

    PLAY RECAP *********************************************************************
    centos7                    : ok=5    changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
    centos8                    : ok=5    changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
    ubuntu                     : ok=5    changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

    INFO     Running default > idempotence

    PLAY [Converge] ****************************************************************

    TASK [Gathering Facts] *********************************************************
    ok: [ubuntu]
    ok: [centos8]
    ok: [centos7]

    TASK [Include elastic-role] ****************************************************

    TASK [elastic-role : Upload tar.gz Elasticsearch from remote URL] *******************
    ok: [centos8]
    ok: [ubuntu]
    ok: [centos7]

    TASK [elastic-role : Create directrory for Elasticsearch] ***************************
    ok: [centos7]
    ok: [centos8]
    ok: [ubuntu]

    TASK [elastic-role : Extract Elasticsearch in the installation directory] ***********
    skipping: [ubuntu]
    skipping: [centos7]
    skipping: [centos8]

    TASK [elastic-role : Set environment Elastic] ***************************************
    ok: [centos7]
    ok: [centos8]
    ok: [ubuntu]

    PLAY RECAP *********************************************************************
    centos7                    : ok=4    changed=0    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0
    centos8                    : ok=4    changed=0    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0
    ubuntu                     : ok=4    changed=0    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0

    INFO     Idempotence completed successfully.
    INFO     Running default > side_effect
    WARNING  Skipping, side effect playbook not configured.
    INFO     Running default > verify
    INFO     Running Ansible Verifier

    PLAY [Verify] ******************************************************************

    TASK [Example assertion] *******************************************************
    ok: [centos7] => {
        "changed": false,
        "msg": "All assertions passed"
    }
    ok: [centos8] => {
        "changed": false,
        "msg": "All assertions passed"
    }
    ok: [ubuntu] => {
        "changed": false,
        "msg": "All assertions passed"
    }

    PLAY RECAP *********************************************************************
    centos7                    : ok=1    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
    centos8                    : ok=1    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
    ubuntu                     : ok=1    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

    INFO     Verifier completed successfully.
    INFO     Running default > cleanup
    WARNING  Skipping, cleanup playbook not configured.
    INFO     Running default > destroy

    PLAY [Destroy] *****************************************************************

    TASK [Destroy molecule instance(s)] ********************************************
    changed: [localhost] => (item=centos8)
    changed: [localhost] => (item=centos7)
    changed: [localhost] => (item=ubuntu)

    TASK [Wait for instance(s) deletion to complete] *******************************
    changed: [localhost] => (item=centos8)
    changed: [localhost] => (item=centos7)
    changed: [localhost] => (item=ubuntu)

    TASK [Delete docker networks(s)] ***********************************************

    PLAY RECAP *********************************************************************
    localhost                  : ok=2    changed=2    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0

    INFO     Pruning extra files from scenario ephemeral directory
    ```

> 5. Создайте новый каталог с ролью при помощи `molecule init role --driver-name docker kibana-role`. Можете использовать другой драйвер, который более удобен вам.

  - создаём роль `kibana-role`:
    ```bash
    $ molecule init role 'acme.kibana-role'
    INFO     Initializing new role kibana...
    Invalid -W option ignored: unknown warning category: 'CryptographyDeprecationWarning'
    Using /etc/ansible/ansible.cfg as config file
    - Role kibana was created successfully
    Invalid -W option ignored: unknown warning category: 'CryptographyDeprecationWarning'
    localhost | CHANGED => {"backup": "","changed": true,"msg": "line added"}
    INFO     Initialized role in /home/aleksandr/git/devops-netology/08-ansible-03-role/playbook/roles/kibana-role successfully.
    ```

> 6. На основе tasks из старого playbook заполните новую role. Разнесите переменные между `vars` и `default`. Проведите тестирование на разных дистрибитивах (centos:7, centos:8, ubuntu).

  - тестирование новой роли `kibana-role`:
    ```bash
    molecule test
    INFO     default scenario test matrix: dependency, lint, cleanup, destroy, syntax, create, prepare, converge, idempotence, side_effect, verify, cleanup, destroy
    INFO     Performing prerun with role_name_check=0...
    INFO     Set ANSIBLE_LIBRARY=/home/aleksandr/.cache/ansible-compat/805c02/modules:/home/aleksandr/.ansible/plugins/modules:/usr/share/ansible/plugins/modules
    INFO     Set ANSIBLE_COLLECTIONS_PATH=/home/aleksandr/.cache/ansible-compat/805c02/collections:/home/aleksandr/.ansible/collections:/usr/share/ansible/collections
    INFO     Set ANSIBLE_ROLES_PATH=/home/aleksandr/.cache/ansible-compat/805c02/roles:/home/aleksandr/.ansible/roles:/usr/share/ansible/roles:/etc/ansible/roles
    INFO     Using /home/aleksandr/.cache/ansible-compat/805c02/roles/docker.elastic symlink to current repository in order to enable Ansible to find the role using its expected full name.
    INFO     Running default > dependency
    WARNING  Skipping, missing the requirements file.
    WARNING  Skipping, missing the requirements file.
    INFO     Running default > lint
    INFO     Lint is disabled.
    INFO     Running default > cleanup
    WARNING  Skipping, cleanup playbook not configured.
    INFO     Running default > destroy
    INFO     Sanity checks: 'docker'

    PLAY [Destroy] *****************************************************************

    TASK [Destroy molecule instance(s)] ********************************************
    changed: [localhost] => (item=centos8)
    changed: [localhost] => (item=centos7)
    changed: [localhost] => (item=ubuntu)

    TASK [Wait for instance(s) deletion to complete] *******************************
    ok: [localhost] => (item=centos8)
    ok: [localhost] => (item=centos7)
    ok: [localhost] => (item=ubuntu)

    TASK [Delete docker networks(s)] ***********************************************

    PLAY RECAP *********************************************************************
    localhost                  : ok=2    changed=1    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0

    INFO     Running default > syntax

    playbook: /home/aleksandr/git/devops-netology/08-ansible-03-role/playbook/roles/kibana/molecule/default/converge.yml
    INFO     Running default > create

    PLAY [Create] ******************************************************************

    TASK [Log into a Docker registry] **********************************************
    skipping: [localhost] => (item=None)
    skipping: [localhost] => (item=None)
    skipping: [localhost] => (item=None)
    skipping: [localhost]

    TASK [Check presence of custom Dockerfiles] ************************************
    ok: [localhost] => (item={'image': 'docker.io/pycontribs/centos:8', 'name': 'centos8', 'pre_build_image': True})
    ok: [localhost] => (item={'image': 'docker.io/pycontribs/centos:7', 'name': 'centos7', 'pre_build_image': True})
    ok: [localhost] => (item={'image': 'docker.io/pycontribs/ubuntu:latest', 'name': 'ubuntu', 'pre_build_image': True})

    TASK [Create Dockerfiles from image names] *************************************
    skipping: [localhost] => (item={'image': 'docker.io/pycontribs/centos:8', 'name': 'centos8', 'pre_build_image': True})
    skipping: [localhost] => (item={'image': 'docker.io/pycontribs/centos:7', 'name': 'centos7', 'pre_build_image': True})
    skipping: [localhost] => (item={'image': 'docker.io/pycontribs/ubuntu:latest', 'name': 'ubuntu', 'pre_build_image': True})

    TASK [Discover local Docker images] ********************************************
    ok: [localhost] => (item={'changed': False, 'skipped': True, 'skip_reason': 'Conditional result was False', 'item': {'image': 'docker.io/pycontribs/centos:8', 'name': 'centos8', 'pre_build_image': True}, 'ansible_loop_var': 'item', 'i': 0, 'ansible_index_var': 'i'})
    ok: [localhost] => (item={'changed': False, 'skipped': True, 'skip_reason': 'Conditional result was False', 'item': {'image': 'docker.io/pycontribs/centos:7', 'name': 'centos7', 'pre_build_image': True}, 'ansible_loop_var': 'item', 'i': 1, 'ansible_index_var': 'i'})
    ok: [localhost] => (item={'changed': False, 'skipped': True, 'skip_reason': 'Conditional result was False', 'item': {'image': 'docker.io/pycontribs/ubuntu:latest', 'name': 'ubuntu', 'pre_build_image': True}, 'ansible_loop_var': 'item', 'i': 2, 'ansible_index_var': 'i'})

    TASK [Build an Ansible compatible image (new)] *********************************
    skipping: [localhost] => (item=molecule_local/docker.io/pycontribs/centos:8)
    skipping: [localhost] => (item=molecule_local/docker.io/pycontribs/centos:7)
    skipping: [localhost] => (item=molecule_local/docker.io/pycontribs/ubuntu:latest)

    TASK [Create docker network(s)] ************************************************

    TASK [Determine the CMD directives] ********************************************
    ok: [localhost] => (item={'image': 'docker.io/pycontribs/centos:8', 'name': 'centos8', 'pre_build_image': True})
    ok: [localhost] => (item={'image': 'docker.io/pycontribs/centos:7', 'name': 'centos7', 'pre_build_image': True})
    ok: [localhost] => (item={'image': 'docker.io/pycontribs/ubuntu:latest', 'name': 'ubuntu', 'pre_build_image': True})

    TASK [Create molecule instance(s)] *********************************************
    changed: [localhost] => (item=centos8)
    changed: [localhost] => (item=centos7)
    changed: [localhost] => (item=ubuntu)

    TASK [Wait for instance(s) creation to complete] *******************************
    changed: [localhost] => (item={'failed': 0, 'started': 1, 'finished': 0, 'ansible_job_id': '898710632384.735779', 'results_file': '/home/aleksandr/.ansible_async/898710632384.735779', 'changed': True, 'item': {'image': 'docker.io/pycontribs/centos:8', 'name': 'centos8', 'pre_build_image': True}, 'ansible_loop_var': 'item'})
    changed: [localhost] => (item={'failed': 0, 'started': 1, 'finished': 0, 'ansible_job_id': '87186819715.735805', 'results_file': '/home/aleksandr/.ansible_async/87186819715.735805', 'changed': True, 'item': {'image': 'docker.io/pycontribs/centos:7', 'name': 'centos7', 'pre_build_image': True}, 'ansible_loop_var': 'item'})
    changed: [localhost] => (item={'failed': 0, 'started': 1, 'finished': 0, 'ansible_job_id': '562087512109.735834', 'results_file': '/home/aleksandr/.ansible_async/562087512109.735834', 'changed': True, 'item': {'image': 'docker.io/pycontribs/ubuntu:latest', 'name': 'ubuntu', 'pre_build_image': True}, 'ansible_loop_var': 'item'})

    PLAY RECAP *********************************************************************
    localhost                  : ok=5    changed=2    unreachable=0    failed=0    skipped=4    rescued=0    ignored=0

    INFO     Running default > prepare
    WARNING  Skipping, prepare playbook not configured.
    INFO     Running default > converge

    PLAY [Converge] ****************************************************************

    TASK [Include kibana] **********************************************************

    TASK [kibana-role : Upload tar.gz Kibana from remote url] ***************************
    changed: [centos7]
    changed: [centos8]
    changed: [ubuntu]

    TASK [kibana-role : Create directory for Kibana] ************************************
    changed: [centos7]
    changed: [ubuntu]
    changed: [centos8]

    TASK [kibana-role : Extract Kibana in the installation directory] *******************
    changed: [centos8]
    changed: [ubuntu]
    changed: [centos7]

    TASK [kibana-role : Set environment Kibana] *****************************************
    changed: [centos7]
    changed: [centos8]
    changed: [ubuntu]

    PLAY RECAP *********************************************************************
    centos7                    : ok=4    changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
    centos8                    : ok=4    changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
    ubuntu                     : ok=4    changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

    INFO     Running default > idempotence

    PLAY [Converge] ****************************************************************

    TASK [Include kibana] **********************************************************

    TASK [kibana-role : Upload tar.gz Kibana from remote url] ***************************
    ok: [centos7]
    ok: [centos8]
    ok: [ubuntu]

    TASK [kibana-role : Create directory for Kibana] ************************************
    ok: [centos7]
    ok: [ubuntu]
    ok: [centos8]

    TASK [kibana-role : Extract Kibana in the installation directory] *******************
    skipping: [centos7]
    skipping: [centos8]
    skipping: [ubuntu]

    TASK [kibana-role : Set environment Kibana] *****************************************
    ok: [centos7]
    ok: [centos8]
    ok: [ubuntu]

    PLAY RECAP *********************************************************************
    centos7                    : ok=3    changed=0    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0
    centos8                    : ok=3    changed=0    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0
    ubuntu                     : ok=3    changed=0    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0

    INFO     Idempotence completed successfully.
    INFO     Running default > side_effect
    WARNING  Skipping, side effect playbook not configured.
    INFO     Running default > verify
    INFO     Running Ansible Verifier

    PLAY [Verify] ******************************************************************

    TASK [Example assertion] *******************************************************
    ok: [centos7] => {
        "changed": false,
        "msg": "All assertions passed"
    }
    ok: [centos8] => {
        "changed": false,
        "msg": "All assertions passed"
    }
    ok: [ubuntu] => {
        "changed": false,
        "msg": "All assertions passed"
    }

    PLAY RECAP *********************************************************************
    centos7                    : ok=1    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
    centos8                    : ok=1    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
    ubuntu                     : ok=1    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

    INFO     Verifier completed successfully.
    INFO     Running default > cleanup
    WARNING  Skipping, cleanup playbook not configured.
    INFO     Running default > destroy

    PLAY [Destroy] *****************************************************************

    TASK [Destroy molecule instance(s)] ********************************************
    changed: [localhost] => (item=centos8)
    changed: [localhost] => (item=centos7)
    changed: [localhost] => (item=ubuntu)

    TASK [Wait for instance(s) deletion to complete] *******************************
    FAILED - RETRYING: [localhost]: Wait for instance(s) deletion to complete (300 retries left).
    changed: [localhost] => (item=centos8)
    changed: [localhost] => (item=centos7)
    changed: [localhost] => (item=ubuntu)

    TASK [Delete docker networks(s)] ***********************************************

    PLAY RECAP *********************************************************************
    localhost                  : ok=2    changed=2    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0

    INFO     Pruning extra files from scenario ephemeral directory
    ```

> 7. Выложите все roles в репозитории. Проставьте тэги, используя семантическую нумерацию.

  - [java](https://gitlab.com/aleksandr.manzin/devops-netology/-/tree/main/08-ansible-03-role/playbook/roles/java)
  - [elastic-role](git@gitlab.com:aleksandr.manzin/elastic-role.git)
  - [kibana-role](git@gitlab.com:aleksandr.manzin/kibana-role.git)

> 8. Добавьте roles в `requirements.yml` в playbook.

  - `requirements.yml`:
    ```yml
    ---
      - src: git@github.com:netology-code/mnt-homeworks-ansible.git
        scm: git
        version: "1.0.1"
        name: java
      - src: git@gitlab.com:aleksandr.manzin/elastic-role.git
        scm: git
        version: "1.0.1"
        name: elastic-role
      - src: git@gitlab.com:aleksandr.manzin/kibana-role.git
        scm: git
        version: "1.0.1"
        name: kibana-role
    ```

> 9. Переработайте playbook на использование roles.

  - изменение `playbook`:
    ```yml
    ---
    - name: Install Java
      hosts: all
      roles:
        - java
        - elastic-role
        - kibana-role
    ```
  - запуск `playbook`:
    ```bash
    $ ansible-playbook -i inventory/prod.yml site.yml

    PLAY [Install Java] *************************************************************************************************************************************************************

    TASK [Gathering Facts] **********************************************************************************************************************************************************
    ok: [elastic]

    TASK [java : Upload .tar.gz file containing binaries from local storage] ********************************************************************************************************
    skipping: [elastic]

    TASK [java : Upload .tar.gz file conaining binaries from remote storage] ********************************************************************************************************
    changed: [elastic]

    TASK [java : Ensure installation dir exists] ************************************************************************************************************************************
    changed: [elastic]

    TASK [java : Extract java in the installation directory] ************************************************************************************************************************
    changed: [elastic]

    TASK [java : Export environment variables] **************************************************************************************************************************************
    changed: [elastic]

    TASK [elastic-role : Upload tar.gz Elasticsearch from remote URL] ********************************************************************************************************************
    changed: [elastic]

    TASK [elastic-role : Create directrory for Elasticsearch] ****************************************************************************************************************************
    ok: [elastic]

    TASK [elastic-role : Extract Elasticsearch in the installation directory] ************************************************************************************************************
    skipping: [elastic]

    TASK [elastic-role : Set environment Elastic] ****************************************************************************************************************************************
    ok: [elastic]

    TASK [kibana-role : Upload tar.gz Kibana from remote url] ****************************************************************************************************************************
    ok: [elastic]

    TASK [kibana-role : Create directory for Kibana] *************************************************************************************************************************************
    ok: [elastic]

    TASK [kibana-role : Extract Kibana in the installation directory] ********************************************************************************************************************
    skipping: [elastic]

    TASK [kibana-role : Set environment Kibana] ******************************************************************************************************************************************
    ok: [elastic]

    PLAY RECAP **********************************************************************************************************************************************************************
    elastic                    : ok=11   changed=5    unreachable=0    failed=0    skipped=3    rescued=0    ignored=0  
    ```
> 10. Выложите playbook в репозиторий.
> 11. В ответ приведите ссылки на оба репозитория с roles и одну ссылку на репозиторий с playbook.

  - [playbook](https://gitlab.com/aleksandr.manzin/devops-netology/-/tree/main/08-ansible-03-role/playbook)
  - [elastic-role](https://gitlab.com/aleksandr.manzin/elastic-role)
  - [kibana-role](https://gitlab.com/aleksandr.manzin/kibana-role)
