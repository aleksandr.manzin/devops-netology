# Домашнее задание к занятию "6.2. SQL"

## Задача 1

> Используя docker поднимите инстанс PostgreSQL (версию 12) c 2 volume, в который будут складываться данные БД и бэкапы.
>
> Приведите получившуюся команду или docker-compose манифест.

- т.к. у меня на ноутбуке установлен `PostgreSQL`, в `docker` я поменял порт на `5555`. `docker-compose.yml` получился следующим:

  ```yaml
  version: '2.9'

  volumes:
    pg_data:
    pg_backup:

  services:
    pg_db:
      image: postgres:12
      restart: always
      environment:
        - POSTGRES_PASSWORD=netology
      volumes:
        - pg_data:/var/lib/postgresql/data
        - pg_backup:/backup
      ports:
        - 5555:5432
  ```
- запустим наш контейнер:
  ```bash
  $ docker compose up -d
  [+] Running 3/3
   ⠿ Volume "06-db-02-sql_pg_data"     Created                0.0s
   ⠿ Volume "06-db-02-sql_pg_backup"   Created                0.0s
   ⠿ Container 06-db-02-sql-pg_db-1    Started                0.6s
  ```
- проверяем запущенные контейнеры:
  ```bash
  $ docker compose ps   
  NAME                   COMMAND                  SERVICE             STATUS              PORTS
  06-db-02-sql-pg_db-1   "docker-entrypoint.s…"   pg_db               running             0.0.0.0:5555->5432/tcp, :::5555->5432/tcp
  ```
## Задача 2

> В БД из задачи 1:
- создайте пользователя test-admin-user и БД test_db
- в БД test_db создайте таблицу orders и clients (спeцификация таблиц ниже)
- предоставьте привилегии на все операции пользователю test-admin-user на таблицы БД test_db
- создайте пользователя test-simple-user  
- предоставьте пользователю test-simple-user права на SELECT/INSERT/UPDATE/DELETE данных таблиц БД test_db
>
> Таблица orders:
- id (serial primary key)
- наименование (string)
- цена (integer)
>
> Таблица clients:
- id (serial primary key)
- ФИО (string)
- страна проживания (string, index)
- заказ (foreign key orders)
>
> Приведите:
- итоговый список БД после выполнения пунктов выше,
- описание таблиц (describe)
- SQL-запрос для выдачи списка пользователей с правами над таблицами test_db
- список пользователей с правами над таблицами test_db

- подключаемся к нашему контейнеру, запустив сразу `psql`:
  ```bash
  docker exec -it 06-db-02-sql-pg_db-1 psql -U postgres
  psql (12.11 (Debian 12.11-1.pgdg110+1))
  Type "help" for help.
  ```
- создаем пользователя:
  ```SQL
  postgres=# CREATE USER "test-admin-user" WITH PASSWORD 'netology';
  CREATE ROLE

  postgres=# \du
  List of roles
  Role name    |                         Attributes                         | Member of
  -----------------+------------------------------------------------------------+-----------
  postgres        | Superuser, Create role, Create DB, Replication, Bypass RLS | {}
  test-admin-user |                                                            | {}
  ```
- создаем БД:
  ```SQL
  postgres=# CREATE DATABASE test_db;
  CREATE DATABASE
  ```
- переключаемся на БД `test_db` и создаем таблицы `orders` и `clients`:
  ```SQL
  postgres=# \c test_db
  You are now connected to database "test_db" as user "postgres".

  test_db=# CREATE TABLE orders
  (
    id              SERIAL PRIMARY KEY,
    "Наименование"  TEXT,
    "Цена"          INT
  );
  CREATE TABLE

  test_db=# CREATE TABLE clients
  (
    id                   SERIAL PRIMARY KEY,
    "ФИО"                VARCHAR(30),
    "Страна проживания"  VARCHAR(20),
    "Заказ"              TEXT REFERENCES orders(id)
  );

  test_db=# CREATE INDEX country ON clients("Страна проживания");
  CREATE INDEX
  ```
- выдаем права пользователю `test-admin-user` на таблицы `orders` и `clients`:
  ```SQL
  test_db=# GRANT ALL PRIVILEGES ON orders TO "test-admin-user";
  GRANT
  test_db=# GRANT ALL PRIVILEGES ON clients TO "test-admin-user";
  GRANT
  ```
- создаём пользователя `test-simple-user`
  ```SQL
  test_db=# CREATE USER "test-simple-user" WITH PASSWORD 'netology';
  CREATE ROLE
  ```
- предоставляем пользователю `test-simple-user` права на `SELECT/INSERT/UPDATE/DELETE` для таблиц `orders` и `clients`:
  ```SQL
  test_db=# GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO "test-simple-user";
  GRANT
  test_db=# GRANT SELECT, INSERT, UPDATE, DELETE ON clients TO "test-simple-user";
  GRANT
  ```

- итоговый список БД после выполнения пунктов выше:
  ```SQL
  test_db=# \list
                                   List of databases
     Name    |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges   
  -----------+----------+----------+------------+------------+-----------------------
   postgres  | postgres | UTF8     | en_US.utf8 | en_US.utf8 |
   template0 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
             |          |          |            |            | postgres=CTc/postgres
   template1 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
             |          |          |            |            | postgres=CTc/postgres
   test_db   | postgres | UTF8     | en_US.utf8 | en_US.utf8 |
  (4 rows)
  ```

- описание таблиц (describe):
  ```SQL
  test_db=# \d orders
                                 Table "public.orders"
      Column    |  Type   | Collation | Nullable |              Default               
  --------------+---------+-----------+----------+------------------------------------
   id           | integer |           | not null | nextval('orders_id_seq'::regclass)
   Наименование | text    |           |          |
   Цена         | integer |           |          |
  Indexes:
      "orders_pkey" PRIMARY KEY, btree (id)
  Referenced by:
      TABLE "clients" CONSTRAINT "clients_Заказ_fkey" FOREIGN KEY ("Заказ") REFERENCES orders(id)

  test_db=# \d clients
                                           Table "public.clients"
        Column       |         Type          | Collation | Nullable |               Default               
  -------------------+-----------------------+-----------+----------+-------------------------------------
   id                | integer               |           | not null | nextval('clients_id_seq'::regclass)
   ФИО               | character varying(30) |           |          |
   Страна проживания | character varying(20) |           |          |
   Заказ             | integer               |           |          |
  Indexes:
      "clients_pkey" PRIMARY KEY, btree (id)
      "country" btree ("Страна проживания")
  Foreign-key constraints:
      "clients_Заказ_fkey" FOREIGN KEY ("Заказ") REFERENCES orders(id)

  ```

- SQL-запрос для выдачи списка пользователей с правами над таблицами test_db:
    ```SQL
    test_db=# SELECT grantee, table_catalog, table_name, privilege_type
              FROM information_schema.table_privileges
              WHERE grantee IN ('test-admin-user', 'test-simple-user');\

         grantee      | table_catalog | table_name | privilege_type
    ------------------+---------------+------------+----------------
     test-admin-user  | test_db       | orders     | INSERT
     test-admin-user  | test_db       | orders     | SELECT
     test-admin-user  | test_db       | orders     | UPDATE
     test-admin-user  | test_db       | orders     | DELETE
     test-admin-user  | test_db       | orders     | TRUNCATE
     test-admin-user  | test_db       | orders     | REFERENCES
     test-admin-user  | test_db       | orders     | TRIGGER
     test-simple-user | test_db       | orders     | INSERT
     test-simple-user | test_db       | orders     | SELECT
     test-simple-user | test_db       | orders     | UPDATE
     test-simple-user | test_db       | orders     | DELETE
     test-admin-user  | test_db       | clients    | INSERT
     test-admin-user  | test_db       | clients    | SELECT
     test-admin-user  | test_db       | clients    | UPDATE
     test-admin-user  | test_db       | clients    | DELETE
     test-admin-user  | test_db       | clients    | TRUNCATE
     test-admin-user  | test_db       | clients    | REFERENCES
     test-admin-user  | test_db       | clients    | TRIGGER
     test-simple-user | test_db       | clients    | INSERT
     test-simple-user | test_db       | clients    | SELECT
     test-simple-user | test_db       | clients    | UPDATE
     test-simple-user | test_db       | clients    | DELETE
    (22 rows)
    ```

- список пользователей с правами над таблицами test_db:
  ```SQL
  test_db-# \dp
                                             Access privileges
   Schema |      Name      |   Type   |         Access privileges          | Column privileges | Policies
  --------+----------------+----------+------------------------------------+-------------------+----------
   public | clients        | table    | postgres=arwdDxt/postgres         +|                   |
          |                |          | "test-admin-user"=arwdDxt/postgres+|                   |
          |                |          | "test-simple-user"=arwd/postgres   |                   |
   public | clients_id_seq | sequence |                                    |                   |
   public | orders         | table    | postgres=arwdDxt/postgres         +|                   |
          |                |          | "test-admin-user"=arwdDxt/postgres+|                   |
          |                |          | "test-simple-user"=arwd/postgres   |                   |
   public | orders_id_seq  | sequence |                                    |                   |
  (4 rows)
  ```

## Задача 3

> Используя SQL синтаксис - наполните таблицы следующими тестовыми данными:
>
> Таблица orders
>
> |Наименование|цена|
> |------------|----|
> |Шоколад| 10 |
> |Принтер| 3000 |
> |Книга| 500 |
> |Монитор| 7000|
> |Гитара| 4000|
>
>Таблица clients
>
> |ФИО|Страна проживания|
> |------------|----|
> |Иванов Иван Иванович| USA |
> |Петров Петр Петрович| Canada |
> |Иоганн Себастьян Бах| Japan |
> |Ронни Джеймс Дио| Russia|
> |Ritchie Blackmore| Russia|
>
> Используя SQL синтаксис:
- вычислите количество записей для каждой таблицы
- приведите в ответе:
    - запросы
    - результаты их выполнения.

* заполняем таблицу `orders`:
  ```SQL
  test_db=# INSERT INTO orders ("Наименование", "Цена")
            VALUES ('Шоколад', 10),
  	             ('Принтер', 3000),
                   ('Книга', 500),
  	             ('Монитор', 7000),
  	             ('Гитара', 4000);
  ```
* заполняем таблицу `clients`:
  ```SQL
  test_db=# INSERT INTO clients ("ФИО", "Страна проживания")
            VALUES ('Петров Петр Петрович', 'Canada'),
    		       ('Иванов Иван Иванович', 'USA'),
                   ('Иоганн Себастьян Бах', 'Japan'),
                   ('Ронни Джеймс Джо', 'Russia'),
                   ('Ritchie Blackmore', 'Russia');
  ```
* вычисляем количество записей для каждой таблицы:
  ```SQL
  test_db=# SELECT COUNT(*) "Количество записей:" FROM orders;
   Количество записей:
  ---------------------
                     5
  (1 row)

  test_db=# SELECT COUNT(*) "Количество записей:" FROM clients;
   Количество записей:
  ---------------------
                     5
  (1 row)
  ```

## Задача 4

> Часть пользователей из таблицы clients решили оформить заказы из таблицы orders.
>
>  Используя foreign keys свяжите записи из таблиц, согласно таблице:
>
>  |ФИО|Заказ|
>  |------------|----|
>  |Иванов Иван Иванович| Книга |
>  |Петров Петр Петрович| Монитор |
>  |Иоганн Себастьян Бах| Гитара |
>
>  Приведите SQL-запросы для выполнения данных операций.
>
>  Приведите SQL-запрос для выдачи всех пользователей, которые совершили заказ, а также вывод данного запроса.
>   
>  Подсказк - используйте директиву `UPDATE`.

* добавляем заказы клиентам:
  ```SQL
  test_db=# UPDATE clients
            SET "Заказ"=3
            WHERE id=1;
  UPDATE 1

  test_db=# UPDATE clients
            SET "Заказ"=4
            WHERE "ФИО"='Петров Петр Петрович';
  UPDATE 1

  test_db=# UPDATE clients
            SET "Заказ"=5
            WHERE id=3;
  UPDATE 1
  ```
* проверяем всех пользователей, у которых есть заказ:
  ```SQL
  test_db=# SELECT *
            FROM clients
            WHERE "Заказ" > 0;

   id |         ФИО          | Страна проживания | Заказ
  ----+----------------------+-------------------+-------
    1 | Иванов Иван Иванович | USA               |     3
    2 | Петров Петр Петрович | Canada            |     4
    3 | Иоганн Себастьян Бах | Japan             |     5
  (3 rows)

  test_db=# SELECT *
            FROM clients
            WHERE "Заказ" IS NOT NULL;

   id |         ФИО          | Страна проживания | Заказ
  ----+----------------------+-------------------+-------
    1 | Иванов Иван Иванович | USA               |     3
    2 | Петров Петр Петрович | Canada            |     4
    3 | Иоганн Себастьян Бах | Japan             |     5
  (3 rows)

  ```
## Задача 5

> Получите полную информацию по выполнению запроса выдачи всех пользователей из задачи 4
  (используя директиву EXPLAIN).
>
> Приведите получившийся результат и объясните что значат полученные значения.

  ```SQL
  test_db=# EXPLAIN
            SELECT *
            FROM clients
            WHERE "Заказ" > 0;

              QUERY PLAN                         
  ------------------------------------------------------------
  Seq Scan on clients  (cost=0.00..15.88 rows=157 width=144)
  Filter: ("Заказ" > 0)
  (2 rows)

  test_db=# EXPLAIN
            SELECT *
            FROM clients
            WHERE "Заказ" IS NOT NULL;

              QUERY PLAN                         
  ------------------------------------------------------------
  Seq Scan on clients  (cost=0.00..14.70 rows=468 width=144)
  Filter: ("Заказ" IS NOT NULL)
  (2 rows)
  ```

## Задача 6
> Создайте бэкап БД test_db и поместите его в volume, предназначенный для бэкапов (см. Задачу 1).
>
> Остановите контейнер с PostgreSQL (но не удаляйте volumes).
>
> Поднимите новый пустой контейнер с PostgreSQL.
>
> Восстановите БД test_db в новом контейнере.
>
> Приведите список операций, который вы применяли для бэкапа данных и восстановления.

* делаем полный дамп БД:
  ```SQL
  test_db=# pg_dump -U postgres -d test_db > /backup/test_db.dump
  ```
* отключаемся от контейнера и останавливаем его:
  ```bash
  $ docker compose stop
  [+] Running 1/1
   ⠿ Container 06-db-02-sql-pg_db-1  Stopped  
  $ docker compose ps
  NAME                   COMMAND                  SERVICE             STATUS              PORTS
  06-db-02-sql-pg_db-1   "docker-entrypoint.s…"   pg_db               exited (0)
  ```
* делаем правки в нашем `docker-compose.yml`. Меняем порт на 5550 и комментируем `volume` с полной структурой БД. Также меняем имя сервиса на `pg_db_2`:
  ```yaml
  version: '2.9'

  volumes:
  #  pg_data:
    pg_backup:

  services:
    pg_db_2:
      image: postgres:12
      restart: always
      environment:
        - POSTGRES_PASSWORD=netology
      volumes:
  #      - pg_data:/var/lib/postgresql/data
        - pg_backup:/backup
      ports:
        - 5550:5432
  ```
* запускаем наш контейнер и подключаемся к нему:
  ```bash
  $ docker compose up -d
  WARN[0000] Found orphan containers ([06-db-02-sql-pg_db-1]) for this project. If you removed or renamed this service in your compose file, you can run this command with the --remove-orphans flag to clean it up.
  [+] Running 1/1
   ⠿ Container 06-db-02-sql-pg_db_2-1  Started
  $ docker compose ps
  NAME                     COMMAND                  SERVICE             STATUS              PORTS
  06-db-02-sql-pg_db-1     "docker-entrypoint.s…"   pg_db               exited (0)          
  06-db-02-sql-pg_db_2-1   "docker-entrypoint.s…"   pg_db_2             running             0.0.0.0:5550->5432/tcp, :::5550->5432/tcp
  $ docker exec -it 06-db-02-sql-pg_db_2-1 bash
  ```
* восстанавливаем базу:
  ```bash
  $ psql -U postgres -d test_db -f /backup/test_db.dump
  psql: error: connection to server on socket "/var/run/postgresql/.s.PGSQL.5432" failed: FATAL:  database "test_db" does not exist
  ```
* как видим, мы получили ошибку, т.к. у нас такой базы нет. Давайте создадим её и снова попробуем восстановить:
  - ```bash
    $ psql -U postgres
    psql (12.11 (Debian 12.11-1.pgdg110+1))
    Type "help" for help.
    ```
  - ```SQL
    postgres=# CREATE DATABASE test_db;
    CREATE DATABASE
    \q
    ```
  - ```bash
    $ psql -U postgres -d test_db -f /backup/test_db.dump
    SET
    SET
    SET
    SET
    SET
     set_config
    ------------

    (1 row)

    SET
    SET
    SET
    SET
    SET
    SET
    CREATE TABLE
    ALTER TABLE
    CREATE SEQUENCE
    ALTER TABLE
    ALTER SEQUENCE
    CREATE TABLE
    ALTER TABLE
    CREATE SEQUENCE
    ALTER TABLE
    ALTER SEQUENCE
    ALTER TABLE
    ALTER TABLE
    COPY 5
    COPY 5
     setval
    --------
          5
    (1 row)

     setval
    --------
         15
    (1 row)

    ALTER TABLE
    ALTER TABLE
    CREATE INDEX
    ALTER TABLE
    psql:/backup/test_db.dump:183: ERROR:  role "test-admin-user" does not exist
    psql:/backup/test_db.dump:184: ERROR:  role "test-simple-user" does not exist
    psql:/backup/test_db.dump:191: ERROR:  role "test-admin-user" does not exist
    psql:/backup/test_db.dump:192: ERROR:  role "test-simple-user" does not exist
    ```
  - как видим база успешно восстановилась:
    ```SQL
    postgres=# \c test_db
    You are now connected to database "test_db" as user "postgres".
    test_db=# SELECT * FROM clients;
     id |         ФИО          | Страна проживания | Заказ
    ----+----------------------+-------------------+-------
      4 | Ронни Джеймс Джо     | Russia            |      
      5 | Ritchie Blackmore    | Russia            |      
      1 | Иванов Иван Иванович | USA               |     3
      2 | Петров Петр Петрович | Canada            |     4
      3 | Иоганн Себастьян Бах | Japan             |     5
    (5 rows)

    test_db=# SELECT * FROM orders;
     id | Наименование | Цена
    ----+--------------+------
      1 | Шоколад      |   10
      2 | Принтер      | 3000
      3 | Книга        |  500
      4 | Монитор      | 7000
      5 | Гитара       | 4000
    (5 rows)
    ```
