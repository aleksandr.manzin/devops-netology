# Домашнее задание к занятию "08.02 Работа с Playbook"

## Подготовка к выполнению
> 1. Создайте свой собственный (или используйте старый) публичный репозиторий на github с произвольным именем.
> 2. Скачайте [playbook](./playbook/) из репозитория с домашним заданием и перенесите его в свой репозиторий.
> 3. Подготовьте хосты в соотвтествии с группами из предподготовленного playbook.
> 4. Скачайте дистрибутив [java](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html) и положите его в директорию `playbook/files/`.

  ![Java](screenshots/java.png "Java")

## Основная часть
> 1. Приготовьте свой собственный inventory файл `prod.yml`.

  ![](screenshots/prod.png "prod.yml")

> 2. Допишите playbook: нужно сделать ещё один play, который устанавливает и настраивает kibana.
> 3. При создании tasks рекомендую использовать модули: `get_url`, `template`, `unarchive`, `file`.
> 4. Tasks должны: скачать нужной версии дистрибутив, выполнить распаковку в выбранную директорию, сгенерировать конфигурацию с параметрами.

  - `site.yml` - добавлена установка кибаны
    ```yml
    - name: Install Kibana
      hosts: kibana
      tasks:
        - name: Upload tar.gz Kibana from remote url
          ansible.builtin.get_url:
            url: "https://artifacts.elastic.co/downloads/kibana/kibana-{{ kibana_version }}-linux-x86_64.tar.gz"
            dest: "/tmp/kibana-{{ kibana_version }}-linux-x86_64.tar.gz"
            mode: 0644
            timeout: 60
            force: true
            validate_certs: false
          register: get_kibana
          until: get_kibana is succeeded
          tags: kibana
        - name: Create directory for Kibana
          ansible.builtin.file:
            state: directory
            path: "{{ kibana_home }}"
            mode: 0755
          tags: kibana
        - name: Extract Kibana in the installation directory
          become: true
          ansible.builtin.unarchive:
            copy: false
            src: "/tmp/kibana-{{ kibana_version }}-linux-x86_64.tar.gz"
            dest: "{{ kibana_home }}"
            extra_opts: [--strip-components=1]
            creates: "{{ kibana_home }}/bin/kibana"
            mode: 0755
          tags:
            - kibana
        - name: Set environment Kibana
          become: true
          ansible.builtin.template:
            src: templates/kibana.sh.j2
            dest: /etc/profile.d/kibana.sh
            mode: 0644
          tags: kibana
    ```
  - `group_vars/kibana/vars.yml`:
    ```yml
    ---
    kibana_version: "8.4.2"
    kibana_home: "/opt/kibana/{{ kibana_version }}"
    ```
  - `templates/kibana.sh.j2`
    ```bash
    # Warning: This file is Ansible Managed, manual changes will be overwritten on next playbook run.
    #!/usr/bin/env bash

    export KIBANA_HOME={{ kibana_home }}
    export PATH=$PATH:$KIBANA_HOME/bin
    ```

> 5. Запустите `ansible-lint site.yml` и исправьте ошибки, если они есть.

  - для начала понадобилось установить `ansible-lint`:
    ```bash
    $ pip install ansible-lint
    ```
  - запуск `ansible-lint` выдал предупреждения:
    ```bash
    fqcn-builtins: Use FQCN for builtin actions.
    site.yml:9 Task/Handler: Upload .tar.gz file containing binaries from local storage
    .........................
    fqcn-builtins: Use FQCN for builtin actions.
    playbook/site.yml:102 Task/Handler: Set environment Kibana

    risky-file-permissions: File permissions unset or incorrect.
    playbook/site.yml:102 Task/Handler: Set environment Kibana

    You can skip specific rules or tags by adding them to your configuration file:
    # .config/ansible-lint.yml

    warn_list:  # or 'skip_list' to silence them completely
      - experimental  # all rules tagged as experimental
      - fqcn-builtins  # Use FQCN for builtin actions.
    ```

  - папкам и архивам выдал разрешения с помощью `mode`. Модули заменил на `ansible.builtin.*` (вместо `*`, соответственно, название модуля)

  - также обнаружил ошибку `WARNING  Overriding detected file kind 'yaml' with 'playbook' for given positional argument: site.yml`. Помогло переименование папки `playbook` в `playbooks`

    ![](screenshots/linter.png "Ansible lint")

> 6. Попробуйте запустить playbook на этом окружении с флагом `--check`.

  - `ansible-playbook -i inventory/prod.yml site.yml --check`

    ![](screenshots/check.png "Playbook Check")

  - т.к. у нас не созданы директории, то `--check` останавливается на ошибке. Поэтому для тасков, в которых может вылететь ошибка, добавил условие `when: not ansible_check_mode`, которое проверяет указан параметр `--check` или нет. В случае, если параметр `true`, то пропускает таск:
    ```bash
    $ ansible-playbook -i inventory/prod.yml site.yml --check
    PLAY [Install Java] *************************************************************************************************************************************************************************************************

    TASK [Gathering Facts] **********************************************************************************************************************************************************************************************
    ok: [ubuntu]

    TASK [Set facts for Java 11 vars] ***********************************************************************************************************************************************************************************
    ok: [ubuntu]

    TASK [Upload .tar.gz file containing binaries from local storage] ***************************************************************************************************************************************************
    changed: [ubuntu]

    TASK [Ensure installation dir exists] *******************************************************************************************************************************************************************************
    skipping: [ubuntu]

    TASK [Extract java in the installation directory] *******************************************************************************************************************************************************************
    skipping: [ubuntu]

    TASK [Export environment variables] *********************************************************************************************************************************************************************************
    changed: [ubuntu]

    PLAY [Install Elasticsearch] ****************************************************************************************************************************************************************************************

    TASK [Gathering Facts] **********************************************************************************************************************************************************************************************
    ok: [ubuntu]

    TASK [Upload tar.gz Elasticsearch from remote URL] ******************************************************************************************************************************************************************
    changed: [ubuntu]

    TASK [Create directory for Elasticsearch] ***************************************************************************************************************************************************************************
    skipping: [ubuntu]

    TASK [Extract Elasticsearch in the installation directory] **********************************************************************************************************************************************************
    skipping: [ubuntu]

    TASK [Set environment Elastic] **************************************************************************************************************************************************************************************
    changed: [ubuntu]

    PLAY [Install Kibana] ***********************************************************************************************************************************************************************************************

    TASK [Gathering Facts] **********************************************************************************************************************************************************************************************
    ok: [ubuntu]

    TASK [Upload tar.gz Kibana from remote url] *************************************************************************************************************************************************************************
    changed: [ubuntu]

    TASK [Create directory for Kibana] **********************************************************************************************************************************************************************************
    skipping: [ubuntu]

    TASK [Extract Kibana in the installation directory] *****************************************************************************************************************************************************************
    skipping: [ubuntu]

    TASK [Set environment Kibana] ***************************************************************************************************************************************************************************************
    changed: [ubuntu]

    PLAY RECAP **********************************************************************************************************************************************************************************************************
    ubuntu                     : ok=10   changed=6    unreachable=0    failed=0    skipped=6    rescued=0    ignored=0  
    ```

> 7. Запустите playbook на `prod.yml` окружении с флагом `--diff`. Убедитесь, что изменения на системе произведены.

  - `ansible-playbook -i inventory/prod.yml site.yml --diff`
    ```bash
    $ ansible-playbook -i inventory/prod.yml site.yml --diff

    PLAY [Install Java] *************************************************************************************************************************************************************************************************

    TASK [Gathering Facts] **********************************************************************************************************************************************************************************************
    ok: [ubuntu]

    TASK [Set facts for Java 11 vars] ***********************************************************************************************************************************************************************************
    ok: [ubuntu]

    TASK [Upload .tar.gz file containing binaries from local storage] ***************************************************************************************************************************************************
    diff skipped: source file size is greater than 104448
    changed: [ubuntu]

    TASK [Ensure installation dir exists] *******************************************************************************************************************************************************************************
    --- before
    +++ after
    @@ -1,4 +1,4 @@
     {
         "path": "/opt/jdk/11.0.16",
    -    "state": "absent"
    +    "state": "directory"
     }

    changed: [ubuntu]

    TASK [Extract java in the installation directory] *******************************************************************************************************************************************************************
    changed: [ubuntu]

    TASK [Export environment variables] *********************************************************************************************************************************************************************************
    --- before
    +++ after: /home/aleksandr/.ansible/tmp/ansible-local-95741ggq2lwpq/tmplsktnz5j/jdk.sh.j2
    @@ -0,0 +1,5 @@
    +# Warning: This file is Ansible Managed, manual changes will be overwritten on next playbook run.
    +#!/usr/bin/env bash
    +
    +export JAVA_HOME=/opt/jdk/11.0.16
    +export PATH=$PATH:$JAVA_HOME/bin
    \ No newline at end of file

    changed: [ubuntu]

    PLAY [Install Elasticsearch] ****************************************************************************************************************************************************************************************

    TASK [Gathering Facts] **********************************************************************************************************************************************************************************************
    ok: [ubuntu]

    TASK [Upload tar.gz Elasticsearch from remote URL] ******************************************************************************************************************************************************************
    FAILED - RETRYING: [ubuntu]: Upload tar.gz Elasticsearch from remote URL (3 retries left).
    changed: [ubuntu]

    TASK [Create directory for Elasticsearch] ***************************************************************************************************************************************************************************
    --- before
    +++ after
    @@ -1,4 +1,4 @@
     {
         "path": "/opt/elastic/8.4.2",
    -    "state": "absent"
    +    "state": "directory"
     }

    changed: [ubuntu]

    TASK [Extract Elasticsearch in the installation directory] **********************************************************************************************************************************************************
    changed: [ubuntu]

    TASK [Set environment Elastic] **************************************************************************************************************************************************************************************
    --- before
    +++ after: /home/aleksandr/.ansible/tmp/ansible-local-95741ggq2lwpq/tmprsk31pg_/elk.sh.j2
    @@ -0,0 +1,5 @@
    +# Warning: This file is Ansible Managed, manual changes will be overwritten on next playbook run.
    +#!/usr/bin/env bash
    +
    +export ES_HOME=/opt/elastic/8.4.2
    +export PATH=$PATH:$ES_HOME/bin
    \ No newline at end of file

    changed: [ubuntu]

    PLAY [Install Kibana] ***********************************************************************************************************************************************************************************************

    TASK [Gathering Facts] **********************************************************************************************************************************************************************************************
    ok: [ubuntu]

    TASK [Upload tar.gz Kibana from remote url] *************************************************************************************************************************************************************************
    FAILED - RETRYING: [ubuntu]: Upload tar.gz Kibana from remote url (3 retries left).
    FAILED - RETRYING: [ubuntu]: Upload tar.gz Kibana from remote url (2 retries left).
    changed: [ubuntu]

    TASK [Create directory for Kibana] **********************************************************************************************************************************************************************************
    --- before
    +++ after
    @@ -1,4 +1,4 @@
     {
         "path": "/opt/kibana/8.4.2",
    -    "state": "absent"
    +    "state": "directory"
     }

    changed: [ubuntu]

    TASK [Extract Kibana in the installation directory] *****************************************************************************************************************************************************************
    changed: [ubuntu]

    TASK [Set environment Kibana] ***************************************************************************************************************************************************************************************
    --- before
    +++ after: /home/aleksandr/.ansible/tmp/ansible-local-95741ggq2lwpq/tmpo8ffo8c4/kibana.sh.j2
    @@ -0,0 +1,5 @@
    +# Warning: This file is Ansible Managed, manual changes will be overwritten on next playbook run.
    +#!/usr/bin/env bash
    +
    +export KIBANA_HOME=/opt/kibana/8.4.2
    +export PATH=$PATH:$KIBANA_HOME/bin

    changed: [ubuntu]

    PLAY RECAP **********************************************************************************************************************************************************************************************************
    ubuntu                     : ok=16   changed=12   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
    ```

> 8. Повторно запустите playbook с флагом `--diff` и убедитесь, что playbook идемпотентен.

  - результат идемпотентен:

    ![](screenshots/idempotency.png "Idempotency")
> 9. Подготовьте README.md файл по своему playbook. В нём должно быть описано: что делает playbook, какие у него есть параметры и теги.
> 10. Готовый playbook выложите в свой репозиторий, в ответ предоставьте ссылку на него.

  [README.md](https://gitlab.com/aleksandr.manzin/devops-netology/-/tree/main/08-ansible-02-playbook/playbook/README.md)


## Необязательная часть

> 1. Приготовьте дополнительный хост для установки logstash.

  - поднят контейнер с установленной `ubuntu`

> 2. Пропишите данный хост в `prod.yml` в новую группу `logstash`.

  ```yml
  logstash:
    hosts:
      logstash:
        ansible_host: 192.168.22.101
        ansible_connection: ssh
  ```

> 3. Дополните playbook ещё одним play, который будет исполнять установку logstash только на выделенный для него хост.

  ```yml
  - name: Install Logstash
    hosts: logstash
    tasks:
      - name: Upload tar.gz Logstash from remote url
        ansible.builtin.get_url:
          url: "https://artifacts.elastic.co/downloads/logstash/logstash-{{ logstash_version }}-linux-x86_64.tar.gz"
          dest: "/tmp/logstash-{{ logstash_version }}-linux-x86_64.tar.gz"
          mode: '0644'
          timeout: 60
          force: true
          validate_certs: false
        register: get_logstash
        until: get_logstash is succeeded
        tags: logstash
      - name: Create directory for logstash
        become: true
        ansible.builtin.file:
          state: directory
          path: "{{ logstash_home }}"
          mode: '0755'
        when: not ansible_check_mode
        tags: logstash
      - name: Extract Logstash in the installation directory
        become: true
        ansible.builtin.unarchive:
          copy: false
          src: "/tmp/logstash-{{ logstash_version }}-linux-x86_64.tar.gz"
          dest: "{{ logstash_home }}"
          extra_opts: [--strip-components=1]
          creates: "{{ logstash_home }}/bin/logstash"
          mode: '0755'
        when: not ansible_check_mode
        tags:
          - logstash
      - name: Copy Logstash config
        become: true
        ansible.builtin.template:
          src: templates/logstash.conf.j2
          dest: /opt/logstash/{{ logstash_version }}/config/logstash.conf
          mode: '0644'
        tags: logstash
      - name: Set environment Logstash
        become: true
        ansible.builtin.template:
          src: templates/logstash.sh.j2
          dest: /etc/profile.d/logstash.sh
          mode: '0644'
        tags: logstash
  ```

> 4. Все переменные для нового play определите в отдельный файл `group_vars/logstash/vars.yml`.

  ```yml
  ---
  logstash_version: "8.4.2"
  logstash_home: "/opt/logstash/{{ logstash_version }}"
  ```

> 5. Logstash конфиг должен конфигурироваться в части ссылки на elasticsearch (можно взять, например его IP из facts или определить через vars).

  - в папке `templates` создан шаблон `logstash.conf.j2`:
    ```yml
    input {
      beats {
        port => 5044
        host => "127.0.0.1"
      }
    }

    output {
      elasticsearch {
        hosts => ["192.168.22.100:9200"]
      }
      stdout {
        codec => rubydebug
      }
    }
    ```

> 6. Дополните README.md, протестируйте playbook, выложите новую версию в github. В ответ предоставьте ссылку на репозиторий.

  ```bash
  $ ansible-playbook site.yml
  PLAY [Install Java] ***************************************************************************************************************************************************************************************************************************

  TASK [Gathering Facts] ************************************************************************************************************************************************************************************************************************
  ok: [elastic]
  ok: [logstash]

  TASK [Set facts for Java 11 vars] *************************************************************************************************************************************************************************************************************
  ok: [elastic]
  ok: [logstash]

  TASK [Upload .tar.gz file containing binaries from local storage] *****************************************************************************************************************************************************************************
  changed: [elastic]
  changed: [logstash]

  TASK [Ensure installation dir exists] *********************************************************************************************************************************************************************************************************
  changed: [elastic]
  changed: [logstash]

  TASK [Extract java in the installation directory] *********************************************************************************************************************************************************************************************
  changed: [elastic]
  changed: [logstash]

  TASK [Export environment variables] ***********************************************************************************************************************************************************************************************************
  changed: [elastic]
  changed: [logstash]

  PLAY [Install Elasticsearch] ******************************************************************************************************************************************************************************************************************

  TASK [Gathering Facts] ************************************************************************************************************************************************************************************************************************
  ok: [elastic]

  TASK [Upload tar.gz Elasticsearch from remote URL] ********************************************************************************************************************************************************************************************
  changed: [elastic]

  TASK [Create directory for Elasticsearch] *****************************************************************************************************************************************************************************************************
  changed: [elastic]

  TASK [Extract Elasticsearch in the installation directory] ************************************************************************************************************************************************************************************
  changed: [elastic]

  TASK [Set environment Elastic] ****************************************************************************************************************************************************************************************************************
  changed: [elastic]

  PLAY [Install Kibana] *************************************************************************************************************************************************************************************************************************

  TASK [Gathering Facts] ************************************************************************************************************************************************************************************************************************
  ok: [elastic]

  TASK [Upload tar.gz Kibana from remote url] ***************************************************************************************************************************************************************************************************
  FAILED - RETRYING: [elastic]: Upload tar.gz Kibana from remote url (3 retries left).
  changed: [elastic]

  TASK [Create directory for Kibana] ************************************************************************************************************************************************************************************************************
  changed: [elastic]

  TASK [Extract Kibana in the installation directory] *******************************************************************************************************************************************************************************************
  changed: [elastic]

  TASK [Set environment Kibana] *****************************************************************************************************************************************************************************************************************
  changed: [elastic]

  PLAY [Install Logstash] ***********************************************************************************************************************************************************************************************************************

  TASK [Gathering Facts] ************************************************************************************************************************************************************************************************************************
  ok: [logstash]

  TASK [Upload tar.gz Logstash from remote url] *************************************************************************************************************************************************************************************************
  FAILED - RETRYING: [logstash]: Upload tar.gz Logstash from remote url (3 retries left).
  changed: [logstash]

  TASK [Create directory for logstash] **********************************************************************************************************************************************************************************************************
  changed: [logstash]

  TASK [Extract Logstash in the installation directory] *****************************************************************************************************************************************************************************************
  changed: [logstash]

  TASK [Copy Logstash config] *******************************************************************************************************************************************************************************************************************
  changed: [logstash]

  TASK [Set environment Logstash] ***************************************************************************************************************************************************************************************************************
  changed: [logstash]

  PLAY RECAP ************************************************************************************************************************************************************************************************************************************
  elastic                    : ok=15   changed=7    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0   
  logstash                   : ok=11   changed=5    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0   
  ```

  [README.md](https://gitlab.com/aleksandr.manzin/devops-netology/-/tree/main/08-ansible-02-playbook/)
