# Домашнее задание к занятию "4.1. Командная оболочка Bash: Практические навыки"
---
### Задача №1
> Есть скрипт:
```bash
a=1
b=2
c=a+b
d=$a+$b
e=$(($a+$b))
```

> Какие значения переменным c,d,e будут присвоены? Почему?

|Переменная	|Значение	|Обоснование																																																						|	
|:---					|:---				|:---																																																										|
|`c`					|`a+b`		|тут мы пытаемся сложить переменные, а не их значения																																				|
|`d`					|`1+2`		|т.к. переменные объявлены в неявном виде и мы обращаемся к их значениям, то на выходе получаем вывод в виде операции сложения строк 	|
|`e`					|`3`				|сложение значений переменных в явном виде																																								|


___

### Задача №2 
> На нашем локальном сервере упал сервис и мы написали скрипт, который постоянно проверяет его доступность, записывая дату проверок до тех пор, пока сервис не станет доступным (после чего скрипт должен завершиться). В скрипте допущена ошибка, из-за которой выполнение не может завершиться, при этом место на Жёстком Диске постоянно уменьшается. Что необходимо сделать, чтобы его исправить:

В коде отсутствует вторая закрывающая скобка в цикле `while` и `break` для прерывания скрипта. В остальном код полностью рабочий:
```bash
while ((1==1))
do
	curl https://localhost:4757
	if (( "$?" != 0 ))
	then
		date >> curl.log
		break
	fi
done
```

___

### Задача №3

> Необходимо написать скрипт, который проверяет доступность трёх IP: 192.168.0.1, 173.194.222.113, 87.250.250.242 по 80 порту и записывает результат в файл log. Проверять доступность необходимо пять раз для каждого узла.

Для реализации данной задачи решил воспользоваться утилитой `nmap`, хоть и понимаю, что можно было делать через тот же `curl`, который был в предыдущем примере, или через `netcat`:

```bash
#!/usr/bin/env bash
addresses=("192.168.0.1" "173.194.222.113" "87.250.250.242")
for IP in "${addresses[@]}"
do
        for (( i = 0; i < 5; i++ ))
        do
                echo `date`
                result="$(nmap -p 80 "$IP")"
                echo "$result"
        done
done >> nmap.log
```

```bash
$ cat nmap.log
Mon 21 Mar 2022 08:27:44 PM MSK
Starting Nmap 7.80 ( https://nmap.org ) at 2022-03-23 23:02 MSK
Note: Host seems down. If it is really up, but blocking our ping probes, try -Pn
Nmap done: 1 IP address (0 hosts up) scanned in 3.07 seconds
Mon 21 Mar 2022 08:27:47 PM MSK
Starting Nmap 7.80 ( https://nmap.org ) at 2022-03-23 23:02 MSK
Note: Host seems down. If it is really up, but blocking our ping probes, try -Pn
Nmap done: 1 IP address (0 hosts up) scanned in 3.08 seconds
Mon 21 Mar 2022 08:27:50 PM MSK
Starting Nmap 7.80 ( https://nmap.org ) at 2022-03-23 23:02 MSK
Note: Host seems down. If it is really up, but blocking our ping probes, try -Pn
Nmap done: 1 IP address (0 hosts up) scanned in 3.08 seconds
Mon 21 Mar 2022 08:27:53 PM MSK
Starting Nmap 7.80 ( https://nmap.org ) at 2022-03-23 23:02 MSK
Note: Host seems down. If it is really up, but blocking our ping probes, try -Pn
Nmap done: 1 IP address (0 hosts up) scanned in 3.08 seconds
Mon 21 Mar 2022 08:27:56 PM MSK
Starting Nmap 7.80 ( https://nmap.org ) at 2022-03-23 23:02 MSK
Note: Host seems down. If it is really up, but blocking our ping probes, try -Pn
Nmap done: 1 IP address (0 hosts up) scanned in 3.05 seconds
Mon 21 Mar 2022 08:27:59 PM MSK
Starting Nmap 7.80 ( https://nmap.org ) at 2022-03-23 23:02 MSK
Nmap scan report for lo-in-f113.1e100.net (173.194.222.113)
Host is up (0.0049s latency).

PORT   STATE SERVICE
80/tcp open  http

Nmap done: 1 IP address (1 host up) scanned in 0.05 seconds
Mon 21 Mar 2022 08:27:59 PM MSK
Starting Nmap 7.80 ( https://nmap.org ) at 2022-03-23 23:02 MSK
Nmap scan report for lo-in-f113.1e100.net (173.194.222.113)
Host is up (0.0048s latency).

PORT   STATE SERVICE
80/tcp open  http

Nmap done: 1 IP address (1 host up) scanned in 0.05 seconds
Mon 21 Mar 2022 08:27:59 PM MSK
Starting Nmap 7.80 ( https://nmap.org ) at 2022-03-23 23:02 MSK
Nmap scan report for lo-in-f113.1e100.net (173.194.222.113)
Host is up (0.0054s latency).

PORT   STATE SERVICE
80/tcp open  http

Nmap done: 1 IP address (1 host up) scanned in 0.05 seconds
Mon 21 Mar 2022 08:27:59 PM MSK
Starting Nmap 7.80 ( https://nmap.org ) at 2022-03-23 23:02 MSK
Nmap scan report for lo-in-f113.1e100.net (173.194.222.113)
Host is up (0.0050s latency).

PORT   STATE SERVICE
80/tcp open  http

Nmap done: 1 IP address (1 host up) scanned in 0.05 seconds
Mon 21 Mar 2022 08:28:00 PM MSK
Starting Nmap 7.80 ( https://nmap.org ) at 2022-03-23 23:03 MSK
Nmap scan report for lo-in-f113.1e100.net (173.194.222.113)
Host is up (0.0049s latency).

PORT   STATE SERVICE
80/tcp open  http

Nmap done: 1 IP address (1 host up) scanned in 0.05 seconds
Mon 21 Mar 2022 08:28:00 PM MSK
Starting Nmap 7.80 ( https://nmap.org ) at 2022-03-23 23:03 MSK
Nmap scan report for ya.ru (87.250.250.242)
Host is up (0.012s latency).

PORT   STATE SERVICE
80/tcp open  http

Nmap done: 1 IP address (1 host up) scanned in 0.08 seconds
Mon 21 Mar 2022 08:28:00 PM MSK
Starting Nmap 7.80 ( https://nmap.org ) at 2022-03-23 23:03 MSK
Nmap scan report for ya.ru (87.250.250.242)
Host is up (0.012s latency).

PORT   STATE SERVICE
80/tcp open  http

Nmap done: 1 IP address (1 host up) scanned in 0.06 seconds
Mon 21 Mar 2022 08:28:00 PM MSK
Starting Nmap 7.80 ( https://nmap.org ) at 2022-03-23 23:03 MSK
Nmap scan report for ya.ru (87.250.250.242)
Host is up (0.012s latency).

PORT   STATE SERVICE
80/tcp open  http

Nmap done: 1 IP address (1 host up) scanned in 0.06 seconds
Mon 21 Mar 2022 08:28:00 PM MSK
Starting Nmap 7.80 ( https://nmap.org ) at 2022-03-23 23:03 MSK
Nmap scan report for ya.ru (87.250.250.242)
Host is up (0.013s latency).

PORT   STATE SERVICE
80/tcp open  http

Nmap done: 1 IP address (1 host up) scanned in 0.07 seconds
Mon 21 Mar 2022 08:28:00 PM MSK
Starting Nmap 7.80 ( https://nmap.org ) at 2022-03-23 23:03 MSK
Nmap scan report for ya.ru (87.250.250.242)
Host is up (0.013s latency).

PORT   STATE SERVICE
80/tcp open  http

Nmap done: 1 IP address (1 host up) scanned in 0.07 seconds
```

___

### Задача №4
> Необходимо дописать скрипт из предыдущего задания так, чтобы он выполнялся до тех пор, пока один из узлов не окажется недоступным. Если любой из узлов недоступен - IP этого узла пишется в файл error, скрипт прерывается.


```bash
#!/usr/bin/env bash
addresses=("173.194.222.113" "87.250.250.242" "192.168.0.1")
while (( i < 1 ))
do
        for IP in "${addresses[@]}"
        do
                result="$(nmap -p 80 "$IP" | grep "Note")"
                pattern="Host seems down"
                if [[ $result =~ $pattern ]]
                then
                        echo "$(date) - host "$IP" seems down" >> ~/error.log
                        i="$(( i + 1 ))"
                else
                        echo "$(date) - host "$IP" is up"
                fi
        done
done
```

```bash
$ cat error.log
Mon 21 Mar 2022 08:56:12 PM MSK - 192.168.0.1 seems down
```

___

### Задача №5

> Мы хотим, чтобы у нас были красивые сообщения для коммитов в репозиторий. Для этого нужно написать локальный хук для git, который будет проверять, что сообщение в коммите содержит код текущего задания в квадратных скобках и количество символов в сообщении не превышает 30. Пример сообщения: [04-script-01-bash] сломал хук.

В `.git\hooks` в файле `commit-msg.sample` написал следующий код и убрал расширение:

```bash
#!/usr/bin/env bash

max=30
len=$(cat $1 | awk '{print length}')
message="[04-script-[0-9]-[a-z]]"
pattern="\[04-script-[0-9]{1,9}-[a-z]{1,9}\]"
str=$(grep -Eoh "$pattern" $1)

if [[ -z $str ]]; then
	echo "Commit message don't match pattern, $message is missing"
	exit 1
elif [[ "$len" -gt "$max" ]]; then
	echo "Commit message should contain "$max" symbols max"
	exit 1
else
	echo "Commit message OK"
	exit 0
fi
```
Возможные варианты событий при попытке коммита:

* неправильный шаблон:
```bash
git commit -m "04-script-01-bash" 
Commit message don't match pattern, [04-script-[0-9]-[a-z] is missing
```

* превышение длины сообщения коммита:
```bash
git commit -m "[04-script-01-bash] testingcommitmessage"
Commit message should contain 30 symbols max
```

* успешный ввод сообщения коммита:
```bash
git commit -m "[04-script-02-python] test"
Commit message OK
```