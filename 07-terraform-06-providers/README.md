# Домашнее задание к занятию "7.6. Написание собственных провайдеров для Terraform."

## Задача 1.
> Давайте потренируемся читать исходный код AWS провайдера, который можно склонировать от сюда:
[https://github.com/hashicorp/terraform-provider-aws.git](https://github.com/hashicorp/terraform-provider-aws.git).
Просто найдите нужные ресурсы в исходном коде и ответы на вопросы станут понятны.  
>
> 1. Найдите, где перечислены все доступные `resource` и `data_source`, приложите ссылку на эти строки в коде на гитхабе.   
> 1. Для создания очереди сообщений SQS используется ресурс `aws_sqs_queue` у которого есть параметр `name`.
>   * С каким другим параметром конфликтует `name`? Приложите строчку кода, в которой это указано.
>   * Какая максимальная длина имени?
>   * Какому регулярному выражению должно подчиняться имя?

1. была использована версия `4.29.0`:

    ![Version](screenshots/version.png "Version")

    - перечисление `data_source` начинается на строке `415`:

      ![Data Sources](screenshots/datasources.png "Data Sources Map")

      Cсылка на [github](https://github.com/hashicorp/terraform-provider-aws/blob/152b2be34ec29440fa7f3fc5a7c81f12f8ffcb45/internal/provider/provider.go#L415)

    - перечисление `resource` начинается на строке `925`:

      ![Resourcesmap](screenshots/resources.png "Resources Map")

      Cсылка на [github](https://github.com/hashicorp/terraform-provider-aws/blob/152b2be34ec29440fa7f3fc5a7c81f12f8ffcb45/internal/provider/provider.go#L925)

1. в коде находим файла `provider.go` находим ресурс `aws_sqs_queue`:

    ![AWS SQS](screenshots/sqs.png "aws_sqs_queue")

    - видим в коде, что используется пакет `sqs`. Идем по пути `terraform-provider-aws/internal/service/sqs/` и открываем файл `queue.go` (через `GoLand` очень удобно, что можно быстро перейти по необходимому пути). Параметр `name` конфликтует с `name_prefix`:

      Ссылка на [github](https://github.com/hashicorp/terraform-provider-aws/blob/e5a0325c8ebf7bb18db36d06155285ea7f58b444/internal/service/sqs/queue.go#L82)

      ![Conflicts](screenshots/conflicts.png "Name Conflicts")

    - максимальная длина не может превышать 80 символов (описано в регулярном выражении):

      ![Length](screenshots/name.png "Max Name Length")

    - имя получаем из условия (ссылка на [github](https://github.com/hashicorp/terraform-provider-aws/blob/e5a0325c8ebf7bb18db36d06155285ea7f58b444/internal/service/sqs/queue.go#L424))

      - если `fifoQueue`, то имя может состоять максимум из 80 символов, 75 из которых могут включать верний и нижний регистры, тире и нижнее подчеркивание, а также `.fifo` в конце имени (`^[a-zA-Z0-9_-]{1,75}.fifo$`);

      - в противном случае, имя состоит от 1 до 80 символов и может содержать верхний и нижний регистры, нижнее подчеркивание и тире (`^[a-zA-Z0-9_-]{1,80}$`)
