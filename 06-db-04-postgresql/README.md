# Домашнее задание к занятию "6.4. PostgreSQL"

## Задача 1

> Используя docker поднимите инстанс PostgreSQL (версию 13). Данные БД сохраните в volume.
>
> Подключитесь к БД PostgreSQL используя `psql`.
>
> Воспользуйтесь командой `\?` для вывода подсказки по имеющимся в `psql` управляющим командам.
>
> **Найдите и приведите** управляющие команды для:
> - вывода списка БД
> - подключения к БД
> - вывода списка таблиц
> - вывода описания содержимого таблиц
> - выхода из psql

- запустим `PostgreSQL 13` в `docker-контейнере`:
  ```bash
  $ cp test_dump.sql /backup
  $ docker run -it --name postgres -p 5555:5432 \
  -e POSTGRES_PASSWORD=netology \
  --mount type=bind,source=/backup,destination=/backup \
  --mount type=volume,source=pg_data,destination=/var/lib/postgresql/data \
  -d postgres:13
  ```
- запускаем утилиту `psql`:
  ```bash
  $ docker exec -it postgres psql -U postgres
  ```
- управляющие команды:
  - `\l` - вывод списка БД;
  - `\c DatabaseName` - подключение к БД;
  - `\dt` - вывод списка таблиц
  - `\d TableName` - вывод описания содержимого таблиц;
  - `\q` - выход из `psql`

## Задача 2

> Используя `psql` создайте БД `test_database`.
>
> Изучите [бэкап БД](https://github.com/netology-code/virt-homeworks/tree/master/06-db-04-postgresql/test_data).
>
> Восстановите бэкап БД в `test_database`.
>
> Перейдите в управляющую консоль `psql` внутри контейнера.
> Подключитесь к восстановленной БД и проведите операцию ANALYZE для сбора статистики по таблице.
>
> Используя таблицу [pg_stats](https://postgrespro.ru/docs/postgresql/12/view-pg-stats), найдите столбец таблицы `orders`
с наибольшим средним значением размера элементов в байтах.
>
> **Приведите в ответе** команду, которую вы использовали для вычисления и полученный результат.

- создадим базу `test_database`:
  ```sql
  postgres=# CREATE DATABASE test_database;
  CREATE DATABASE
  ```
- восстановим бэкап в `test_database`:
  ```bash
  $ psql -U postgres -d test_database < /backup/test_dump.sql
  SET
  SET
  SET
  SET
  SET
   set_config
  ------------

  (1 row)

  SET
  SET
  SET
  SET
  SET
  SET
  CREATE TABLE
  ALTER TABLE
  CREATE SEQUENCE
  ALTER TABLE
  ALTER SEQUENCE
  ALTER TABLE
  COPY 8
   setval
  --------
        8
  (1 row)

  ALTER TABLE
  ```
- подключимся к нашей базе:
  ```bash
  $ psql -U postgres -d test_database
  psql (13.7 (Debian 13.7-1.pgdg110+1))
  Type "help" for help.
  ```
- проведем `ANALYZE` для сбора статистики по таблице:
  ```SQL
  test_database=# \dt
           List of relations
   Schema |  Name  | Type  |  Owner   
  --------+--------+-------+----------
   public | orders | table | postgres
  (1 row)

  test_database=# ANALYZE VERBOSE orders;
  INFO:  analyzing "public.orders"
  INFO:  "orders": scanned 1 of 1 pages, containing 8 live rows and 0 dead rows; 8 rows in sample, 8 estimated total rows
  ANALYZE
  ```
- выводим столбец с наибольшим средним значением размера элементов в байтах:
  ```sql
  test_database=# SELECT max(avg_width) "Max Average Width (in bytes)"
                  FROM pg_stats
                  WHERE tablename = 'orders';
   Max Average Width (in bytes)
  ------------------------------
                             16
  (1 row)

  test_database=# SELECT tablename "Table Name",
                         attname "Column",
                         max(avg_width) "Max Average Width (in bytes)"
                  FROM pg_catalog.pg_stats
                  WHERE tablename = 'orders'
                  AND attname = 'title'
                  GROUP BY attname, tablename;
   Table Name | Column | Max Average Width (in bytes)
  ------------+--------+------------------------------
   orders     | title  |                           16
  (1 row)
  ```
## Задача 3

>  Архитектор и администратор БД выяснили, что ваша таблица orders разрослась до невиданных размеров и
>  поиск по ней занимает долгое время. Вам, как успешному выпускнику курсов DevOps в нетологии предложили
>  провести разбиение таблицы на 2 (шардировать на orders_1 - price>499 и orders_2 - price<=499).
>
>  Предложите SQL-транзакцию для проведения данной операции.
>
>  Можно ли было изначально исключить "ручное" разбиение при проектировании таблицы orders?

- создадим две таблицы (с ценой выше 499 и с ценой меньше 500):
  ```sql
  --создаем таблицу, в которую будем помещать заказы с ценой выше 499
  test_database=# CREATE TABLE orders_more_499
                  (
                      LIKE orders INCLUDING ALL
                  )
                  INHERITS (orders);
  NOTICE:  merging column "id" with inherited definition
  NOTICE:  merging column "title" with inherited definition
  NOTICE:  merging column "price" with inherited definition
  CREATE TABLE

  --создаем таблицу, в которую будем помещать заказы с ценой ниже 500
  test_database=# CREATE TABLE orders_less_499
                  (
                      LIKE orders INCLUDING ALL
                  )
                  INHERITS (orders);
  NOTICE:  merging column "id" with inherited definition
  NOTICE:  merging column "title" with inherited definition
  NOTICE:  merging column "price" with inherited definition
  CREATE TABLE

  --смотрим основную таблицу и видим, что у неё появились две дочерние
  test_database=# \d+ orders
                                                         Table "public.orders"
   Column |         Type          | Collation | Nullable |              Default               | Storage  | Stats target | Description
  --------+-----------------------+-----------+----------+------------------------------------+----------+--------------+-------------
   id     | integer               |           | not null | nextval('orders_id_seq'::regclass) | plain    |              |
   title  | character varying(80) |           | not null |                                    | extended |              |
   price  | integer               |           |          | 0                                  | plain    |              |
  Indexes:
      "orders_pkey" PRIMARY KEY, btree (id)
  Child tables: orders_less_499,
                orders_more_499
  Access method: heap
  ```
- теперь заполним новые таблицы данными из таблицы `orders`:
  ```SQL
  test_database=# INSERT INTO orders_more_499
                  SELECT * FROM orders
                  WHERE price > 499;
  INSERT 0 3

  test_database=# INSERT INTO orders_less_499
                  SELECT * FROM orders
                  WHERE price <= 499;
  INSERT 0 5

  ```
- теперь, если мы делаем, например, выборку по таблице `orders`, то проверяются все таблицы:
  ```SQL
  test_database=# EXPLAIN ANALYZE
                  SELECT * FROM orders;
                  QUERY PLAN                                                          
  -----------------------------------------------------------------------------------------------------------------------------
  Append  (cost=0.00..32.52 rows=768 width=184) (actual time=0.008..0.018 rows=16 loops=1)
  ->  Seq Scan on orders orders_1  (cost=0.00..1.08 rows=8 width=24) (actual time=0.007..0.009 rows=8 loops=1)
  ->  Seq Scan on orders_more_499 orders_2  (cost=0.00..13.80 rows=380 width=186) (actual time=0.002..0.002 rows=3 loops=1)
  ->  Seq Scan on orders_less_499 orders_3  (cost=0.00..13.80 rows=380 width=186) (actual time=0.002..0.003 rows=5 loops=1)
  Planning Time: 0.136 ms
  Execution Time: 0.039 ms
  (6 rows)
  ```
- добавим ограничения для дочерних таблиц:
  ```SQL
  test_database=# ALTER TABLE orders_more_499
                  ADD CONSTRAINT partition_more_499_check
                  CHECK (price > 499);
  ALTER TABLE

  test_database=# ALTER TABLE orders_less_499
                  ADD CONSTRAINT partition_less_499_check
                  CHECK (price <= 499);
  ALTER TABLE
  ```                
- а теперь удалим данные из таблицы `orders` и проверим таблицу `orders`:
  ```SQL
  test_database=# DELETE FROM ONLY orders;
  DELETE 8

  test_database=# SELECT * FROM orders;
  id |        title         | price
  ----+----------------------+-------
   2 | My little database   |   500
   6 | WAL never lies       |   900
   8 | Dbiezdmin            |   501
   1 | War and peace        |   100
   3 | Adventure psql time  |   300
   4 | Server gravity falls |   300
   5 | Log gossips          |   123
   7 | Me and my bash-pet   |   499
  (8 rows)
  ```
- как видим данные в таблицу подтягиваются из дочерних таблиц. Однако, нам хотелось бы и чтобы новые данные записывались в дочерние таблицы по значению цены. Т.к. таблица у нас не секционированная и преобразовать её в секционированную нельзя, то в данной ситуации мы можем создать либо триггер, либо правило, которое будет реализовывать данную задачу:
  - реализация через правила:
    ```SQL
    test_database=# CREATE RULE orders_insert_more_499 AS
                    ON INSERT TO orders
                    WHERE
                          (price > 499)
                    DO INSTEAD
                    INSERT INTO orders_more_499 VALUES (NEW.*);
    CREATE RULE
    test_database=# CREATE RULE orders_insert_less_499 AS
                    ON INSERT TO orders
                    WHERE
                          (price <= 499)
                    DO INSTEAD
                    INSERT INTO orders_less_499 VALUES (NEW.*);
    CREATE RULE
    test_database=# \d orders
                                       Table "public.orders"
     Column |         Type          | Collation | Nullable |              Default               
    --------+-----------------------+-----------+----------+------------------------------------
     id     | integer               |           | not null | nextval('orders_id_seq'::regclass)
     title  | character varying(80) |           | not null |
     price  | integer               |           |          | 0
    Indexes:
        "orders_pkey" PRIMARY KEY, btree (id)
    Rules:
        orders_insert_less_499 AS
        ON INSERT TO orders
       WHERE new.price <= 499 DO INSTEAD  INSERT INTO orders_less_499 (id, title, price)
      VALUES (new.id, new.title, new.price)
        orders_insert_more_499 AS
        ON INSERT TO orders
       WHERE new.price > 499 DO INSTEAD  INSERT INTO orders_more_499 (id, title, price)
      VALUES (new.id, new.title, new.price)
    Number of child tables: 2 (Use \d+ to list them.)
    ```
  - реализация через триггер:
    ```SQL
    -- создадим триггер, который будет вызывать функции
    test_database=# CREATE TRIGGER orders_insert_trigger
                    BEFORE INSERT ON orders
                    FOR EACH ROW EXECUTE FUNCTION orders_insert_trigger();
    CREATE TRIGGER

    -- создадим функцию
    test_database=# CREATE OR REPLACE FUNCTION orders_insert_trigger()
                    RETURNS TRIGGER AS $$
                    BEGIN
                        IF ( NEW.price <= 499 ) THEN
                            INSERT INTO orders_less_499 VALUES (NEW.*);
                        ELSIF ( NEW.price > 499 ) THEN
                            INSERT INTO orders_more_499 VALUES (NEW.*);
                        END IF;
                        RETURN NULL;
                    END;
                    $$
                    LANGUAGE plpgsql;
    CREATE FUNCTION
    test_database=# \d orders
                                       Table "public.orders"
     Column |         Type          | Collation | Nullable |              Default               
    --------+-----------------------+-----------+----------+------------------------------------
     id     | integer               |           | not null | nextval('orders_id_seq'::regclass)
     title  | character varying(80) |           | not null |
     price  | integer               |           |          | 0
    Indexes:
        "orders_pkey" PRIMARY KEY, btree (id)
    Triggers:
        orders_insert_trigger BEFORE INSERT ON orders FOR EACH ROW EXECUTE FUNCTION orders_insert_trigger()
    Number of child tables: 2 (Use \d+ to list them.)
    ```
    Однако, стоит заметить, что у правил достаточно много недостатков и, например, при копировании данных через `COPY` правила игнорируются, поэтому их необходимо копировать сразу в дочернюю таблицу (команда `COPY` не игнорирует триггеры).

- попробуем добавить данные в таблицу `orders`:
  ```SQL
  test_database=# INSERT INTO orders (title, price)
                  VALUES ('PostgreSQL', 555);
  INSERT 0 0
  test_database=# INSERT INTO orders (title, price)
                  VALUES ('SQL is awesome', 302);
  INSERT 0 0
  test_database=# SELECT * FROM orders;
   id |        title         | price
  ----+----------------------+-------
    2 | My little database   |   500
    6 | WAL never lies       |   900
    8 | Dbiezdmin            |   501
   13 | PostgreSQL           |   555
    1 | War and peace        |   100
    3 | Adventure psql time  |   300
    4 | Server gravity falls |   300
    5 | Log gossips          |   123
    7 | Me and my bash-pet   |   499
   14 | SQL is awesome       |   302
  (10 rows)

  test_database=# SELECT * FROM orders_more_499;
   id |       title        | price
  ----+--------------------+-------
    2 | My little database |   500
    6 | WAL never lies     |   900
    8 | Dbiezdmin          |   501
   13 | PostgreSQL         |   555
  (4 rows)

  test_database=# SELECT * FROM orders_less_499;
   id |        title         | price
  ----+----------------------+-------
    1 | War and peace        |   100
    3 | Adventure psql time  |   300
    4 | Server gravity falls |   300
    5 | Log gossips          |   123
    7 | Me and my bash-pet   |   499
   14 | SQL is awesome       |   302
  (6 rows)
  ```
  Как видим, значения, которые добавляются в таблицу `orders` попадают в дочерние таблицы.

- отвечая на вопрос, можно ли было изначально исключить "ручное" разбиение, скажу - **да**. Можно было изначально создать секционированную таблицу, создать секции и тогда, при добавлении значений, они сами будут распределяться по "своим" секциям:
  ```SQL
  -- для начала переименуем имеющуюся таблицу orders
  test_database=# ALTER TABLE orders
                  RENAME TO orders_original;
  ALTER TABLE

  -- создадим секционированную таблицу orders аналогичную orders_original
  test_database=# CREATE TABLE orders
                  (
                    LIKE orders_original
                  )
                  PARTITION BY RANGE(price);
  CREATE TABLE

  -- создаем две таблицы для разбития по ценовому диапазону
  test_database=# CREATE TABLE orders_less_499
                  (
                    LIKE orders INCLUDING ALL
                  );
  CREATE TABLE
  test_database=# CREATE TABLE orders_more_499
                  (
                    LIKE orders INCLUDING ALL
                  );
  CREATE TABLE

  -- добавим ограничения
  test_database=# ALTER TABLE orders_less_499
                  ADD CONSTRAINT less_499
                  CHECK ( price <= 499);
  ALTER TABLE
  test_database=# ALTER TABLE orders_more_499
                  ADD CONSTRAINT more_499
                  CHECK ( price > 499);
  ALTER TABLE

  -- делаем наши новые таблицы секциями таблицы orders
  test_database=# ALTER TABLE orders
                  ATTACH PARTITION orders_less_499
                  FOR VALUES FROM (0) TO (500);
  ALTER TABLE
  test_database=# ALTER TABLE orders
                  ATTACH PARTITION orders_more_499
                  FOR VALUES FROM (500) TO (1000);
  ALTER TABLE

  -- добавляем значения в таблицу orders из таблицы orders_original
  test_database=# INSERT INTO orders
                  SELECT * FROM orders_original;
  INSERT 0 8

  -- проверяем
  test_database=# SELECT * FROM orders;
   id |        title         | price
  ----+----------------------+-------
    1 | War and peace        |   100
    3 | Adventure psql time  |   300
    4 | Server gravity falls |   300
    5 | Log gossips          |   123
    7 | Me and my bash-pet   |   499
    2 | My little database   |   500
    6 | WAL never lies       |   900
    8 | Dbiezdmin            |   501
  (8 rows)

  test_database=# SELECT * FROM orders_less_499;
   id |        title         | price
  ----+----------------------+-------
    1 | War and peace        |   100
    3 | Adventure psql time  |   300
    4 | Server gravity falls |   300
    5 | Log gossips          |   123
    7 | Me and my bash-pet   |   499
  (5 rows)

  test_database=# SELECT * FROM orders_more_499;
   id |       title        | price
  ----+--------------------+-------
    2 | My little database |   500
    6 | WAL never lies     |   900
    8 | Dbiezdmin          |   501
  (3 rows)
  ```

## Задача 4

>  Используя утилиту `pg_dump` создайте бекап БД `test_database`.
>
>  Как бы вы доработали бэкап-файл, чтобы добавить уникальность значения столбца `title` для таблиц `test_database`?

- создадим бэкап базы `test_database`:
  ```bash
  $ pg_dump -U postgres -d test_database > backup/test_database_dump.sql
  ```
- если мы хотим, чтоб столбец `title` имел только уникальные значения, то для него можно определить атрибут `UNIQUE`. Но тут есть одно но. Если мы попытаемся создать уникальный индекс исключительно для поля `title`, то получим ошибку, т.к. каждый уникальный ключ в секционированных таблицах, должен содержать ключ разделения. Поэтому необходимо делать уникальный индекс по двум столбцам. В нашем случае, по `title` и `price`:
  ```sql
  test_database=# CREATE UNIQUE INDEX title_price_unq_idx
                  ON orders (title, price);
  CREATE INDEX

  test_database=# \d+ orders
                                                  Partitioned table "public.orders"
  Column |         Type          | Collation | Nullable |              Default               | Storage  | Stats target | Description
  --------+-----------------------+-----------+----------+------------------------------------+----------+--------------+-------------
  id     | integer               |           | not null | nextval('orders_id_seq'::regclass) | plain    |              |
  title  | character varying(80) |           | not null |                                    | extended |              |
  price  | integer               |           |          |                                    | plain    |              |
  Partition key: RANGE (price)
  Indexes:
  "title_price_unq_idx" UNIQUE, btree (title, price)
  Partitions: orders_less_499 FOR VALUES FROM (0) TO (500),
  orders_more_499 FOR VALUES FROM (500) TO (1000)
  ```
