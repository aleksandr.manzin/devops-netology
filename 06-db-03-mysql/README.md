# Домашнее задание к занятию "6.3. MySQL"

## Задача 1

> Используя docker поднимите инстанс MySQL (версию 8). Данные БД сохраните в volume.
>
> Изучите [бэкап БД](https://github.com/netology-code/virt-homeworks/tree/master/06-db-03-mysql/test_data) и
восстановитесь из него.
>
> Перейдите в управляющую консоль `mysql` внутри контейнера.
>
> Используя команду `\h` получите список управляющих команд.
>
> Найдите команду для выдачи статуса БД и **приведите в ответе** из ее вывода версию сервера БД.
>
> Подключитесь к восстановленной БД и получите список таблиц из этой БД.
>
> **Приведите в ответе** количество записей с `price` > 300.

* для начала создадим файл `docker-compose.yml`, с помощью которого будем поднимать контейнер с `MySQL`:
  ```yaml
  version: '2.9'

  volumes:
    mysql_data:
    mysql_backup:

  services:
    mysql:
      image: mysql:8
      container_name: mysql_db
      restart: always
      environment:
        - MYSQL_ROOT_PASSWORD=netology
      volumes:
        - mysql_data:/var/lib/mysql
        - mysql_backup:/backup
      ports:
        - 3306:3306
  ```
* запускаем наш контейнер:
  ```bash
  $ docker compose up -d
  [+] Running 1/1
   ⠿ Volume "06-db-03-mysql_mysql_data"    Created
   ⠿ Volume "06-db-03-mysql_mysql_backup"  Created
   ⠿ Container mysql_db                    Started   
  $ docker compose ps
  NAME                COMMAND                  SERVICE             STATUS              PORTS
  mysql_db            "docker-entrypoint.s…"   mysql               running             0.0.0.0:3306->3306/tcp, :::3306->3306/tcp
  ```
* теперь нам надо скопировать базу в наш контейнер:
  ```bash
  $ docker cp test_data/test_dump.sql mysql_db:/backup
  ```
* подключаемся к командной оболочке нашего контейнера и переключаемся в командную оболочку `mysql`:
  ```bash
  $ docker exec -it mysql_db bash
  $ mysql --user root -p
  Enter password:
  ```
* создаем базу `test_db`, а также пользователя, который будет иметь доступ к этой базе и выдаем ему права:
  ```sql
  mysql> CREATE DATABASE test_db;
  Query OK, 1 row affected (0.01 sec)

  mysql> USE test_db;
  Database changed

  mysql> CREATE USER aleksandr IDENTIFIED BY 'devops';
  Query OK, 0 rows affected (0.01 sec)

  mysql> GRANT ALL PRIVILEGES ON test_db.* TO aleksandr;
  Query OK, 0 rows affected (0.01 sec)

  mysql> SELECT USER FROM mysql.user;
  +------------------+
  | USER             |
  +------------------+
  | aleksandr        |
  | root             |
  | mysql.infoschema |
  | mysql.session    |
  | mysql.sys        |
  | root             |
  +------------------+
  6 rows in set (0.00 sec)

  mysql> SHOW GRANTS FOR aleksandr;
  +--------------------------------------------------------+
  | Grants for aleksandr@%                                 |
  +--------------------------------------------------------+
  | GRANT USAGE ON *.* TO `aleksandr`@`%`                  |
  | GRANT ALL PRIVILEGES ON `test_db`.* TO `aleksandr`@`%` |
  +--------------------------------------------------------+
  2 rows in set (0.00 sec)

  mysql> \q
  Bye
  ```
* восстанавливаем базу из файла `test_dump.sql`:
  ```bash
  $ mysql -u aleksandr -p test_db < backup/test_dump.sql
  $ mysql -u aleksandr -p test_db
  Enter password:
  ```
* получим статус БД:
  ```sql
  mysql> \s
  --------------
  mysql  Ver 8.0.29 for Linux on x86_64 (MySQL Community Server - GPL)

  Connection id:		8
  Current database:
  Current user:		root@localhost
  SSL:			Not in use
  Current pager:		stdout
  Using outfile:		''
  Using delimiter:	;
  Server version:		8.0.29 MySQL Community Server - GPL
  Protocol version:	10
  Connection:		Localhost via UNIX socket
  Server characterset:	utf8mb4
  Db     characterset:	utf8mb4
  Client characterset:	latin1
  Conn.  characterset:	latin1
  UNIX socket:		/var/run/mysqld/mysqld.sock
  Binary data as:		Hexadecimal
  Uptime:			7 min 18 sec

  Threads: 2  Questions: 15  Slow queries: 0  Opens: 149  Flush tables: 3  Open tables: 68  Queries per second avg: 0.034
  ```
  Как видно из полученной информации, версия сервера MySQL `8.0.29`.
* получаем список таблиц:
  ```sql
  mysql> SHOW TABLES;
  +-------------------+
  | Tables_in_test_db |
  +-------------------+
  | orders            |
  +-------------------+
  1 row in set (0.00 sec)
  ```
* записи с `price > 300`:
  ```sql
  mysql> SELECT * FROM orders WHERE price > 300;
  +----+----------------+-------+
  | id | title          | price |
  +----+----------------+-------+
  |  2 | My little pony |   500 |
  +----+----------------+-------+
  1 row in set (0.00 sec)

  mysql> SELECT COUNT(*) QUANTITY
      -> FROM orders
      -> WHERE price > 300;
  +----------+
  | QUANTITY |
  +----------+
  |        1 |
  +----------+
  1 row in set (0.00 sec)
  ```

## Задача 2

> Создайте пользователя test в БД c паролем test-pass, используя:
>  - плагин авторизации mysql_native_password
>  - срок истечения пароля - 180 дней
>  - количество попыток авторизации - 3
>  - максимальное количество запросов в час - 100
>  - аттрибуты пользователя:
>      - Фамилия "Pretty"
>      - Имя "James"
>
>  Предоставьте привелегии пользователю `test` на операции SELECT базы `test_db`.
>
>  Используя таблицу INFORMATION_SCHEMA.USER_ATTRIBUTES получите данные по пользователю `test` и приведите в ответе к задаче

* создадим пользователя:
  ```sql
  mysql> CREATE USER test
      -> IDENTIFIED WITH mysql_native_password
      -> BY 'test-pass'
      -> WITH MAX_QUERIES_PER_HOUR 100
      -> PASSWORD EXPIRE INTERVAL 180 DAY
      -> FAILED_LOGIN_ATTEMPTS 3
      -> ATTRIBUTE '{"fname": "James", "lname": "Pretty"}';
  Query OK, 0 rows affected (0.01 sec)
  ```
* посмотрим аттрибуты нашего пользователя `test`:
  ```sql
  mysql> SELECT USER,
      -> CONCAT(ATTRIBUTE->>"$.fname"," ",ATTRIBUTE->>"$.lname") AS 'Full Name'
      -> FROM INFORMATION_SCHEMA.USER_ATTRIBUTES
      -> WHERE USER = "test";
  +------+--------------+
  | USER | Full Name    |
  +------+--------------+
  | test | James Pretty |
  +------+--------------+
  1 row in set (0.00 sec)
  ```
## Задача 3

> Установите профилирование `SET profiling = 1`.
> Изучите вывод профилирования команд `SHOW PROFILES;`.
>
> Исследуйте, какой `engine` используется в таблице БД `test_db` и **приведите в ответе**.
>
> Измените `engine` и **приведите время выполнения и запрос на изменения из профайлера в ответе**:
> - на `MyISAM`
> - на `InnoDB`

* `SHOW PROFILES` - профилирование запросов в текущей сессии;
* включим профилирование и сделаем запрос:
  ```sql
  mysql> SET profiling = 1;
  Query OK, 0 rows affected, 1 warning (0.00 sec)

  mysql> SELECT * FROM orders;
  +----+-----------------------+-------+
  | id | title                 | price |
  +----+-----------------------+-------+
  |  1 | War and Peace         |   100 |
  |  2 | My little pony        |   500 |
  |  3 | Adventure mysql times |   300 |
  |  4 | Server gravity falls  |   300 |
  |  5 | Log gossips           |   123 |
  +----+-----------------------+-------+
  5 rows in set (0.00 sec)

  mysql> CREATE TABLE test_table (id INT, name CHAR(5));
  Query OK, 0 rows affected (0.02 sec)

  mysql> SHOW PROFILES;
  +----------+------------+------------------------------------------------+
  | Query_ID | Duration   | Query                                          |
  +----------+------------+------------------------------------------------+
  |        1 | 0.00052050 | SELECT * FROM orders                           |
  |        2 | 0.01842900 | CREATE TABLE test_table (id INT, name CHAR(5)) |
  +----------+------------+------------------------------------------------+
  2 rows in set, 1 warning (0.00 sec)
  ```
  Как видим, при включении профилирования все запросы попадают в таблицу, им присваивается ID, время выпронения запроса и сам запрос.

* вывод команды `SHOW PROFILES` по умолчанию содержит 15 команд. Для установки иного значения используется команда `SET profiling_history_size = 100`, где 100 - максимальное значение;

* проверяем движок, который установлен для таблицы `orders`:
  ```sql
  mysql> SELECT TABLE_NAME, ENGINE FROM information_schema.TABLES WHERE TABLE_SCHEMA="test_db";
  +------------+--------+
  | TABLE_NAME | ENGINE |
  +------------+--------+
  | orders     | InnoDB |
  +------------+--------+
  1 row in set (0.01 sec)

  ```
  Как видим, для таблицы `orders` стоит "движок" `InnoDB`.

* изменим "движок" на `MyISAM` и обратно на `InnoDB`. Произведем несколько запросов:
  ```sql
  -- Сначала изменим движок на MyISAM
  mysql> ALTER TABLE orders ENGINE = MyISAM;
  Query OK, 5 rows affected (0.03 sec)
  Records: 5  Duplicates: 0  Warnings: 0

  -- Выполним несколько команд и посмотрим получившуюся таблицу:
  mysql> SHOW PROFILES;
  +----------+------------+-----------------------------------------------------------------------------------------+
  | Query_ID | Duration   | Query                                                                                   |
  +----------+------------+-----------------------------------------------------------------------------------------+
  |        1 | 0.01902350 | ALTER TABLE orders ENGINE = MyISAM                                                      |
  |        2 | 0.00160575 | SELECT TABLE_NAME, ENGINE FROM information_schema.TABLES WHERE TABLE_SCHEMA = "test_db" |
  |        3 | 0.00042325 | SELECT * FROM orders                                                                    |
  |        4 | 0.00035475 | SELECT COUNT(*) FROM orders WHERE price > 300                                           |
  +----------+------------+-----------------------------------------------------------------------------------------+
  4 rows in set, 1 warning (0.00 sec)

  -- А теперь изменим движок обратно на InnoDB
  mysql> ALTER TABLE orders ENGINE = InnoDB;
  Query OK, 5 rows affected (0.04 sec)
  Records: 5  Duplicates: 0  Warnings: 0

  -- Выполним несколько команд и посмотрим получившуюся таблицу:
  mysql> SHOW PROFILES;
  +----------+------------+-----------------------------------------------------------------------------------------+
  | Query_ID | Duration   | Query                                                                                   |
  +----------+------------+-----------------------------------------------------------------------------------------+
  |        1 | 0.03419575 | ALTER TABLE orders ENGINE = InnoDB                                                      |
  |        2 | 0.00306225 | SELECT TABLE_NAME, ENGINE FROM information_schema.TABLES WHERE TABLE_SCHEMA = "test_db" |
  |        3 | 0.00056725 | SELECT * FROM orders                                                                    |
  |        4 | 0.00050275 | SELECT COUNT(*) FROM orders WHERE price > 300                                           |
  +----------+------------+-----------------------------------------------------------------------------------------+
  4 rows in set, 1 warning (0.00 sec)
  ```
  Как видим, в `engine InnoDB` время выполнения всех запросов выше.

## Задача 4

>  Изучите файл `my.cnf` в директории /etc/mysql.
>
>  Измените его согласно ТЗ (движок InnoDB):
>  - Скорость IO важнее сохранности данных
>  - Нужна компрессия таблиц для экономии места на диске
>  - Размер буффера с незакомиченными транзакциями 1 Мб
>  - Буффер кеширования 30% от ОЗУ
>  - Размер файла логов операций 100 Мб
>
>  Приведите в ответе измененный файл `my.cnf`.

* файл `my.cnf`, в моем случае, находится в `/etc/`
* отредактируем файл согласно условия:
  ```
  [mysqld]
  skip-host-cache
  skip-name-resolve
  datadir=/var/lib/mysql
  socket=/var/run/mysqld/mysqld.sock
  secure-file-priv=/var/lib/mysql-files
  user=mysql

  # скорость IO важнее сохранности данных:
  innodb_flush_log_at_trx_commit = 2

  # компрессия таблиц для экономии места на диске:
  innodb_file_per_table = 1

  # размер буффера с незакомиченными транзакциями 1Мб
  innodb_log_buffer_size = 1M

  # буфер кэширования 30% от ОЗУ (30% от 12Гб):
  innodb_buffer_pool_size = 3,6G

  # размер файла логов операций 100Мб
  innodb_log_file_size = 100M

  pid-file=/var/run/mysqld/mysqld.pid
  [client]
  socket=/var/run/mysqld/mysqld.sock

  !includedir /etc/mysql/conf.d/
  ```
