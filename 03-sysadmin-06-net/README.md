# Домашнее задание к занятию "3.6. Компьютерные сети, лекция 1"

1. 
    ```
    vagrant@vagrant:~$ telnet stackoverflow.com 80
    Trying 151.101.65.69...
    Connected to stackoverflow.com.
    Escape character is '^]'.
    GET /questions HTTP/1.0
    HOST: stackoverflow.com
    
    HTTP/1.1 301 Moved Permanently
    cache-control: no-cache, no-store, must-revalidate
    location: https://stackoverflow.com/questions
    x-request-guid: b6231f89-fb82-42e5-a508-3de22faafeb4
    feature-policy: microphone 'none'; speaker 'none'
    content-security-policy: upgrade-insecure-requests; frame-ancestors 'self' https://stackexchange.com
    Accept-Ranges: bytes
    Date: Mon, 14 Feb 2022 17:07:32 GMT
    Via: 1.1 varnish
    Connection: close
    X-Served-By: cache-hhn4041-HHN
    X-Cache: MISS
    X-Cache-Hits: 0
    X-Timer: S1644858453.893047,VS0,VE78
    Vary: Fastly-SSL
    X-DNS-Prefetch-Control: off
    Set-Cookie: prov=daed09ee-86d0-63a8-6328-a36276e3f0e9; domain=.stackoverflow.com; expires=Fri, 01-Jan-2055 00:00:00 GMT; path=/; HttpOnly
    
    Connection closed by foreign host.
    ```
    Мы получаем код ответа `301 Moved Permanently`, который говорит, что запрошенный ресурс был перемещен на постоянной основе на новый URI(Uniform Resource Identifier), который указан в поле заголовка `Locataion` - `https://stackoverflow.com/questions`
2.    
    ```
    Request URL: http://stackoverflow.com/
    Request Method: GET
    Status Code: 307 Internal Redirect
    Referrer Policy: strict-origin-when-cross-origin
    Location: https://stackoverflow.com/
    Non-Authoritative-Reason: HSTS
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
    Upgrade-Insecure-Requests: 1
    User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.82 Safari/537.36
    ```
    Код HTTP-ответа - `307 Internal Redirect` - перенаправление на ресурс `https://stackoverflow.com/`. Дольше всего обрабатывался запрос `https://stackoverflow.com` - 281 ms
    ![Chrome DevTools](screenshots/console.png "F12-Network-Headers")
3. Мой IP-адрес: 188.243.161.167
4. 
    ```bash
    vagrant@vagrant:~$ whois 188.243.161.167 | grep -Ei 'inetnum|netname|descr|address|origin|route|created'
    inetnum:        188.243.128.0 - 188.243.255.255
    netname:        SKYNET
    descr:          SkyNet Network
    mnt-routes:     MNT-SKNT
    created:        2014-09-30T14:25:55Z
    address:        SkyNet LLC
    address:        192239 St. Petersburg
    address:        Russian Federation
    created:        2008-04-21T16:28:30Z
    route:          188.243.128.0/18
    descr:          SkyNet Networks
    origin:         AS35807
    created:        2009-07-06T09:26:47Z
    ```
    Провайдер - SKYNET, Автономная система - AS35807
5. воспользуемся traceroute с ключами: `-I` - для ICMP пакетов(по умолчанию UDP); `-A` - сопоставление AS; `-n` - не сопостовлять IP-адреса с именами узлов: 
    ```bash
    vagrant@vagrant:~$ traceroute -IAn 8.8.8.8
    traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
     1  10.0.2.2 [*]  0.149 ms  0.100 ms  0.072 ms
     2  192.168.100.1 [*]  0.715 ms  0.709 ms  0.633 ms
     3  * * *
     4  93.100.0.61 [AS35807]  0.982 ms  1.132 ms  1.127 ms
     5  93.100.0.14 [AS35807]  1.122 ms  1.116 ms  1.088 ms
     6  185.37.128.144 [AS35807]  1.273 ms  1.576 ms  1.560 ms
     7  185.37.128.161 [AS35807]  1.349 ms  1.342 ms  1.671 ms
     8  72.14.216.110 [AS15169]  1.635 ms  1.808 ms  1.803 ms
     9  172.253.76.91 [AS15169]  1.618 ms  1.791 ms  1.785 ms
    10  74.125.244.180 [AS15169]  2.006 ms  1.958 ms  1.918 ms
    11  142.251.61.219 [AS15169]  6.056 ms  6.240 ms  5.297 ms
    12  172.253.64.53 [AS15169]  7.467 ms  7.591 ms  7.050 ms
    13  * * *
    14  * * *
    15  * * *
    16  * * *
    17  * * *
    18  * * *
    19  * * *
    20  * * *
    21  * * *
    22  * * *
    23  * * *
    24  8.8.8.8 [AS15169]  4.462 ms  4.637 ms  4.593 ms
    ```
    Таким образом, видим, что пакет отправляется на шлюз виртуальной машины (10.0.2.2), затем на роутер (192.168.100.1), затем попадает в автономную сеть`AS35807`, которая принадлежит провайдеру SkyNet, и проходит узлы провайдера, после чего попадает в `AS15169`, которая принадлежит Google:
    ```bash
    vagrant@vagrant:~$ whois -h whois.radb.net AS15169 | grep -Ei 'descr'
    descr:      Google, Inc
    ``` 
6. используем утилиту `mtr` со следующими ключами: `-z` - сопоставление AS; `-r` - вывод отчета без сопоставление IP-адресов с именами узлов; `-c` - количество отправленных пакетов:
    ```bash
    vagrant@vagrant:~$ mtr -zrc 10 8.8.8.8
    Start: 2022-02-14T22:49:04+0300
    HOST: vagrant                     Loss%   Snt   Last   Avg  Best  Wrst StDev
      1. AS???    _gateway             0.0%    10    0.2   0.2   0.1   0.3   0.0
      2. AS???    192.168.100.1        0.0%    10    0.8   0.8   0.6   1.6   0.3
      3. AS???    ???                 100.0    10    0.0   0.0   0.0   0.0   0.0
      4. AS35807  router.sknt.ru       0.0%    10    3.7   5.3   0.9  17.6   5.6
      5. AS35807  r20-core             0.0%    10    1.1   4.0   1.0  18.3   5.9
      6. AS35807  k12-core             0.0%    10    1.5   5.3   1.1  14.3   4.8
      7. AS35807  sel-core             0.0%    10    6.1   5.0   1.4  11.6   4.0
      8. AS15169  72.14.216.110        0.0%    10    2.3   5.0   1.4  16.1   4.6
      9. AS15169  172.253.76.91        0.0%    10    1.4   3.7   1.3  12.4   3.6
     10. AS15169  74.125.244.180       0.0%    10    5.9   6.0   1.5  10.3   3.3
     11. AS15169  142.251.61.219       0.0%    10   13.0  11.6   5.8  16.7   3.8
     12. AS15169  172.253.64.53        0.0%    10   15.0  13.3   6.6  27.7   6.7
     13. AS???    ???                 100.0    10    0.0   0.0   0.0   0.0   0.0
     14. AS???    ???                 100.0    10    0.0   0.0   0.0   0.0   0.0
     15. AS???    ???                 100.0    10    0.0   0.0   0.0   0.0   0.0
     16. AS???    ???                 100.0    10    0.0   0.0   0.0   0.0   0.0
     17. AS???    ???                 100.0    10    0.0   0.0   0.0   0.0   0.0
     18. AS???    ???                 100.0    10    0.0   0.0   0.0   0.0   0.0
     19. AS???    ???                 100.0    10    0.0   0.0   0.0   0.0   0.0
     20. AS???    ???                 100.0    10    0.0   0.0   0.0   0.0   0.0
     21. AS15169  dns.google          90.0%    10    4.8   4.8   4.8   4.8   0.0
    ```
    Максимальное время задержки(`Wrst`) на 12 "прыжке" и составляет 27.7. Среднее время задержки(`Avg`) также самое высокое на 12 "прыжке"
7. `dns.google` - DNS-сервера и A-записи:
    ```bash
    vagrant@vagrant:~$ whois +trace dns.google | grep 'dns.google'
    ...
    ; <<>> DiG 9.16.1-Ubuntu <<>> +trace dns.google
    ; 
    dns.google.             10800   IN      NS      ns4.zdns.google.
    dns.google.             10800   IN      NS      ns2.zdns.google.
    dns.google.             10800   IN      NS      ns1.zdns.google.
    dns.google.             10800   IN      NS      ns3.zdns.google.
    
    # A-записи...
    dns.google.             900     IN      A       8.8.4.4
    dns.google.             900     IN      A       8.8.8.8
    ```
8. PTR-записи для адресов `8.8.8.8` и `4.4.4.4`:
    ```bash
    vagrant@vagrant:~$ dig -x 8.8.8.8 | grep PTR
    ;8.8.8.8.in-addr.arpa.          IN      PTR
    8.8.8.8.in-addr.arpa.   3026    IN      PTR     dns.google.
    vagrant@vagrant:~$ dig -x 8.8.4.4 | grep PTR
    ;4.4.8.8.in-addr.arpa.          IN      PTR
    4.4.8.8.in-addr.arpa.   3140    IN      PTR     dns.google.
    ```
    Таким образом, видим, что к IP-адрес привязано доменное имя `dns.google`:
    ```bash
    vagrant@vagrant:~$ dig dns.google
    
    ; <<>> DiG 9.16.1-Ubuntu <<>> dns.google
    ;; global options: +cmd
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 19914
    ;; flags: qr rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1
    
    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags:; udp: 65494
    ;; QUESTION SECTION:
    ;dns.google.                    IN      A
    
    ;; ANSWER SECTION:
    dns.google.             8       IN      A       8.8.4.4
    dns.google.             8       IN      A       8.8.8.8
    
    ;; Query time: 0 msec
    ;; SERVER: 127.0.0.53#53(127.0.0.53)
    ;; WHEN: Mon Feb 14 23:22:38 MSK 2022
    ;; MSG SIZE  rcvd: 71
    
    ```
