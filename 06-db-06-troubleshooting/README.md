# Домашнее задание к занятию "6.6. Troubleshooting"

## Задача 1

> Перед выполнением задания ознакомьтесь с документацией по [администрированию MongoDB](https://docs.mongodb.com/manual/administration/).
>
> Пользователь (разработчик) написал в канал поддержки, что у него уже 3 минуты происходит CRUD операция в MongoDB и её
нужно прервать.
>
> Вы как инженер поддержки решили произвести данную операцию:
> - напишите список операций, которые вы будете производить для остановки запроса пользователя
> - предложите вариант решения проблемы с долгими (зависающими) запросами в MongoDB

- Список операций для остановки запроса:

  - первоначально нам необходимо вычислить проблемные запросы, для этого необходимо сделать вызов команды `db.currentOp()`, которая выведет список текущих запросов, а в совокупности с `secs_running` выведет продолжительность этих запросов:

    ```sql
    db.currentOp(
      {
        "secs_running": { $gte: 3 }
      }
    )
    ```

    В данном примере, команда вернет нам список выполняемых операций, выполнение которых занимает более 3 секунд. Однако, для сокращения вывода и нахождения неудачного запроса, следует варьировать значения.

  - когда мы найдем тот самый неудачный запрос - его можно "убить", используя команду `db.killOp(opid)`.

- решение проблем с долгими(зависающими) запросами:

  - можно включить `Database Profiler`. Профилировщик собирает подробные данные о запросах в `MongoDB`, длина которых превышает определенных порог в милисекундах, при котором профилировщик считает запрос медленным. Для этого нужно ввести команду `db.SetProfilingLevel(1, 3000)`, где `1` - это включение профилировщика для БД, к которой подключены, а `3000` - порог, превышение которого будет считаться медленным запросом (`slowms`). Важными данным в профайлере будут `keysExamined, docsExamined` и др., с помощью которых можно понять причину медленного запроса

  - также можно провести анализ запросов, используя `explain("executionStats")` для понимания, что же повлияло на столь медленный запрос.

  - требуется использовать правильные индексы для эффективного поиска по документам. Например, если существующие индексы не оптимальны, то `MongoDB` потребуется изучить дополнительные документы. Соответственно, если индексы не оптимальны, то их необходимо заменить на оптимальные.

## Задача 2

> Перед выполнением задания познакомьтесь с документацией по [Redis latency troobleshooting](https://redis.io/topics/latency).
>
> Вы запустили инстанс Redis для использования совместно с сервисом, который использует механизм TTL. Причем отношение количества записанных key-value значений к количеству истёкших значений есть величина постоянная и увеличивается пропорционально количеству реплик сервиса.
>
> При масштабировании сервиса до N реплик вы увидели, что:
> - сначала рост отношения записанных значений к истекшим
> - Redis блокирует операции записи
>
> Как вы думаете, в чем может быть проблема?

- возможно, причиной такого поведения является недостаток памяти. Параметр `maxmemory` задаёт максимально возможное количество потребляемой памяти. При достижении указанного значения система попытается высвободить память, руководствуясь алгоритмом, указанным в `maxmemory-policy`, который отвечает за вытеснение ключей/значений. Возможно, вытеснение отключено (`noeviction`), а значит будет переполняться память ключами, у которых истёк срок действия.

## Задача 3

> Перед выполнением задания познакомьтесь с документацией по [Common Mysql errors](https://dev.mysql.com/doc/refman/8.0/en/common-errors.html).
>
> Вы подняли базу данных MySQL для использования в гис-системе. При росте количества записей, в таблицах базы, пользователи начали жаловаться на ошибки вида:
> ```python
>   InterfaceError: (InterfaceError) 2013: Lost connection to MySQL server during query u'SELECT..... '
>   ```
> Как вы думаете, почему это начало происходить и как локализовать проблему?
>
> Какие пути решения данной проблемы вы можете предложить?

- первоначально необходимо изучить логи `/var/lib/mysql/`, скорее всего, ответ удастся найти в них.

- в документации к `MySQL` дана исчерпывающая информация по данной ошибке(`Lost connection to MySQL server`), в которой указаны возможные варианты появления данной ошибки:

  - плохое сетевое соединение. Следует убедиться, что используется стабильное соединение;

  - иногда `during query`  возникает, когда миллионы строк отправляются как часть одного или нескольких запросов. Если это действительно происходит, то следует увеличить значение `net_read_timeout` с 30 секунд, которые установлены по умолчанию до 60 или более;

  - реже это может происходить, когда клиент пытается установить начальное соединение с сервером. В этом случае, проблема решается увеличением значения `connect_timeout`;

  - значение `BLOB` больше, чем `max_allowed_packet`. В этом случае, необходимо установить более высокое значение для `max_allowed_packet` в файле конфигурации `/etc/my.cnf`. Если файл конфигурации недоступен, то это значение можно установить с помощью команды `SET GLOBAL max_allowed_packet=value`.

## Задача 4

> Перед выполнением задания ознакомтесь со статьей [Common PostgreSQL errors](https://www.percona.com/blog/2020/06/05/10-common-postgresql-errors/) из блога Percona.
>
> Вы решили перевести гис-систему из задачи 3 на PostgreSQL, так как прочитали в документации, что эта СУБД работает с большим объемом данных лучше, чем MySQL.
>
> После запуска пользователи начали жаловаться, что СУБД время от времени становится недоступной. В dmesg вы видите, что:
>
> `postmaster invoked oom-killer`
>
> Как вы думаете, что происходит?
>
> Как бы вы решили данную проблему?

- проблема:

  - ситуация такова, что в системе нет памяти. Если вдаваться в подробности, то ОС резервирует память за каждым процессом, но фактически не выделяет её. Память выделяется только в тот момент, когда процесс действительно собирается использовать её. Иногда происходит, что ОС резервирует память, но в момент, когда она действительно нужна, свободной памяти нет, тогда происходит сбой системы;

  - однако, существует `Out_Of_Memory Killer`, который завершает процессы, чтоб уберечь ядро от сбоев.

- пути решения:

  - первоначально я бы порекомендовал увеличить память на сервере. Однако, если это невозможно, то можно понизить приоритет на завершение процесса:

    - узнаем `pid` нашего процесса `PostgreSQL` либо через `ps`, либо через оболочку `PostgreSQL`. Например:

      ```sql
      postgres=# SELECT pg_backend_pid();
      pg_backend_pid
      ----------------
          14259
      (1 row)
      ```

    - зная `pid`, мы можем посмотреть `oom_score`, который назначается каждому процессу. Это значение умножается на объём памяти. Соответственно, чем выше значение у процесса, тем больше шансов стать "жертвой" `oom-killer`:

      ```bash
      $ sudo cat /proc/14259/oom_score
      2
      ```

    - этот показатель можно уменьшить, если процесс связать с привилегированным пользователем, т.к. такие процессы имеют более низкую оценку `oom_score`, но это не означает, что процесс не может быть завершен `oom-killer`'ом;

    - если мы хотим, чтоб наш процесс `PostgreSQL` никогда не завершался, то мы можем изменить параметр `oom_score_adj` для нашего процесса с `pid 14259`, для этого можем, к примеру, передать значение `-100`:

      ```bash
      $ sudo echo -100 > /proc/14259/oom_score_adj
      ```

    - либо то же самое можно сделать в блоке `[Service]` соответствующей службы:

      ```bash
      [Service]
      OOMScoreAdjust=-1000
      ```

  - можно и отключить `oom-killer`, но делать этого не рекомендуется;

  - в ядре существует переменная `vm.overcommit_memory`, для которой можно указать значение `2`, и тогда ядро не будет резервировать больше памяти, чем указано в параметре `overcommit_ratio`.

  - можно увеличить параметр `/proc/sys/vm/swappiness`, по умолчанию стоит значение 60. Чем выше значение, тем меньше вероятность, что `oom-killer` завершит процесс.

---
### Рекомендации преподавателя

По 2-му вопросу. Проблема скорей всего в том что вся память занята истекшими в один и тот же момент ключами которые еще не удаленны. Так как Redis использует в основном однопоточную конструкцию, поэтому все запросы обслуживаются последовательно, в связи с этим пока не выполнится очистка, все операции записи блокируются.

По 4-му вопросу. Можно поиграть с параметрами, которые регулируют память в Postgres из основных это: `max_connections`, `shared_buffer`, `work_mem`, `effective_cache_size`, `maintenance_work_mem`.

`shared_buffer` - этот параметр устанавливает, сколько выделенной памяти будет использоваться PostgreSQL для кеширования.

`wal_buffers` - PostgreSQL сначала записывает записи в WAL (журнал пред записи) в буферы, а затем эти буферы сбрасываются на диск. Размер буфера по умолчанию, определенный wal_buffers, составляет 16 МБ. Но если у нас много одновременных подключений, то более высокое значение может повысить производительность.

`effective_cache_size` - предоставляет оценку памяти, доступной для кэширования диска. Это всего лишь ориентир, а не точный объем выделенной памяти или кеша. Он не выделяет фактическую память, но сообщает оптимизатору объем кеша, доступный в ядре. Если значение этого параметра установлено слишком низким, планировщик запросов может принять решение не использовать некоторые индексы, даже если они будут полезны. Поэтому установка большого значения всегда имеет смысл.

`work_mem` - если нам нужно выполнить сложную сортировку, увеличьте значение work_mem для получения хороших результатов. Сортировка в памяти происходит намного быстрее, чем сортировка данных на диске. Установка очень высокого значения может стать причиной узкого места в памяти для нашей среды, поскольку этот параметр относится к операции сортировки пользователя.

`maintenance_work_mem` - это параметр памяти, используемый для задач обслуживания.
