# Домашнее задание к занятию "4.3. Языки разметки JSON и YAML"
___

### Задача №1

> Мы выгрузили JSON, который получили через API запрос к нашему сервису:

```
{ "info" : "Sample JSON output from our service\t",
    "elements" :[
        { "name" : "first",
        "type" : "server",
        "ip" : 7175 
        },
        { "name" : "second",
        "type" : "proxy",
        "ip : 71.78.22.43
        }
    ]
}

```

>  Нужно найти и исправить все ошибки, которые допускает наш сервис


```
{
	"info": "Sample JSON output from our service\t",
	"elements": [{
			"name": "first",
			"type": "server",
			"ip": 7175
		},
		{
			"name": "second",
			"type": "proxy",
			"ip": "71.78.22.43"
		}
	]
}

```
___

### Задача №2

> В прошлый рабочий день мы создавали скрипт, позволяющий опрашивать веб-сервисы и получать их IP. К уже реализованному функционалу нам нужно добавить возможность записи JSON и YAML файлов, описывающих наши сервисы. Формат записи JSON по одному сервису: { "имя сервиса" : "его IP"}. Формат записи YAML по одному сервису: - имя сервиса: его IP. Если в момент исполнения скрипта меняется IP у сервиса - он должен так же поменяться в yml и json файле.

```python
#!/usr/bin/env python3
import socket
import json
import yaml
import time
import datetime as dt

service = {
    'drive.google.com': '0.0.0.0',
    'mail.google.com': '0.0.0.0',
    'google.com': '0.0.0.0'
}


while True:
    for host_name, host_ip in service.items():
        ip = socket.gethostbyname(host_name)
        with open('service.log', 'a') as log:
            log.write(f'{host_name} - {ip}\n')

        timestamp = dt.datetime.now().strftime("%H:%M:%S")
        time.sleep(1)
        if service[host_name] == ip:
            print(f'{timestamp} {host_name} - {host_ip}')
        else:
            print(f'{timestamp} [ERROR] {host_name} IP mismatch: {service[host_name]} {ip}')
            with open('error.log', 'a') as error:
                error.write(f'{timestamp} [ERROR] {host_name} IP mismatch: {service[host_name]} {ip}\n')

        to_json = {host_name: ip}
        to_yml = [{host_name: ip}]
        with open('service.json', 'w') as js:
            json.dump(to_json, js)
        with open('service.yaml', 'w') as ym:
            yaml.dump(to_yml, ym)

    with open('service.log', 'r') as log:
        data = log.read()
        for line in data.splitlines():
            host, ip = line.split(' - ')
            service[host] = ip
```

Проверим скрипт:

```bash
$ cat service.json
{"drive.google.com": "74.125.131.194"}
$ cat service.yaml
- drive.google.com: 74.125.131.194
```

