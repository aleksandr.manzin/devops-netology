# Домашнее задание к занятию "3.4. Операционные системы, лекция 2"
1. Начнем по порядку:
    * скачал свежую версию `Node Exporter` с [github](https://github.com/prometheus/node_exporter/releases) и распаковал:
       ```bash
       $ wget https://github.com/prometheus/node_exporter/releases/download/v1.3.1/node_exporter-1.3.1.linux-amd64.tar.gz
       $ tar xvfz node_exporter-1.3.1.linux-amd64.tar.gz
       ```
    * переходим в директорию `node_exporter-*`, и копируем бинарный файл в папку `/usr/local/bin`:
       ```bash
       $ cd node_exporter-1.3.1.linux-amd64
       $ cp node_exporter /usr/local/bin
       ```
    * создадим пользователя, от имени которого будет запускаться сервис `node_exporter` без создания домашней директории и без возможности входа в систему:
       ```bash
       $ useradd --no-create-home --shell /bin/false node_exporter
       ```
    * теперь создадим юнит-файл для нашего `node_exporter`:
       ```bash
       $ cd /etc/systemd/system
       $ sudo nano node_exporter.service
       ```
    * и запишем в него описание; пользователя и группу, которую мы создали и от чьего имени, соответственно, будет запускаться; путь к исполняемому файлу; вариант перезагрузки сервиса (в данном случае, поставил `always` - сервис будет перезапущен независимо от причины её завершения, будь то сигнал, тайм-аут или чистое завершение); а также указан запуск в многопользовательском режиме без графики (`multi-user.target`):
       ```
       [Unit]
       Description=Prometheus Node Exporter
       
       [Service]
       Type=Simple
       User=node_exporter
       Group=node_exporter
       Environment=/etc/default/node_exporter               # создал файл node_exporter в папке /etc/default и записал в него переменную node_help="--help"
       ExecStart=/usr/local/bin/node_exporter $node_help 
       Restart=always
       
       [Install]
       WantedBy=multi-user.target
       ```
    * запускаем `Node Exporter`:
       ```bash
       $ systemctl daemon-reload        # перечитываем юнит-файлы systemd
       $ systemctl start node_exporter  # запускаем сервис node_exporter
       ```
    * проверяем, запускается/завершается ли работа сервиса:
       ```bash
       $ systemctl status node_exporter # видим, что сервис работает: Active: active (running)
       $ systemctl stop node_exporter   # останавливаем работу сервиса: Active: inactive (dead)
       $ systemctl start node_exporter  # снова запускаем сервис: Active: active (running)
       ```
    * прописываем в автозапуск:
       ```bash
       $ systemctl enable node_exporter
       ```
    * перезагружаем виртуальную машину и смотрим статус нашей службы: `Active: active (running)`;
    * пытаем отправить сигнал, предварительно посмотрев PID процесса (в принципе, `systemctl status node_exporter` нам его показывает, поэтому нет нужды смотреть PID через `ps`):
         ```bash
         $ sudo kill 2203
         $ systemctl status node_exporter # видим, что сервис перезапустился и получил новый PID 2238
         ```

2. 
   * cpu - собираем интересующую нас статистику по каждому ядру (собирается из `/proc/stat`):
     ```
     node_cpu_seconds_total{cpu="0",mode="idle"}             # время, которое CPU ничего не делал
     node_cpu_seconds_total{cpu="0",mode="system"}           # использование ядра системой
     node_cpu_seconds_total{cpu="0",mode="user"}             # использование ядра пользователем
     ```
   * memory:
     ```
     node_memory_MemAvailable_bytes                          # MemAvailable_bytes
     node_memory_MemFree_bytes                               # MemFree_bytes
     node_memory_MemTotal_bytes                              # MemTotal_bytes
     ```
    * disk (метрики собираются по аналогии с утилитой `iostat`):
     ```
     node_disk_io_now{device="sda"}                          # количество выполняемых операций ввода-вывода
     node_disk_io_time_seconds_total{device="sda"}           # %util - процент утилизации
     node_disk_reads_completed_total{device="sda"}           # r/s - количество запросов чтения в секунду
     node_disk_reads_merged_total{device="sda"}              # rrqm/s - обобщённое количество запросов на чтение в секунду
     node_disk_read_bytes_total{device="sda"}                # rkB/s - количество прочитанных байт в секунду
     node_disk_writes_completed_total{device="sda"}          # w/s - количество запросов записи в секунду
     node_disk_writes_merged_total{device="sda"}             # wrqm/s - обобщённое количество запросов на запись в секунду
     node_disk_written_bytes_total{device="sda"}             # wkB/s - количество записанных байт в секунду
     node_disk_io_time_weighted_seconds_total{device="sda"}  # avgqu-sz - средняя длина очереди запросов, отправленных устройству
     ```
    * network: 
     ```
     node_network_receive_bytes_total{device="eth0"}         # статистика receive_bytes
     node_network_receive_drop_total{device="eth0"}          # статистика receive_drop
     node_network_receive_errs_total{device="eth0"}          # статистика receive_errs
     node_network_receive_packets_total{device="eth0"}       # статистика receive_packets
     node_network_transmit_bytes_total{device="eth0"}        # статистика transmit_bytes
     node_network_transmit_colls_total{device="eth0"}        # статистика transmit_colls
     node_network_transmit_drop_total{device="eth0"}         # статистика transmit_drop
     node_network_transmit_errs_total{device="eth0"}         # статистика transmit_errs
     node_network_transmit_packets_total{device="eth0"}      # статистика transmit_packets
     node_network_up{device="eth0"}                          # 1 - включено, 0 - выключено
     ```
3. * установил `Netdata`: `sudo apt install netdata`;
   * в конфигурационный файл `/etc/netdata/netdata.conf` добавил раздел `[web]` и прописал строчку `bind to = 0.0.0.0`;
   * в файле `VagrantFile` добавил строчку `config.vm.network "forwarded_port", guest: 19999, host: 19999`;
   * перезагрузил vagrant: `vagrant reload`;
   * зашел на веб-морду и ознакомился с метриками, представленными `Netdata`.

4. по выводу `dmesg` понятно, что используется система виртуализации:
    ```bash
    $ dmesg | grep -i virtual
    [    0.000000] DMI: innotek GmbH VirtualBox/VirtualBox, BIOS VirtualBox 12/01/2006
    [    0.003165] CPU MTRRs all blank - virtualized system.
    [    0.096175] Booting paravirtualized kernel on KVM
    [    2.438121] systemd[1]: Detected virtualization oracle.
    ```
    ```bash
    $ dmesg | grep -i vbox
    [    0.088518] ACPI: RSDP 0x00000000000E0000 000024 (v02 VBOX  )
    [    0.088522] ACPI: XSDT 0x000000007FFF0030 00003C (v01 VBOX   VBOXXSDT 00000001 ASL  00000061)
    [    0.088526] ACPI: FACP 0x000000007FFF00F0 0000F4 (v04 VBOX   VBOXFACP 00000001 ASL  00000061)
    [    0.088531] ACPI: DSDT 0x000000007FFF0470 002325 (v02 VBOX   VBOXBIOS 00000002 INTL 20100528)
    [    0.088539] ACPI: APIC 0x000000007FFF0240 00005C (v02 VBOX   VBOXAPIC 00000001 ASL  00000061)
    [    0.088541] ACPI: SSDT 0x000000007FFF02A0 0001CC (v01 VBOX   VBOXCPUT 00000002 INTL 20100528)
    [    1.016764] vboxvideo: loading out-of-tree module taints kernel.
    [    1.021582] vboxvideo: module verification failed: signature and/or required key missing - tainting kernel
    [    1.022933] vboxvideo: loading version 6.1.30 r148432
    [    1.033836] fbcon: vboxvideodrmfb (fb0) is primary device
    [    1.046523] vboxvideo 0000:00:02.0: fb0: vboxvideodrmfb frame buffer device
    [    1.056770] [drm] Initialized vboxvideo 1.0.0 20130823 for 0000:00:02.0 on minor 0
    [    1.342971] ata3.00: ATA-6: VBOX HARDDISK, 1.0, max UDMA/133
    [    1.343293] scsi 2:0:0:0: Direct-Access     ATA      VBOX HARDDISK    1.0  PQ: 0 ANSI: 5
    [    3.861546] vboxguest: Successfully loaded version 6.1.30 r148432
    [    3.861571] vboxguest: misc device minor 58, IRQ 20, I/O port d020, MMIO at 00000000f0400000 (size 0x400000)
    [    3.861572] vboxguest: Successfully loaded version 6.1.30 r148432 (interface 0x00010004)
    [    7.917699] vboxsf: g_fHostFeatures=0x8000000f g_fSfFeatures=0x1 g_uSfLastFunction=29
    [    7.917815] *** VALIDATE vboxsf ***
    [    7.917818] vboxsf: Successfully loaded version 6.1.30 r148432
    [    7.917839] vboxsf: Successfully loaded version 6.1.30 r148432 on 5.4.0-91-generic SMP mod_unload modversions  (LINUX_VERSION_CODE=0x50497)
    [    7.918713] vboxsf: SHFL_FN_MAP_FOLDER failed for '/vagrant': share not found
    [   10.381215] 09:35:12.129180 main     VBoxService 6.1.30 r148432 (verbosity: 0) linux.amd64 (Nov 22 2021 16:16:32) release log
    [   10.381321] 09:35:12.129314 main     Executable: /opt/VBoxGuestAdditions-6.1.30/sbin/VBoxService
    ```
5. 
    * `fs.nr_open` - максимальное допустимое количество открытых файловых дескрипторов на уровне системы;
    * `ulimit` - используется для ограничения доступа пользователей системы к ресурсам оболочки:  
        * `ulimit -n` - максимальное количество одновременно открытых файловых дескрипторов в одном процессе (ограничено максимальным значением `fs.nr_open`);
        * `ulimit -Hn` - количество файловых дескрипторов, которые система может открывать одновременно (ограничено максимальным значением `fs.nr_open`);
    * чтоб увеличить значение `fs.nr_open`, можно сделать следующее (изменения не персистентны и при перезагрузке системы вернутся к дефолтным):
        * воспользоваться утилитой `ulimit`: 
        * `ulimit -n 2048` - задаст значение в 2048, которое позволит открывать одновременно до 2048 файловых дескрипторов в одном процессе;
        * `ulimit -Hn 1048576`- задаст ограничение на уровне системы (по умолчанию `1048576`);
        * заданные значения через `ulimit` будут доступны исключительно из текущего `tty`, т.е. в соседнем `tty` будут дефолтные значения;
        * отредактировать файл `/proc/sys/fs/nr_open`;
        * воспользоваться командой `sysctl -w fs.nr_open=1048576`;
        * значение `2147483584` максимально допустимое, определено на уровне ядра;
    * для внесения персистентных изменений: 
        * `fs.nr_open` - необходимо изменить файл `/etc/sysctl.conf`, добавив в него соответствующую строчку: `fs.nr_open=2147483584`;
        * `ulimit` -  необходимо изменить файл `/etc/security/limits.conf`, добавив в него строчки:
            ```
            *    soft    nofile    2048
            *    hard    nofile    2147483584
            ```
        * перезагружаем систему и проверяем (если бы вносили изменения только в `sysctl.conf`, то можно было бы просто перечитать конфиги с помощью `sysctl -p`):
            ```bash
            $ sysctl -n fs.nr_open
            2147483584
            $ cat /proc/sys/fs/nr_open
            2147483584
            $ ulimit -n
            2048
            $ ulimit -Hn
            2147483584
            ```
6. 
    ```bash
    $ which sleep
    /usr/bin/sleep
    $ screen -S sleepSession
    $ sudo -i
    $ unshare --fork --pid --mount-proc /usr/bin/sleep 1h &
    [1] 2310
    # CTRL+a d - сворачиваем работу screen
    $ sudo -i
    $ ps aux | grep sleep
    root        2311  0.0  0.0   5476   592 pts/5    S    20:13   0:00 /usr/bin/sleep 1h
    $ nsenter --target 2311 --pid --mount
    $ ps aux
    USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
    root           1  0.0  0.0   5476   592 pts/5    S+   20:13   0:00 /usr/bin/sleep 1h
    root          26  0.0  0.2   7236  4132 pts/2    S    20:18   0:00 -bash
    root          37  0.0  0.1   8892  3336 pts/2    R+   20:18   0:00 ps uax
    ``` 
7. 
    * `:(){ :|:& };:` - так называемая `fork bomb`:
        ```
        fork() - определение функции
        { fork | fork & } - запускает фунцкию и вывод передаёт в себя же, после чего запускает в фоновом режиме
        ```
        Таким образом, функция при каждом вызове вызывает себя дважды. При этом она не может себя завершить. Будет работать до тех пор, пока не закончатся ресурсы.
    * `[ 1645.674608] cgroup: fork rejected by pids controller in /user.slice/user-1000.slice/session-3.scope` - механизм автоматической стабилизации.
    `pids` - `kernel/cgroup_pids.c` - используется для ограничения количества процессов в рамках контрольной группы
    * ограничить число процессов, которое можно создать в сессии, можно через команду `ulimit -u 1000`.