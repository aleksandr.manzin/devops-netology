# Домашнее задание к занятию "3.5. Файловые системы"
1. Ознакомился с `sparce`. Очень интересная технология, похожа на сжатие. Но по сравнению со сжатием имеет преимущество, т.к. сжатие может снижать производительность системы при выполнении операций чтение/запись. Однако, и у `sparce` есть свои недостатки. Например, не все ФС поддерживают файлы такого типа и, при переносе файла в такую ФС, он увеличится в размере. ПО, которое поддерживает работу с разреженными файлами:
    * `μTorrent` - BitTorrent-клиент;
    * `VirtualBox` - программа виртуализации;
    * `FarManager` - файловый менеджер;
    * `RaidRecovery` - программа, которая позволяет восстанавливать разреженные файлы.
2. Файлы, являющиеся «жесткой ссылкой» на один объект, имеют одинаковые права и владельца/группу владельца. У них один и тот же индексный дескриптор (`inode`), а это означает, что все жесткие ссылки на объект, указывают на один и тот же участок диска:
    ```bash
    vagrant@vagrant:~$ touch file
    vagrant@vagrant:~$ ln file file2
    vagrant@vagrant:~$ stat file* --format="FileName: %n Inode: %i Links: %h"
    Name: file Inode: 1048620 Links: 2
    Name: file2 Inode: 1048620 Links: 2
    vagrant@vagrant:~$ find -inum 1048620 
    ./file
    ./file2
    ```
    Как мы можем видеть, созданные файлы имеют один и тот же индексный дескриптор, а также одинаковое количество «жесткий ссылок».
3. Сделал `vagrant destroy`. Отредактировал файл `VagrantFile`:
    ```
    Vagrant.configure("2") do |config|
      config.vm.box = "bento/ubuntu-20.04"
      config.vm.provider :virtualbox do |vb|
        lvm_experiments_disk0_path = "/tmp/lvm_experiments_disk0.vmdk"
        lvm_experiments_disk1_path = "/tmp/lvm_experiments_disk1.vmdk"
        vb.customize ['createmedium', '--filename', lvm_experiments_disk0_path, '--size', 2560]
        vb.customize ['createmedium', '--filename', lvm_experiments_disk1_path, '--size', 2560]
        vb.customize ['storageattach', :id, '--storagectl', 'SATA Controller', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', lvm_experiments_disk0_path]
        vb.customize ['storageattach', :id, '--storagectl', 'SATA Controller', '--port', 2, '--device', 0, '--type', 'hdd', '--medium', lvm_experiments_disk1_path]
        vb.name = "DevOps@Netology"
        vb.memory = 2048
        vb.cpus = 2
      end
    end
    ```
4. С помощью утилиты `fdisk` создал два раздела:
    ```bash
    vagrant@vagrant:~$ sudo fdisk -l
    vagrant@vagrant:~$ sudo fdisk /dev/sdb
    ```
    * попали в интерактивный режим, в котором вызываем список доступных команда `m`;
    * `p` - вывести таблицу разделов;
    * `g` - создаём таблицу разделов `GPT`;
    * `n` - создать новый раздел:
        * создали раздел 2G `/dev/sdb1` (2059M);
        * создали раздел 500M `/dev/sdb2` (500M);
    * `w` - применяем сделанные изменения:
        ```bash
        vagrant@vagrant:~# lsblk | grep sdb
        sdb                         8:16   0  2.5G  0 disk
        ├─sdb1                      8:17   0    2G  0 part
        └─sdb2                      8:18   0  500M  0 part
        ```
5. Копируем таблицы разделов с диска `/dev/sdb` на диск `/dev/sdc`:
    ```bash
    vagrant@vagrant:~$ sudo -i
    root@vagrant:~# sfdisk -d /dev/sdb | sfdisk /dev/sdc
    root@vagrant:~# lsblk | grep sdc
    sdc                         8:32   0  2.5G  0 disk
    ├─sdc1                      8:33   0    2G  0 part
    └─sdc2                      8:34   0  500M  0 part
    ```
6. создаём `RAID1` на дисках `/dev/sdb1` и `/dev/sdc1`:
    ```bash
    root@vagrant:~# mdadm --create --verbose /dev/md1 --level=raid1 --raid-devices=2 /dev/sd{b1,c1}
    ```
    Соглашаемся с предложением создать массив. И проверяем:
    ```bash
    root@vagrant:~# lsblk | egrep '(sd[bc]|md)'
      └─ubuntu--vg-ubuntu--lv 253:0    0 31.5G  0 lvm   /
    sdb                         8:16   0  2.5G  0 disk
    ├─sdb1                      8:17   0    2G  0 part
    │ └─md1                     9:0    0    2G  0 raid1
    └─sdb2                      8:18   0  500M  0 part
    sdc                         8:32   0  2.5G  0 disk
    ├─sdc1                      8:33   0    2G  0 part
    │ └─md1                     9:0    0    2G  0 raid1
    └─sdc2                      8:34   0  500M  0 part
    ```
7. создаём `RAID0` на дисках `/dev/sdb2` и `/dev/sdc2`:
    ```bash
    root@vagrant:~# mdadm --create --verbose /dev/md0 --level=raid0 --raid-devices=2 /dev/sd{b2,c2}
    ```
    Соглашаемся с предложением создать массив. И проверяем:
    ```bash
    root@vagrant:~# lsblk | egrep '(sd[bc]|md)' | grep -v 'md1''
    sdb                         8:16   0  2.5G  0 disk
    ├─sdb1                      8:17   0    2G  0 part
    └─sdb2                      8:18   0  500M  0 part
      └─md0                     9:0    0  995M  0 raid0
    sdc                         8:32   0  2.5G  0 disk
    ├─sdc1                      8:33   0    2G  0 part
    └─sdc2                      8:34   0  500M  0 part
      └─md0                     9:0    0  995M  0 raid0

    root@vagrant:~# cat /proc/mdstat
    Personalities : [linear] [multipath] [raid0] [raid1] [raid6] [raid5] [raid4] [raid10]
    md1 : active raid1 sdc1[1] sdb1[0]
          2105344 blocks super 1.2 [2/2] [UU]
    md0 : active raid0 sdc2[1] sdb2[0]
          1018880 blocks super 1.2 512k chunks
    ```
    Теперь нам необходимо сохранить конфигурацию в файле `/etc/mdadm/mdadm.conf`:
    ```bash
    root@vagrant:~# cat /etc/mdadm/mdadm.conf
    # Раскомментируем строчку "DEVICE partitions containers"
    root@vagrant:~# mdadm --detail --scan >> /etc/mdadm/mdadm.conf
    root@vagrant:~# cat /etc/mdadm/mdadm.conf
    DEVICE partitions containers
    ARRAY /dev/md1 metadata=1.2 name=vagrant:1 UUID=4c239e90:0bdf110b:12719918:7521f021
    ARRAY /dev/md0 metadata=1.2 name=vagrant:0 UUID=5755ba00:d7ef08c5:bf0e12cb:3bfb1c87
    ```
    Так как мы отредактировали файл `mdadm.conf`, нам необходимо обновить `initramfs` на всех установленных версиях ядра(по умолчанию используется последняя версия ядра `cat /etc/initramfs-tools/update-initramfs.conf`):
    ```bash
    root@vagrant:~# update-initramfs -k all -u
    ```
    P.s. столкнулся с проблемой, что после перезагрузки системы, `/dev/md0` и `/dev/md1` становились `/dev/md126` и `/dev/md127` соответственно. Пытался пересобрать raid, всё указывало, что все правильно, применял команду `update-initramfs -u` и перезагружался, а после перезагрузки снова видел злополучные `/dev/md126` и `/dev/md127`. Стал разбираться причинах и, в мануале по `update-initramfs`, наткнулся на описание ключа -k, который внёс определенного рода ясность. 
8. создаём `Physical Volumes` на `/dev/md0` и `/dev/md1`:
    ```bash
    root@vagrant:~# pvcreate /dev/md0 /dev/md1
    root@vagrant:~# pvs
      PV         VG        Fmt  Attr PSize   PFree
      /dev/md0             lvm2 ---  995.00m 995.00m
      /dev/md1             lvm2 ---   <2.01g  <2.01g
      /dev/sda3  ubuntu-vg lvm2 a--  <63.00g <31.50g
    root@vagrant:~ pvdisplay
      "/dev/md0" is a new physical volume of "995.00 MiB"
      --- NEW Physical volume ---
      PV Name               /dev/md0
      VG Name
      PV Size               995.00 MiB
      Allocatable           NO
      PE Size               0
      Total PE              0
      Free PE               0
      Allocated PE          0
      PV UUID               8B6ov1-iT0K-B9y5-7L17-ZwrO-Fh8u-PEZVm9
      
      "/dev/md1" is a new physical volume of "<2.01 GiB"
      --- NEW Physical volume ---
      PV Name               /dev/md1
      VG Name
      PV Size               <2.01 GiB
      Allocatable           NO
      PE Size               0
      Total PE              0
      Free PE               0
      Allocated PE          0
      PV UUID               sFV0Tq-nYdl-Kw9j-965o-cLva-9f07-4teJKD
    ``` 
9. создаём общую `Volume Group` на этих двух `Physical Volumes`:
    ```bash
    root@vagrant:~# vgcreate vg1 /dev/md0 /dev/md1
    root@vagrant:~# vgs
      VG        #PV #LV #SN Attr   VSize   VFree
      ubuntu-vg   1   1   0 wz--n- <63.00g <31.50g
      vg01        2   0   0 wz--n-   2.97g   2.97g
    root@vagrant:~# vgdisplay vg01
      --- Volume group ---
      VG Name               vg01
      System ID
      Format                lvm2
      Metadata Areas        2
      Metadata Sequence No  1
      VG Access             read/write
      VG Status             resizable
      MAX LV                0
      Cur LV                0
      Open LV               0
      Max PV                0
      Cur PV                2
      Act PV                2
      VG Size               2.97 GiB
      PE Size               4.00 MiB
      Total PE              761
      Alloc PE / Size       0 / 0
      Free  PE / Size       761 / 2.97 GiB
      VG UUID               Qa7A6i-FmrK-BEPl-4tlS-2Vtl-ZuXa-u90AfD
    
    ```
10. создаём `Logical Volume` размером 100 Мб и указываем его расположение на `Physical Volume` с RAID0 (`/dev/md0`):
    ```bash
    root@vagrant:~# lvcreate -L 100M -n lv01 vg01 /dev/md0
    root@vagrant:~# lvs
      LV        VG        Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
      ubuntu-lv ubuntu-vg -wi-ao----  31.50g
      lv01      vg01      -wi-a----- 100.00m
    root@vagrant:~# lvdisplay vg01
      --- Logical volume ---
      LV Path                /dev/vg01/lv01
      LV Name                lv01
      VG Name                vg01
      LV UUID                bHOMXA-qw5Q-zs0t-9CkF-6RIT-s9uL-VIpvOi
      LV Write Access        read/write
      LV Creation host, time vagrant, 2022-02-10 17:41:20 +0000
      LV Status              available
      # open                 0
      LV Size                100.00 MiB
      Current LE             25
      Segments               1
      Allocation             inherit
      Read ahead sectors     auto
      - currently set to     4096
      Block device           253:1
    root@vagrant:~# lsblk | egrep '(sd[bc]|md0|lv)'
      └─ubuntu--vg-ubuntu--lv 253:0    0 31.5G  0 lvm   /
    sdb                         8:16   0  2.5G  0 disk
    ├─sdb1                      8:17   0    2G  0 part
    └─sdb2                      8:18   0  500M  0 part
      └─md0                     9:0    0  995M  0 raid0
        └─vg01-lv01           253:1    0  100M  0 lvm
    sdc                         8:32   0  2.5G  0 disk
    ├─sdc1                      8:33   0    2G  0 part
    └─sdc2                      8:34   0  500M  0 part
      └─md0                     9:0    0  995M  0 raid0
        └─vg01-lv01           253:1    0  100M  0 lvm
    ```
11. создаём файловую систему `ext4` на нашем LV (`/dev/vg01/lv01`):
    ```bash
    root@vagrant:~# mkfs.ext4 /dev/vg01/lv01
    mke2fs 1.45.5 (07-Jan-2020)
    Creating filesystem with 25600 4k blocks and 25600 inodes
    
    Allocating group tables: done
    Writing inode tables: done
    Creating journal (1024 blocks): done
    Writing superblocks and filesystem accounting information: done    
    ```
12. монтируем раздел в `/mnt/new`:
    ```bash
    root@vagrant:~# mkdir /mnt/new
    root@vagrant:~# mount /dev/vg01/lv01 /mnt/new
    ```
13. помещаем в папку `/mnt/new` тестовый файл:
    ```bash    
    root@vagrant:~# wget https://mirror.yandex.ru/ubuntu/ls-lR.gz -O /mnt/new/test.gz
    ```
14. 
    ```bash
    root@vagrant:~# lsblk
    NAME                      MAJ:MIN RM  SIZE RO TYPE  MOUNTPOINT
    loop0                       7:0    0 55.4M  1 loop  /snap/core18/2128
    loop1                       7:1    0 55.5M  1 loop  /snap/core18/2284
    loop2                       7:2    0 32.3M  1 loop  /snap/snapd/12704
    loop3                       7:3    0 61.9M  1 loop  /snap/core20/1328
    loop4                       7:4    0 67.2M  1 loop  /snap/lxd/21835
    loop5                       7:5    0 43.4M  1 loop  /snap/snapd/14549
    loop6                       7:6    0 70.3M  1 loop  /snap/lxd/21029
    sda                         8:0    0   64G  0 disk
    ├─sda1                      8:1    0    1M  0 part
    ├─sda2                      8:2    0    1G  0 part  /boot
    └─sda3                      8:3    0   63G  0 part
      └─ubuntu--vg-ubuntu--lv 253:0    0 31.5G  0 lvm   /
    sdb                         8:16   0  2.5G  0 disk
    ├─sdb1                      8:17   0    2G  0 part
    │ └─md1                     9:1    0    2G  0 raid1
    └─sdb2                      8:18   0  500M  0 part
      └─md0                     9:0    0  995M  0 raid0
        └─vg01-lv01           253:1    0  100M  0 lvm   /mnt/new
    sdc                         8:32   0  2.5G  0 disk
    ├─sdc1                      8:33   0    2G  0 part
    │ └─md1                     9:1    0    2G  0 raid1
    └─sdc2                      8:34   0  500M  0 part
      └─md0                     9:0    0  995M  0 raid0
        └─vg01-lv01           253:1    0  100M  0 lvm   /mnt/new
    ```
15. тестируем целостность файла:
    ```bash
    root@vagrant:~# gzip -t /mnt/new/test.gz
    root@vagrant:~# echo $?
    0
    ```
16. перемещаем содержимое PV с `/dev/md0` на `/dev/md1`:
    ```bash
    root@vagrant:~# pvmove /dev/md0 /dev/md1
    root@vagrant:~# lsblk
    NAME                      MAJ:MIN RM  SIZE RO TYPE  MOUNTPOINT
    loop0                       7:0    0 55.4M  1 loop  /snap/core18/2128
    loop1                       7:1    0 55.5M  1 loop  /snap/core18/2284
    loop2                       7:2    0 32.3M  1 loop  /snap/snapd/12704
    loop3                       7:3    0 61.9M  1 loop  /snap/core20/1328
    loop4                       7:4    0 67.2M  1 loop  /snap/lxd/21835
    loop5                       7:5    0 43.4M  1 loop  /snap/snapd/14549
    loop6                       7:6    0 70.3M  1 loop  /snap/lxd/21029
    sda                         8:0    0   64G  0 disk
    ├─sda1                      8:1    0    1M  0 part
    ├─sda2                      8:2    0    1G  0 part  /boot
    └─sda3                      8:3    0   63G  0 part
      └─ubuntu--vg-ubuntu--lv 253:0    0 31.5G  0 lvm   /
    sdb                         8:16   0  2.5G  0 disk
    ├─sdb1                      8:17   0    2G  0 part
    │ └─md1                     9:1    0    2G  0 raid1
    │   └─vg01-lv01           253:1    0  100M  0 lvm   /mnt/new
    └─sdb2                      8:18   0  500M  0 part
      └─md0                     9:0    0  995M  0 raid0
    sdc                         8:32   0  2.5G  0 disk
    ├─sdc1                      8:33   0    2G  0 part
    │ └─md1                     9:1    0    2G  0 raid1
    │   └─vg01-lv01           253:1    0  100M  0 lvm   /mnt/new
    └─sdc2                      8:34   0  500M  0 part
      └─md0                     9:0    0  995M  0 raid0
    ```
17. помечаем `/dev/sdb1` вышедшим из строя:
    ```bash
    root@vagrant:~# mdadm /dev/md1 --fail /dev/sdb1
    mdadm: set /dev/sdb1 faulty in /dev/md1
    root@vagrant:~# mdadm -D /dev/md1
    ...
        Number   Major   Minor   RaidDevice State
        -       0        0        0      removed
        1       8       33        1      active sync   /dev/sdc1
        
        0       8       17        -      faulty   /dev/sdb1
    ```
18. собираем информацию из `dmesg`:
    ```bash
    root@vagrant:~# dmesg -L | grep raid1
    [    2.244348] md/raid1:md1: active with 2 out of 2 mirrors
    [10433.747480] md/raid1:md1: Disk failure on sdb1, disabling device.
                   md/raid1:md1: Operation continuing on 1 devices.
    ```
19. тестируем целостность файла:
    ```bash
    root@vagrant:~# gzip -t /mnt/new/test.gz
    root@vagrant:~# echo $?
    0
    ```
20. выполняем `vagrant destroy`:
    ```
    PS F:\!!!!VAGRANT> vagrant destroy
        default: Are you sure you want to destroy the 'default' VM? [y/N] y
    ==> default: Forcing shutdown of VM...
    ==> default: Destroying VM and associated drives...
    ```