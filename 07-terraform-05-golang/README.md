# Домашнее задание к занятию "7.5. Основы golang"

> С `golang` в рамках курса, мы будем работать не много, поэтому можно использовать любой IDE.
Но рекомендуем ознакомиться с [GoLand](https://www.jetbrains.com/ru-ru/go/).  

## Задача 1. Установите golang.
> 1. Воспользуйтесь инструкций с официального сайта: [https://golang.org/](https://golang.org/).
> 2. Так же для тестирования кода можно использовать песочницу: [https://play.golang.org/](https://play.golang.org/).

- `Go` у меня в системе уже был установлен, т.к. в задании `07-terraform-01-intro` необходимо было установить вторую версию `terraform` в системе:
  ```bash
  $ go version
  go version go1.19 linux/amd64
  ```

## Задача 2. Знакомство с gotour.
> У Golang есть обучающая интерактивная консоль [https://tour.golang.org/](https://tour.golang.org/).
> Рекомендуется изучить максимальное количество примеров. В консоли уже написан необходимый код, осталось только с ним ознакомиться и поэкспериментировать как написано в инструкции в левой части экрана.  

- ознакомился с `A Tour of Go`

## Задача 3. Написание кода.
> Цель этого задания закрепить знания о базовом синтаксисе языка. Можно использовать редактор кода на своем компьютере, либо использовать песочницу: [https://play.golang.org/](https://play.golang.org/).
>
> 1. Напишите программу для перевода метров в футы (1 фут = 0.3048 метр). Можно запросить исходные данные у пользователя, а можно статически задать в коде.
> Для взаимодействия с пользователем можно использовать функцию `Scanf`:
>    ```
>    package main
>    
>    import "fmt"
>    
>    func main() {
>        fmt.Print("Enter a number: ")
>        var input float64
>        fmt.Scanf("%f", &input)
>    
>        output := input * 2
>    
>        fmt.Println(output)    
>    }
>    ```
>  
> 1. Напишите программу, которая найдет наименьший элемент в любом заданном списке, например:
>    ```
>    x := []int{48,96,86,68,57,82,63,70,37,34,83,27,19,97,9,17,}
>    ```
> 1. Напишите программу, которая выводит числа от 1 до 100, которые делятся на 3. То есть `(3, 6, 9, …)`.
>
> В виде решения ссылку на код или сам код.

- перевод метров в футы:
  ```go
  package main

  import "fmt"

  func main() {
      const foots = 0.3048
      fmt.Print("Enter a number of meters: ")
      var input float64
      fmt.Scanf("%f", &input)

      output := input / foots

      fmt.Printf("%v meters = %.2f foots\n", input, output)
  }
  ```
  Вывод программы:
  ```bash
  $ go run meters_convertation.go
  Enter a number of meters: 10
  10 meters = 32.81 foots
  ```
  Мне захотелось немного модернизировать программу и дать возможность пользователю самому выбирать, что и куда переводить - метры в футы или же футы в метры:
  ```go
  package main

  import "fmt"

  func main() {
      const foots = 0.3048
      fmt.Print("Welcome to convertation calculator. You have 2 options:\n")
      fmt.Print("1. Convert meters to foots\n")
      fmt.Print("2. Convert foots to meters\n")
      fmt.Print("Choose unit of measure: ")
      var input float64
      fmt.Scanf("%f", &input)

      if input == 1 {
          fmt.Print("Please enter your value in meters: ")
          var input float64
          fmt.Scanf("%f", &input)

          output := input / foots
          fmt.Printf("In %.2f meter(s) there are %.2f foot(s)\n", input, output)
      } else if input == 2 {
            fmt.Print("Please enter your value in foots: ")
            var input float64
            fmt.Scanf("%f", &input)

            output := input * foots
            fmt.Printf("In %.2f foot(s) there are %.2f meter(s)\n", input, output)
      } else {
            fmt.Println("Sorry! Your choose is incorrect!")
      }
  }
  ```
    Вывод программы:
    ```bash
    $ go run convert.go
    Welcome to convertation calculator. You have 2 options:
    1. Convert meters to foots
    2. Convert foots to meters
    Choose unit of measure: 1
    Please enter your value in meters: 12
    In 12.00 meter(s) there are 39.37 foot(s)

    $ go run convert.go
    Welcome to convertation calculator. You have 2 options:
    1. Convert meters to foots
    2. Convert foots to meters
    Choose unit of measure: 2
    Please enter your value in foots: 17
    In 17.00 foot(s) there are 5.18 meter(s)

    $ go run convert.go
    Welcome to convertation calculator. You have 2 options:
    1. Convert meters to foots
    2. Convert foots to meters
    Choose unit of measure: 3  
    Sorry! Your choose is incorrect!
    ```
- вычисление наименьшего элемента в списке:
  - с помощью функции `sort`
    ```go
    package main

    import (
        "fmt"
        "sort"
    )

    func main() {
        x := []int{48,96,86,68,57,82,63,70,37,34,83,27,19,97,9,17,}
        sort.Sort(sort.IntSlice(x))
        fmt.Println("The smallest value in array is", x[0])
    }
    ```
  Вывод программы:
    ```bash
    $ go run sort.go
    The smallest value in array is 9
    ```
  - с использованием цикла `for`:
    ```go
    package main

    import "fmt"

    func main() {
        x := []int{48,96,86,68,57,82,63,70,37,34,83,27,19,97,9,17,}
        min := x[0]
        for _, el := range x {
            if el < min {
                min = el
            }
        }
        fmt.Println("The smallest value in array is", min)
    }
    ```
  Вывод программы:
    ```bash
    $ go run min_element.go
    The smallest value in array is 9
    ```

- числа, кратные 3 в диапазоне от 1 до 100:
  ```go
  package main

  import "fmt"

  func main() {
      var result []int
      for i := 1; i < 100; i++ {
          if i % 3 == 0 {
              result = append(result, i)
          }
      }
      fmt.Println(result)
  }
  ```
  Вывод программы:
  ```bash
  $ go run muliple_3.go
  [3 6 9 12 15 18 21 24 27 30 33 36 39 42 45 48 51 54 57 60 63 66 69 72 75 78 81 84 87 90 93 96 99]
  ```
