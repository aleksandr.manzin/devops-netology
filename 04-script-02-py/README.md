# Домашнее задание к занятию "4.2. Использование Python для решения типовых DevOps задач"
___
### Задача №1

> Есть скрипт:
```python
#!/usr/bin/env python3
a = 1
b = '2'
c = a + b
```

> Какое значение будет присвоено переменной `c`?

Переменной `c` не будет присвоено никакого значения, т.к. складывать `str` и `int` нельзя. Увидим ошибку на экране `TypeError`

> Как получить для переменной `c` значение `12`?

 Можно получить значение `12` для переменной `c`, если в операции сложения указать для переменной `a` тип `str`:
 
 ```python
 a = 1
 b = '2'
 c = str(a) + b
 
 print(f'c = {c}')
 ```
 
 Проверка работы скрипта:
 ```bash
 $ python3 test.py
 c = 12
 ```
 
 > Как получить для переменной `c` значение `3`?
 
 Всё, что нужно сделать - это поменять тип переменной `b`:
 ```python
 a = 1
 b = '2'
 c = a + int(b)
 
 print(f'c = {c}')
 ```
 
 Проверка работы скрипта:
 ```bash
 $ python3 test.py
 c = 3
 ```
 
 ___
 ### Задача №2
 
 > Мы устроились на работу в компанию, где раньше уже был DevOps Engineer. Он написал скрипт, позволяющий узнать, какие файлы модифицированы в репозитории, относительно локальных изменений. Этим скриптом недовольно начальство, потому что в его выводе есть не все изменённые файлы, а также непонятен полный путь к директории, где они находятся. Как можно доработать скрипт ниже, чтобы он исполнял требования вашего руководителя?
 
Во-первых, в скрипте лишняя переменная `is_change`, которая не несёт никакого функционала, во-вторых, лишний `break`, который завершает цикл при первом нахождении искомого `modified`:
 
 ```python
#!/usr/bin/env python3

import os

bash_command = ["cd ~/git", "git status"]
result_os = os.popen(' && '.join(bash_command)).read()
os.chdir('git')

print(f'Modified files in {os.getcwd()}:')
for result in result_os.split('\n'):
    if result.find('modified') == 1:
            prepare_result = result.replace('\tmodified:   ', '')
            print(f'* {prepare_result}')
```

Проверка работы скрипта:
```bash
$ chmod +x repository.py
$ ./repository.py
Modified files in /home/user/git:
* 04-script/README.md
* 1
* 2
* README.md
* test/readme.txt
```
___

### Доработка
```python
#!/usr/bin/env python3

import os

bash_command = ["cd ~/git", "git status"]
result_os = os.popen(' && '.join(bash_command)).read()
os.chdir('git')

#print(f'Modified files in {os.getcwd()}:')
for result in result_os.split('\n'):
    if result.find('modified') == 1:
            prepare_result = result.replace('\tmodified:   ', '')
            print(f'{os.getcwd()}/{prepare_result}')
```

Вывод:
```bash
user@vagrant:~$ ./repository.py git
/home/user/git/1
/home/user/git/04-script/README.md
/home/user/git/2
/home/user/git/README.md
/home/user/git/test/readme.txt
user@vagrant:~$ ./repository.py repo
/home/user/git/1
/home/user/git/04-script/README.md
/home/user/git/2
/home/user/git/README.md
/home/user/git/test/readme.txt
```
___

### Задача №3

> Доработать скрипт выше так, чтобы он мог проверять не только локальный репозиторий в текущей директории, а также умел воспринимать путь к репозиторию, который мы передаём как входной параметр. Мы точно знаем, что начальство коварное и будет проверять работу этого скрипта в директориях, которые не являются локальными репозиториями.

```python
#!/usr/bin/env python3
try:
    import os
    from sys import argv

    current_dir = os.getcwd()
    path_git = argv[1]
    git_exist = os.path.exists(f'{path_git}/.git')
    bash_command = [f'cd {path_git} 2>/dev/null', 'git status 2>/dev/null']
    result_os = os.popen(' && '.join(bash_command)).read()

    try:
        os.chdir(path_git)
        if git_exist:
            print(f'Modified files in: {os.getcwd()}')
        else:
            print('[NotAGitRepositoryError]: Choose a directory with \'.git\' inside')
    except FileNotFoundError as fnfe:
        print(f'[FileNotFoundError]: {fnfe.strerror}: \'{fnfe.filename}\'')
    except NotADirectoryError as nade:
        print(f'[NotADirectoryError]: {nade.strerror}: \'{nade.filename}\'')
    except PermissionError as pe:
        print(f'[PermissionError]: {pe.strerror}. Try to run with sudo')

    for result in result_os.split('\n'):
        if result.find('modified') == 1:
            prepare_result = result.replace('\tmodified:   ', '')
            print(f'* {prepare_result}')

except IndexError as ie:
    print(f'[IndexError]: {ie}. Choose a directory')
```

Проверка работы скрипта:
```bash
user@vagrant:~$ ./repository.py                                       # without arguments
[IndexError]: list index out of range. Choose a directory
user@vagrant:~$ ./repository.py notexist                              # unexistent file
[FileNotFoundError]: No such file or directory: 'notexist'
user@vagrant:~$ ./repository.py test                                  # any file
[NotADirectoryError]: Not a directory: 'test'
user@vagrant:~$ ./repository.py .ssh                                  # directory with no git repository
[NotAGitRepositoryError]: Choose a directory with '.git' inside
user@vagrant:~$ ./repository.py git
Modified files in: /home/user/git
* 1
* 04-script/README.md
* 2
* README.md
* test/readme.txt
user@vagrant:~$ ./repository.py repo
Modified files in: /home/user/repo
* file1
* file2
* file3
* modules/process1
user@vagrant:~$ ./repository.py /tmp/repository_test/
Modified files in: /tmp/repository_test
* program/test
* test.file
```

___

### Доработка
```python
#!/usr/bin/env python3
import os
import sys

if len(sys.argv) < 2:
    print('[NotEnoughArguments] You forgot to include a repository directory')
    sys.exit(1)

path_git = sys.argv[1]
git_exist = os.path.exists(f'{path_git}/.git')
bash_command = [f'cd {path_git} 2>/dev/null', 'git status 2>/dev/null']
result_os = os.popen(' && '.join(bash_command)).read()

if git_exist:
    os.chdir(path_git)
else:
    print('[NotAGitRepository] You should include a directory with \'.git\' inside')

for result in result_os.split('\n'):
    if result.find('modified') == 1:
        prepare_result = result.replace('\tmodified:   ', '')
        print(f'{os.getcwd()}/{prepare_result}')
```

Вывод:
```bash
user@vagrant:~$ ./repository.py																																								# without arguments
[NotEnoughArguments] You forgot to include a repository directory
user@vagrant:~$ ./repository.py 4																																						# not existent file/directory or directory without .git
[NotEnoughArguments] You forgot to include a repository directory
user@vagrant:~$ ./repository.py git
/home/user/git/1
/home/user/git/04-script/README.md
/home/user/git/2
/home/user/git/README.md
/home/user/git/test/readme.txt
```
___

### Задача №4

> Наша команда разрабатывает несколько веб-сервисов, доступных по http. Мы точно знаем, что на их стенде нет никакой балансировки, кластеризации, за DNS прячется конкретный IP сервера, где установлен сервис. Проблема в том, что отдел, занимающийся нашей инфраструктурой очень часто меняет нам сервера, поэтому IP меняются примерно раз в неделю, при этом сервисы сохраняют за собой DNS имена. Это бы совсем никого не беспокоило, если бы несколько раз сервера не уезжали в такой сегмент сети нашей компании, который недоступен для разработчиков. Мы хотим написать скрипт, который опрашивает веб-сервисы, получает их IP, выводит информацию в стандартный вывод в виде: <URL сервиса> - <его IP>. Также, должна быть реализована возможность проверки текущего IP сервиса c его IP из предыдущей проверки. Если проверка будет провалена - оповестить об этом в стандартный вывод сообщением: [ERROR] <URL сервиса> IP mismatch: <старый IP> <Новый IP>. Будем считать, что наша разработка реализовала сервисы: drive.google.com, mail.google.com, google.com.

```python
#!/usr/bin/env python3
import socket
import time
import datetime as dt

service = {
    'drive.google.com': '0.0.0.0',
    'mail.google.com': '0.0.0.0',
    'google.com': '0.0.0.0'
}


while True:
    for host_name, host_ip in service.items():
        ip = socket.gethostbyname(host_name)
        with open('service.log', 'a') as log:
            log.write(f'{host_name} - {ip}\n')

        timestamp = dt.datetime.now().strftime("%H:%M:%S")
        time.sleep(1)
        if service[host_name] == ip:
            print(f'{timestamp} {host_name} - {host_ip}')
        else:
            print(f'{timestamp} [ERROR] {host_name} IP mismatch: {service[host_name]} {ip}')
            with open('error.log', 'a') as error:
                error.write(f'{timestamp} [ERROR] {host_name} IP mismatch: {service[host_name]} {ip}\n')

    with open('service.log', 'r') as log:
        data = log.read()
        for line in data.splitlines():
            host, ip = line.split(' - ')
            service[host] = ip
```

Проверка работы скрипта:
```bash
user@vagrant:~$ ./service.py
19:22:19 [ERROR] drive.google.com IP mismatch: 0.0.0.0 64.233.161.194
19:22:20 [ERROR] mail.google.com IP mismatch: 0.0.0.0 64.233.163.17
19:22:21 [ERROR] google.com IP mismatch: 0.0.0.0 142.251.1.113
19:22:22 drive.google.com - 64.233.161.194
19:22:23 mail.google.com - 64.233.163.17
19:22:24 google.com - 142.251.1.113
19:22:25 drive.google.com - 64.233.161.194
19:22:26 mail.google.com - 64.233.163.17
19:22:27 google.com - 142.251.1.113
19:22:28 drive.google.com - 64.233.161.194
19:22:29 mail.google.com - 64.233.163.17
19:22:30 google.com - 142.251.1.113
19:22:31 drive.google.com - 64.233.161.194
19:22:32 mail.google.com - 64.233.163.17
19:22:33 google.com - 142.251.1.113
19:22:34 drive.google.com - 64.233.161.194
19:22:35 mail.google.com - 64.233.163.17
...
.....
```
Файл ошибок:
```bash
user@vagrant:~$ cat error.log
19:22:19 [ERROR] drive.google.com IP mismatch: 0.0.0.0 64.233.161.194
19:22:20 [ERROR] mail.google.com IP mismatch: 0.0.0.0 64.233.163.17
19:22:21 [ERROR] google.com IP mismatch: 0.0.0.0 142.251.1.113
19:25:13 [ERROR] google.com IP mismatch: 142.251.1.113 173.194.73.138
19:25:14 [ERROR] drive.google.com IP mismatch: 64.233.161.194 64.233.165.194
19:25:15 [ERROR] mail.google.com IP mismatch: 64.233.163.17 173.194.220.19
19:25:16 [ERROR] google.com IP mismatch: 173.194.73.138 173.194.73.139
19:25:18 [ERROR] mail.google.com IP mismatch: 173.194.220.19 173.194.220.18
```
