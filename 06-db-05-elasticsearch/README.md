# Домашнее задание к занятию "6.5. Elasticsearch"

## Задача 1

> В этом задании вы потренируетесь в:
> - установке elasticsearch
> - первоначальном конфигурировании elastcisearch
> - запуске elasticsearch в docker
>
> Используя докер образ [centos:7](https://hub.docker.com/_/centos) как базовый и
> [документацию по установке и запуску Elastcisearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/targz.html):
>
> - составьте Dockerfile-манифест для elasticsearch
> - соберите docker-образ и сделайте `push` в ваш docker.io репозиторий
> - запустите контейнер из получившегося образа и выполните запрос пути `/` c хост-машины
>
> Требования к `elasticsearch.yml`:
> - данные `path` должны сохраняться в `/var/lib`
> - имя ноды должно быть `netology_test`
>
> В ответе приведите:
> - текст Dockerfile манифеста
> - ссылку на образ в репозитории dockerhub
> - ответ `elasticsearch` на запрос пути `/` в json виде
>
> Подсказки:
> - возможно вам понадобится установка пакета perl-Digest-SHA для корректной работы пакета shasum
> - при сетевых проблемах внимательно изучите кластерные и сетевые настройки в elasticsearch.yml
> - при некоторых проблемах вам поможет docker директива ulimit
> - elasticsearch в логах обычно описывает проблему и пути ее решения
>
> Далее мы будем работать с данным экземпляром elasticsearch.

- создадим файл настроек `elasticsearch.yml`:
  ```yml
  node.name: "netology_test"
  path:
    data: /var/lib/elasticsearch
    logs: /var/log/elasticsearch
    repo: /elasticsearch-8.3.3/snapshots
  network.host: 0.0.0.0
  http.port: 9200
  discovery.type: single-node
  discovery.seed_hosts: ["127.0.0.1"]
  xpack.security.enabled: false
  ```

- создадим `Dockerfile`:
  ```
  FROM centos:7

  LABEL author="Aleksandr Manzin <furunduk@gmail.com>"

  ENV PATH=$PATH:/elasticsearch-8.3.3/bin

  RUN yum update -y && \
      yum install -y perl-Digest-SHA && \
      yum install -y wget && \
      wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-8.3.3-linux-x86_64.tar.gz && \
      wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-8.3.3-linux-x86_64.tar.gz.sha512 && \
      shasum -a 512 -c elasticsearch-8.3.3-linux-x86_64.tar.gz.sha512 && \
      tar -xzf elasticsearch-8.3.3-linux-x86_64.tar.gz && \
      rm -f elasticsearch-8.3.3-linux-x86_64.tar.gz*

  RUN mkdir /var/lib/elasticsearch \
            /var/log/elasticsearch \
            /elasticsearch-8.3.3/snapshots && \
      groupadd -g 1000 elasticsearch && \
      useradd elasticsearch -u 1000 -g 1000 && \
      chown -R elasticsearch:elasticsearch /var/lib/elasticsearch \
                                           /var/log/elasticsearch \
                                           /elasticsearch-8.3.3

  COPY elasticsearch.yml /elasticsearch-8.3.3/config

  USER elasticsearch

  CMD [ "elasticsearch" ]
  ```

- собираем получившийся `docker-образ`:
  ```bash
  $ docker build -t aleksandrmanzin/elasticsearch:8.3.3 .
  ```

- "пушим" наш образ в репозиторий:
  ```bash
  $ docker login
  Login with your Docker ID to push and pull images from Docker Hub. If you dont have a Docker ID, head over to https://hub.docker.com to create one.
  Username: aleksandrmanzin
  Password:
  $ docker push aleksandrmanzin/elasticsearch:8.3.3
  The push refers to repository [docker.io/aleksandrmanzin/elasticsearch]
  fb88121efb92: Pushed
  f7a42172cc10: Pushed
  3a611253af04: Pushed
  174f56854903: Mounted from library/centos
  8.3.3: digest: sha256:bf4ad7ae81b36f695f5ab0fbb86bbc20107aedcb885764499af55a6087b8324f size: 1162
  ```
  - ссылка: [aleksandrmanzin/elasticsearch:8.3.3](https://hub.docker.com/layers/elasticsearch/aleksandrmanzin/elasticsearch/8.3.3/images/sha256-bf4ad7ae81b36f695f5ab0fbb86bbc20107aedcb885764499af55a6087b8324f?context=explore)
  - docker pull aleksandrmanzin/elasticsearch:8.3.3

- запустим наш контейнер:
```bash
$ docker run --rm -p 9200:9200 -d ee5315bbb23c
ab50f9fa0401a123f0f2b4e9c33ced690a74ecdac88cf85d391b728e16dec433
```

- выполняем `curl`:
  ```bash
  $ curl -X GET "192.168.100.111:9200/"
  {
    "name" : "netology_test",
    "cluster_name" : "elasticsearch",
    "cluster_uuid" : "SOBMHKcgRfKXKx2N1S2N3Q",
    "version" : {
      "number" : "8.3.3",
      "build_flavor" : "default",
      "build_type" : "tar",
      "build_hash" : "801fed82df74dbe537f89b71b098ccaff88d2c56",
      "build_date" : "2022-07-23T19:30:09.227964828Z",
      "build_snapshot" : false,
      "lucene_version" : "9.2.0",
      "minimum_wire_compatibility_version" : "7.17.0",
      "minimum_index_compatibility_version" : "7.0.0"
    },
    "tagline" : "You Know, for Search"
  }
  ```

## Задача 2

>  В этом задании вы научитесь:
>  - создавать и удалять индексы
>  - изучать состояние кластера
>  - обосновывать причину деградации доступности данных
>
> Ознакомтесь с [документацией](https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-create-index.html) и добавьте в `elasticsearch` 3 индекса, в соответствии с таблицей:
>
> | Имя | Количество реплик | Количество шард |
> |-----|-------------------|-----------------|
> | ind-1| 0 | 1 |
> | ind-2 | 1 | 2 |
> | ind-3 | 2 | 4 |
>
> Получите список индексов и их статусов, используя API и **приведите в ответе** на задание.
>
> Получите состояние кластера `elasticsearch`, используя API.
>
> Как вы думаете, почему часть индексов и кластер находится в состоянии yellow?
>
> Удалите все индексы.
>
> **Важно**
>
> При проектировании кластера elasticsearch нужно корректно рассчитывать количество реплик и шард, иначе возможна потеря данных индексов, вплоть до полной, при деградации системы.

- создадим индексы:
  ```bash
  $ curl -X PUT 192.168.100.111:9200/ind-1 -H 'Content-Type: application/json' -d '
  {
    "settings": {
      "index": {
        "number_of_shards": 1,
        "number_of_replicas": 0
      }
    }
  }'
  {"acknowledged":true,"shards_acknowledged":true,"index":"ind-1"}%

  $ curl -X PUT 192.168.100.111:9200/ind-2 -H 'Content-Type: application/json' -d '
  {
    "settings": {
      "index": {
        "number_of_shards": 2,
        "number_of_replicas": 1
      }
    }
  }'
  {"acknowledged":true,"shards_acknowledged":true,"index":"ind-2"}%

  $ curl -X PUT 192.168.100.111:9200/ind-3 -H 'Content-Type: application/json' -d '
  {
    "settings": {
      "index": {
        "number_of_shards": 4,
        "number_of_replicas": 2
      }
    }
  }'
  {"acknowledged":true,"shards_acknowledged":true,"index":"ind-3"}%
  ```

- посмотрим на наши индексы:
  ```bash
  $ curl -X GET "192.168.100.111:9200/_cat/indices"
  yellow open ind-3 Vb21OOZJTx-EgU-MwsFA5w 4 2 0 0 900b 900b
  green  open ind-1 BqXpGQS6SLC2lmrZ8XhV-g 1 0 0 0 225b 225b
  yellow open ind-2 a0SQk0pSQcOqZknh6C0BQA 2 1 0 0 450b 450b
  ```
- проверим состояние кластера:
  ```bash
  $ curl -X GET "192.168.100.111:9200/_cluster/health/?pretty"
  {
    "cluster_name" : "elasticsearch",
    "status" : "yellow",
    "timed_out" : false,
    "number_of_nodes" : 1,
    "number_of_data_nodes" : 1,
    "active_primary_shards" : 8,
    "active_shards" : 8,
    "relocating_shards" : 0,
    "initializing_shards" : 0,
    "unassigned_shards" : 10,
    "delayed_unassigned_shards" : 0,
    "number_of_pending_tasks" : 0,
    "number_of_in_flight_fetch" : 0,
    "task_max_waiting_in_queue_millis" : 0,
    "active_shards_percent_as_number" : 44.44444444444444
  }
  ```
  Как мы видим, общее состояние кластера `yellow` и часть индексов имеют такое же состояние. Это происходит из-за того, что у нас всего одна нода, а при создании индексов мы указали число реплик, превосходящее значение `number_of_nodes - 1`. Если мы посмотрим на наши шарды (`curl -X GET "192.168.100.111:9200/_cat/shards"`), то увидим, что часть из них в состоянии `UNASIGNED`, т.е. не привязан ни к одной из нод.

- удаляем индексы (при желании можно подправить `elasticsearch.yml` установив параметр `action.destructive_requires_name: false`, что даст нам возможность использовать `*` или `_all` при процедуре удаления):
  ```bash
  $ curl -X DELETE "192.168.100.111:9200/_all"     
  {"acknowledged":true}%    
  ```

## Задача 3

> В данном задании вы научитесь:
> - создавать бэкапы данных
> - восстанавливать индексы из бэкапов
>
> Создайте директорию `{путь до корневой директории с elasticsearch в образе}/snapshots`.
>
> Используя API [зарегистрируйте](https://www.elastic.co/guide/en/elasticsearch/reference/current/snapshots-register-repository.html#snapshots-register-repository) данную директорию как `snapshot repository` c именем `netology_backup`.
>
> **Приведите в ответе** запрос API и результат вызова API для создания репозитория.
>
> Создайте индекс `test` с 0 реплик и 1 шардом и **приведите в ответе** список индексов.
>
> [Создайте `snapshot`](https://www.elastic.co/guide/en/elasticsearch/reference/current/snapshots-take-snapshot.html) состояния кластера `elasticsearch`.
>
> **Приведите в ответе** список файлов в директории со `snapshot`ами.
>
> Удалите индекс `test` и создайте индекс `test-2`. **Приведите в ответе** список индексов.
>
> [Восстановите](https://www.elastic.co/guide/en/elasticsearch/reference/current/snapshots-restore-snapshot.html) состояние кластера `elasticsearch` из `snapshot`, созданного ранее.
>
> **Приведите в ответе** запрос к API восстановления и итоговый список индексов.
>
> Подсказки:
> - возможно вам понадобится доработать `elasticsearch.yml` в части директивы `path.repo` и перезапустить `elasticsearch`

- регистрируем директорию `elasticsearch-8.3.3/snapshots` как `snapshot repository` с именем `netology_backup`:
  ```bash
  $ curl -X PUT "localhost:9200/_snapshot/netology_backup" -H 'Content-Type: application/json' -d '
  {
    "type": "fs",
    "settings":
    {
      "location": "/elasticsearch-8.3.3/snapshots"
    }
  }'
  {"acknowledged":true}%
  ```
- проверим, все ли правильно:
  ```bash
  $ curl -X GET "localhost:9200/_snapshot/netology_backup/?pretty"
  {
    "netology_backup" : {
      "type" : "fs",
      "settings" : {
        "location" : "/elasticsearch-8.3.3/snapshots"
      }
    }
  }

  $ curl -X POST "localhost:9200/_snapshot/netology_backup/_verify/?pretty"
  {
    "nodes" : {
      "Orop-mauTfGzyUXFFVV3iQ" : {
        "name" : "netology_test"
      }
    }
  }
  ```
- создадим индекс `test` с 0 реплик и 1 шардом:
  ```bash
  $ curl -X PUT "localhost:9200/test" -H "Content-Type: application/json" -d '
    {
      "settings": {
        "number_of_shards": 1,
        "number_of_replicas": 0
      }    
    }'
  {"acknowledged":true,"shards_acknowledged":true,"index":"test"}%   
  ```
- посмотрим список индексов:
  ```bash
  $ curl "localhost:9200/_cat/indices"
  green open test YWMdG__-SHeaAshMlTBjwA 1 0 0 0 225b 225b
  ```
- создадим `snapshot`:
  ```bash
  $ curl -X PUT "localhost:9200/_snapshot/netology_backup/elasticsearch"
  {"accepted":true}%  
  ```
- список файлов в директории `/elasticsearch-8.3.3/snapshots`:
  ```bash
  $ docker exec -it unruffled_turing ls -l '/elasticsearch-8.3.3/snapshots'
  total 36
  -rw-r--r-- 1 elasticsearch elasticsearch   846 Aug 15 16:44 index-0
  -rw-r--r-- 1 elasticsearch elasticsearch     8 Aug 15 16:44 index.latest
  drwxr-xr-x 4 elasticsearch elasticsearch  4096 Aug 15 16:44 indices
  -rw-r--r-- 1 elasticsearch elasticsearch 18475 Aug 15 16:44 meta-rXE5vl5qSguN6mrMSS3-ag.dat
  -rw-r--r-- 1 elasticsearch elasticsearch   354 Aug 15 16:44 snap-rXE5vl5qSguN6mrMSS3-ag.dat
  ```
- удаляем индекс `test`:
  ```bash
  $ curl -X DELETE "localhost:9200/test"
  {"acknowledged":true}%  
  ```
- создаем индекс `test-2`:
  ```bash
  $ curl -X PUT "localhost:9200/test-2" -H "Content-Type: application/json" -d '
  {
    "settings": {
      "number_of_shards": 1,
      "number_of_replicas": 0
    }
  }'
  {"acknowledged":true,"shards_acknowledged":true,"index":"test-2"}%
  ```
- список индексов:
  ```bash
  $ curl "localhost:9200/_cat/indices"
  green open test-2 vd0Hr98ERRuxixw_1QoFWg 1 0 0 0 225b 225b
  ```
- восстанавливаем состояние кластера из `snapshot`:
  ```bash
  $ curl -X POST "localhost:9200/_snapshot/netology_backup/elasticsearch/_restore"
  {"accepted":true}%
  ```
- итоговый список индексов:
  ```bash
  $ curl "localhost:9200/_cat/indices"
  green open test-2 vd0Hr98ERRuxixw_1QoFWg 1 0 0 0 225b 225b
  green open test   3L2oTQrYRcOdepdvXLPUKg 1 0 0 0 225b 225b
  ```
