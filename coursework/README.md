# Курсовая работа по итогам модуля "DevOps и системное администрирование"

1. > Создайте виртуальную машину Linux.

	* развернул Ubuntu 20.04 на VirtualBox;
	* добавил второй IP-адрес: `192.168.100.20/24`:
		```bash
		$ cat /etc/netplan/01-network-manager-all.yaml
		# Let NetworkManager manage all devices on this system
		network:
		  version: 2
		  renderer: NetworkManager
		  ethernets:
		    enp0s3:
		      dhcp4: yes
            enp0s8:
              dhcp4: no
              addresses: [192.168.100.20/24]
              gateway4: 192.168.100.1
              nameservers:
                addresses: [192.168.100.1]
		$ sudo netplan apply
		$ ip -c -br -4 a
		lo               UNKNOWN        127.0.0.1/8
		enp0s3           UP             10.0.2.15/24
		enp0s8           UP             192.168.100.20/24		
		```

2. > Установите ufw и разрешите к этой машине сессии на порты 22 и 443, при этом трафик на интерфейсе localhost(lo) должен ходить свободно на все порты.
	
	* включаем `ufw` (`sudo ufw enable`)
	* настраиваем `ufw`:
		```bash
		$ sudo ufw allow 443			# разрешаем подключения на 443 порт
		$ sudo ufw allow 22			# разрешаем подключения на 22 порт
		$ sudo ufw allow in on lo		# разрешаем подключения к сетевому интерфейсу lo
		$ sudo ufw status
		Состояние: активен

		В                          Действие    Из
		-                          --------    --
		Anywhere on lo             ALLOW       Anywhere
		22                         ALLOW       Anywhere
		443                        ALLOW       Anywhere
		Anywhere (v6) on lo        ALLOW       Anywhere (v6)
		22 (v6)                    ALLOW       Anywhere (v6)
		443 (v6)                   ALLOW       Anywhere (v6)
		```
		
	* производим настройку подключения к виртуальной машине с помощью сгенерированного ключа, отключаем подключение по паролю:
		- `ssh-keygen`;
		- копируем содержимое `id_rsa.pub`
		- и настраиваем Файл `config`:
			```
			Host ubnt
			HostName 192.168.100.20
			User user
			IdentityFile "C:\Users\user\.ssh\id_rsa"
			```
	    - теперь можно подключаться, используя короткое имя (`ssh ubnt`) и по ключу.
		
3. > Установите hashicorp vault

	* подключаем официальный репозиторий и устанавливаем пакет vault:
		```bash
		$ curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
		$ sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
		$ sudo apt-get update && sudo apt-get install vault
		```
	* проверяем установку `vault`:
		```bash
		$ vault
		Usage: vault <command> [args]

		Common commands:
			read        Read data and retrieves secrets
			write       Write data, configuration, and secrets
			delete      Delete secrets and configuration
			list        List data or secrets
			login       Authenticate locally
			agent       Start a Vault agent
			server      Start a Vault server
			status      Print seal and HA status
			unwrap      Unwrap a wrapped secret

		Other commands:
			audit                Interact with audit devices
			auth                 Interact with auth methods
			debug                Runs the debug command
			kv                   Interact with Vault's Key-Value storage
			lease                Interact with leases
			monitor              Stream log messages from a Vault server
			namespace            Interact with namespaces
			operator             Perform operator-specific tasks
			path-help            Retrieve API help for paths
			plugin               Interact with Vault plugins and catalog
			policy               Interact with policies
			print                Prints runtime configurations
			secrets              Interact with secrets engines
			ssh                  Initiate an SSH session
			token                Interact with tokens
			version-history      Prints the version history of the target Vault server
		```
	
4. > Cоздайте центр сертификации и выпустите сертификат для использования его в настройке веб-сервера nginx (срок жизни сертификата - месяц)
5. > Установите корневой сертификат созданного центра сертификации в доверенные в хостовой системе.
	
	### Запуск `Vault`
	* в другом терминале запускаем `Vault dev-сервер` с root, в качестве корневого токена:
		```bash
		$ vault server -dev -dev-root-token-id root
		```
	* экспортируем переменную среду `VAULT_ADDR` для обращения к Vault серверу:
		```bash
		$ export VAULT_ADDR=http://127.0.0.1:8200
		```
	* экспортируем переменную среду `VAULT_TOKEN` для аутентификации на Vault сервере:
		```bash
		$ export VAULT_TOKEN=root
		```
	
	### Генерация корневого центра сертификации
	* включаем механизм `pki`:
		```bash
		$ vault secrets enable pki
		Success! Enabled the pki secrets engine at: pki/
		```
	* настраиваем механизм `pki` для выдачи сертификатов с максимальным сроком жизни 87600 часов:
		```bash
		$ vault secrets tune -max-lease-ttl=87600h pki
		Success! Tuned the secrets engine at: pki/
		```
	* генерируем корневой сертификат и сохраняем его как `CA_cert.crt`:
		```bash
		$ vault write -field=certificate pki/root/generate/internal \
			common_name="aleksandrmanzin.com" \
			ttl=87600h > CA_cert.crt
		```
		Т.о. мы сгенерировали самоподписанный сертификат центра сертификации и закрытый ключ. Vault автоматически отзовёт сертификат по окончании срока жизни (TTL). Сертификат центра сертификации подписывает собственный список отзыва сертификатов(CRL)
		
	* настраиваем URL-адреса CA(центра сертификации) и CRL(списки отозванных сертификатов):
		```bash
		$ vault write pki/config/urls \
			issuing_certificates="$VAULT_ADDR/v1/pki/ca" \
			crl_distribution_points="$VAULT_ADDR/v1/pki/crl"
		Success! Data written to: pki/config/urls
		```
	
	### Генерируем промежуточный центр сертификации
	* включаем механизм `pki` в директории `pki_int`:
		```bash
		$ vault secrets enable -path=pki_int pki
		Success! Enabled the pki secrets engine at: pki_int/
		```
	* настраиваем механизм pki_int для выдачи сертификатом с максимальным сроком жизни 43800 часов:
		```bash
		$ vault secrets tune -max-lease-ttl=43800h pki_int
		Success! Tuned the secrets engine at: pki_int/
		```
	* установим утилиту `jq`:
		```bash
		$ sudo apt install -y jq
		```
	* генерируем промежуточный файл и сохраняем `CSR` как `pki_intermediate.csr`:
		```bash
		$ vault write -format=json pki_int/intermediate/generate/internal \
			common_name="aleksandrmanzin.com Intermediate Authority" \
			| jq -r '.data.csr' > pki_intermediate.csr
		```
	* подпишем промежуточный сертификат закрытым ключом корневого ЦС и сохраним сгенерированный сертификат как `intermediate.cert.pem`:
		```bash
		$ vault write -format=json pki/root/sign-intermediate csr=@pki_intermediate.csr \
			format=pem_bundle ttl="43800h" \
			| jq -r '.data.certificate' > intermediate.cert.pem
		```
	* после того, как CSR подписан и корневой ЦС возвращает сертификат, можно его импортировать в Vault:
		```bash
		$ vault write pki_int/intermediate/set-signed certificate=@intermediate.cert.pem
		Success! Data written to: pki_int/intermediate/set-signed
		```
		
	### Создаем роль
	* создаем роль с названием `aleksandr-manzin-dot-com`, которая поддерживает поддомены:
		```bash
		$ vault write pki_int/roles/aleksandr-manzin-dot-com \
			allowed_domains="aleksandrmanzin.com" \
			allow_subdomains=true \
			max_ttl="720h"
		Success! Data written to: pki_int/roles/aleksandr-manzin-dot-com
		```
	
	### Запрос сертификатов
	* запрашиваем новый сертификат для домена `vault.aleksandrmanzin.com`, на основе роли `aleksandr-manzin-dot-com`:
		```bash
		$ vault write -format=json pki_int/issue/aleksandr-manzin-dot-com \
			common_name="vault.aleksandrmanzin.com" ttl="24h" > vault_certificate.json
		Key                 Value
		---                 -----
		ca_chain            [-----BEGIN CERTIFICATE-----
		MIIDgzCCAmugAwIBAgIUCUPx6TvIrwP0ilisw/OlKIOXW9MwDQYJKoZIhvcNAQEL
		BQAwHjEcMBoGA1UEAxMTYWxla3NhbmRybWFuemluLmNvbTAeFw0yMjA0MjgxNzU0
		
					...
		
		-----END CERTIFICATE-----]
		certificate         -----BEGIN CERTIFICATE-----
		MIIDhjCCAm6gAwIBAgIUErMxpNIx82OhUAVSf0+vucP83PYwDQYJKoZIhvcNAQEL
		BQAwNTEzMDEGA1UEAxMqYWxla3NhbmRybWFuemluLmNvbSBJbnRlcm1lZGlhdGUg
		
					...
					
		-----END CERTIFICATE-----
		expiration          1651179286
		issuing_ca          -----BEGIN CERTIFICATE-----
		MIIDgzCCAmugAwIBAgIUCUPx6TvIrwP0ilisw/OlKIOXW9MwDQYJKoZIhvcNAQEL
		BQAwHjEcMBoGA1UEAxMTYWxla3NhbmRybWFuemluLmNvbTAeFw0yMjA0MjgxNzU0
					
					...
					
		-----END CERTIFICATE-----
		private_key         -----BEGIN RSA PRIVATE KEY-----
		MIIEpQIBAAKCAQEAxE5uzjIU5mtVlvSvqLEcN1TV56kwxqTNwPU5UsiaLsYsKw5Y
		VN039WGncOwHkD/bG6vUTsQ2/PdjLWVdOjGTrYO+xE7xc30q5l0S2D97qzovwMr+
		
					...
					
		-----END RSA PRIVATE KEY-----
		private_key_type    rsa
		serial_number       5a:a6:33:9d:c2:5c:01:86:ba:45:b6:04:e2:f3:3d:47:58:e9:8d:ea
		```
	### Сохраняем в отдельные файлы сертификат и ключ:
		```bash
		$ sudo bash -c 'jq -r '.data.certificate' ./vault_certificate.json > /etc/nginx/ssl/vault_certificate.pem'
		$ sudo bash -c 'jq -r '.data.issuing_ca' ./vault_certificate.json >> /etc/nginx/ssl/vault_certificate.pem'
		$ sudo bash -c 'jq -r '.data.private_key' ./vault_certificate.json > /etc/nginx/ssl/vault_certificate.key'

6. > Установите nginx
		
 	* установка `nginx` и проверка запущенного сервиса:
		```bash
		$ sudo apt install -y nginx
		$ systemctl status nginx 
		● nginx.service - A high performance web server and a reverse proxy server
			 Loaded: loaded (/lib/systemd/system/nginx.service; enabled; vendor preset: enabled)
			 Active: active (running) since Thu 2022-04-28 21:01:26 MSK; 1min 8s ago
			   Docs: man:nginx(8)
		   Main PID: 1727 (nginx)
			  Tasks: 2 (limit: 2290)
			 Memory: 3.0M
			 CGroup: /system.slice/nginx.service
					 ├─1727 nginx: master process /usr/sbin/nginx -g daemon on; master_process on;
					 └─1728 nginx: worker process
		```
		
7. > По инструкции настройте nginx на https, используя ранее подготовленный сертификат:
	>	* можно использовать стандартную стартовую страницу nginx для демонстрации работы сервера;
	>	* можно использовать и другой html файл, сделанный вами;
	
	* для начала создадим директорию в каталоге `/var/www/` с названием нашего домена `aleksandrmanzin` и назначим владельца: 
		```bash
		$ sudo mkdir -p /var/www/aleksandrmanzin/html
		$ sudo chown -R $USER:$USER /var/www/aleksandrmanzin/html
		```
	* скопируем стандартную страницу nginx из дефолтной директории:
		```bash
		$ sudo cp /var/www/html/index.nginx-debian.html /var/www/aleksandrmanzin/html/
		```
	* создадим файл в директории `/etc/nginx/sites-available/`, чтоб не изменять файл конфигурации по умолчанию:
		```bash
		$ sudo nano /etc/nginx/sites-available/aleksandrmanzin
		server {
				listen 443 ssl;
				listen [::]:443 ssl;

				root /var/www/aleksandrmanzin/html;
				index index.nginx-debian.html;

				server_name vault.aleksandrmanzin.com;
				ssl_certificate /etc/nginx/ssl/vault_certificate.pem;
				ssl_certificate_key /etc/nginx/ssl/vault_certificate.key;

				location / {
						try_files $uri $uri/ =404;
				}
		}
		```
				
	* создадим ссылку в директории `sites-enabled`, которую nginx считывает при запуске:
		```bash
		$ sudo ln -s /etc/nginx/sites-available/aleksandrmanzin /etc/nginx/sites-enabled
		$ ls -la /etc/nginx/sites-enabled
		ls -l /etc/nginx/sites-enabled/
		итого 0
		lrwxrwxrwx 1 root root 42 апр 28 22:15 aleksandrmanzin -> /etc/nginx/sites-available/aleksandrmanzin
		lrwxrwxrwx 1 root root 34 апр 28 21:01 default -> /etc/nginx/sites-available/default
		```
	* скопируем корневой сертификат и сохраним на хостовой машине, после чего установим его в доверенные корневые центры сертификации:
		
		![CA](screenshots/CA_cert.png "CA_cert")

8. > Откройте в браузере на хосте https адрес страницы, которую обслуживает сервер nginx.

	* проверяем работу сайта и сертификаты:
	
		![Certificates](screenshots/Certificates.png "Certificates")
		![Vault Certificate](screenshots/Vault_certificate.png "Vault Certificate")
		
9. > Создайте скрипт, который будет генерировать новый сертификат в vault:
	> * генерируем новый сертификат так, чтобы не переписывать конфиг nginx;
	> * перезапускаем nginx для применения нового сертификата.
	
	* сделаем скрипт, который будет генерировать сертификат на месяц (720 часов). Предыдущий был на 24часа
		```bash
		$ nano vault_script.sh
		vault write -format=json pki_int/issue/aleksandr-manzin-dot-com common_name="vault.aleksandrmanzin.com" ttl="720h" > vault_certificate.json
		sudo bash -c "jq -r '.data.certificate' ./vault_certificate.json > /etc/nginx/ssl/vault_certificate.pem"
		sudo bash -c "jq -r '.data.issuing_ca' ./vault_certificate.json >> /etc/nginx/ssl/vault_certificate.pem"
		sudo bash -c "jq -r '.data.private_key' ./vault_certificate.json > /etc/nginx/ssl/vault_certificate.key"
		sudo systemctl restart nginx
		```
	* сохраняем созданный скрипт, делаем его исполняемым и запускаем для проверки:
		```bash
		$ chmod +x vault_script.sh
		$ ./vault_script.sh
		```
	* проверяем и видим, что сертификат обновился. Теперь он выдан на месяц:
		![New Certificate](screenshots/New_certificate.png "New Certificate")

10. > Поместите скрипт в crontab, чтобы сертификат обновлялся какого-то числа каждого месяца в удобное для вас время.
	
	* для проверки в скрипте исправил срок выдачи с 720часов на 24часа;
	
	* с учетом того, что у нас в скрипте используется `sudo`, то нам понадобится запустить `crontab` от `sudo`. Для проверки сделал время запуск скрипта в 19:45:
		```bash
		$ sudo crontab -e
		45 19 * * * /home/user/vault_script.sh >/dev/null 2>&1
		```
	* проверим логи, выполняется ли задача в cron:
		```bash
		$ sudo grep CRON /var/log/syslog
		May  1 19:45:01 furunduk CRON[6699]: (root) CMD (/home/user/vault_script.sh >/dev/null 2>&1)
		```
	* проверим, обновился ли сертификат:
		```bash
		$ ls -l /etc/nginx/ssl/
		итого 0
		-rw-r--r-- 1 root root 0 мая  1 19:45 vault_certificate.key
		-rw-r--r-- 1 root root 0 мая  1 19:45 vault_certificate.pem
		```
		![CRON](screenshots/cron.png "Certificate from cron task")